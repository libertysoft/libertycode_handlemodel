<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\standard\library;



class ConstStandardAttrSpec
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_DATA_TYPE_COLLECTION = 'objDataTypeCollection';



    // Configuration
    const TAB_CONFIG_KEY_SORT_DATA_TYPE_ASC_REQUIRE = 'sort_data_type_asc_require';
    const TAB_CONFIG_KEY_SELECT_DATA_TYPE_FIRST_REQUIRE = 'select_data_type_first_require';



    // Exception message constants
    const EXCEPT_MSG_DATA_TYPE_COLLECTION_INVALID_FORMAT =
        'Following data type collection "%1$s" invalid! It must be a data type collection object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the standard attribute specification configuration standard.';



}