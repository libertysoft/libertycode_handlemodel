<?php
/**
 * Description :
 * This class allows to describe behavior of attribute class.
 * Attribute is item, containing all configuration to design specific attribute,
 * used on entity.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\api;

use liberty_code\model\entity\model\ValidatorConfigEntity;



interface AttributeInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get attribute configuration array.
     *
     * @return array
     */
    public function getTabAttributeConfig();



	/**
	 * Get string attribute key
     * (used in entity attribute configuration).
	 *
	 * @return string
	 */
	public function getStrAttributeKey();



    /**
     * Get entity attribute configuration array.
     *
     * Return array format:
     * Attribute configuration array format,
     * from @see ValidatorConfigEntity::getTabConfig() return array format.
     *
     * @return array
     */
    public function getTabEntityAttrConfig();



    /**
     * Get entity attribute rule configuration array.
     *
     * Return array format:
     * Attribute rule configuration array format,
     * from @see ValidatorConfigEntity::getTabRuleConfig() return array format.
     *
     * @return array
     */
    public function getTabEntityAttrRuleConfig();



    /**
     * Get specified entity attribute formatted value when get action required.
     *
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueFormatGet($value);



    /**
     * Get specified entity attribute formatted value when set action required.
     *
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueFormatSet($value);





    // Methods setters
    // ******************************************************************************

    /**
     * Set attribute configuration array.
     *
     * @param array $tabConfig
     */
    public function setAttributeConfig(array $tabConfig);
}