<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\exception;

use liberty_code\handle_model\attribute\library\ConstAttribute;
use liberty_code\handle_model\attribute\api\AttributeInterface;



class CollectionValueInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $value
     */
	public function __construct($value) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstAttribute::EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT, strval($value));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified value has valid format.
	 * 
     * @param mixed $value
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($value)
    {
		// Init var
		$result = (
		    (!is_null($value)) &&
            ($value instanceof AttributeInterface)
        );
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($value);
		}
		
		// Return result
		return $result;
    }
	
	
	
}