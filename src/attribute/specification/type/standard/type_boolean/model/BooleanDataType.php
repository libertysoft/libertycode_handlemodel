<?php
/**
 * This class allows to define boolean data type class.
 * Boolean data type is data type,
 * which allows to manage entity attribute boolean value.
 * Can be consider is base of all boolean data type types.
 *
 * Boolean data type uses the following specified configuration:
 * [
 *     Default data type configuration,
 *
 *     empty_value(optional: got null, if not found): mixed value,
 *
 *     multiple_require(optional: got false, if not found):
 *         true / false
 *         If true, consider entity attribute value as index array of boolean values,
 *
 *     multiple_unique_require(optional: got false, if not found):
 *         true / false
 *         In case multiple required, if true, each value must be unique,
 *
 *     format_list_value_require(optional: got true, if not found): true / false,
 *
 *     format_get_empty_value(optional: got configuration empty_value, if not found): mixed value,
 *
 *     format_set_boolean_value_require(optional: got true, if not found): true / false,
 *
 *     format_set_empty_value(optional: got "", if not found): mixed value,
 *
 *     save_format_get_boolean_value_require(optional: got true, if not found): true / false,
 *
 *     save_format_get_empty_value(optional: got "", if not found): mixed value,
 *
 *     save_format_get_multiple_value_require(optional: got true, if not found): true / false,
 *
 *     save_format_set_boolean_value_require(optional: got true, if not found): true / false,
 *
 *     save_format_set_empty_value(optional: got "", if not found): mixed value,
 *
 *     save_format_set_multiple_value_require(optional: got true, if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\standard\type_boolean\model;

use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_boolean\library\ConstBooleanDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_boolean\exception\ConfigInvalidFormatException;



class BooleanDataType extends DefaultDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        //$result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDataType::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check multiple required.
     * If true, entity attribute value is considered as index array of boolean values.
     *
     * @return boolean
     */
    public function checkMultipleRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) &&
            (intval($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check multiple unique required.
     * In case multiple required, if true, each value must be unique.
     *
     * @return boolean
     */
    public function checkMultipleUniqueRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) &&
            (intval($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting list values required.
     *
     * @return boolean
     */
    public function checkListValueFormatRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute boolean value required,
     * when set action required.
     *
     * @return boolean
     */
    public function checkBoolValueFormatSetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_FORMAT_SET_BOOLEAN_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_FORMAT_SET_BOOLEAN_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute boolean value required,
     * when get action required, to be saved.
     *
     * @return boolean
     */
    public function checkBoolValueSaveFormatGetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_BOOLEAN_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_BOOLEAN_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute multiple value required,
     * when get action required, to be saved.
     *
     * @return boolean
     */
    public function checkMultiValueSaveFormatGetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute boolean value required,
     * when set action required, to be loaded.
     *
     * @return boolean
     */
    public function checkBoolValueSaveFormatSetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_BOOLEAN_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_BOOLEAN_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute multiple value required,
     * when set action required, to be loaded.
     *
     * @return boolean
     */
    public function checkMultiValueSaveFormatSetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get empty value.
     *
     * @return mixed
     */
    protected function getEmptyValue()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstBooleanDataType::TAB_CONFIG_KEY_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_EMPTY_VALUE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get entity attribute rule configuration array,
     * for boolean value.
     * Overwrite it to set specific validation.
     *
     * Return format:
     * @see getTabRuleConfig() return array format.
     *
     * @return array
     */
    protected function getTabValidBooleanRuleConfig()
    {
        // Init var
        $result = array(
            [
                'type_boolean',
                [
                    'integer_enable_require' => false,
                    'string_enable_require' => false
                ]
            ]
        );

        // Return result
        return $result;
    }



    /**
     * Get entity attribute rule configuration array,
     * for empty value.
     *
     * Return format:
     * @see getTabRuleConfig() return array format.
     *
     * @return array
     */
    protected function getTabEmptyRuleConfig()
    {
        // Return result
        return array(
            [
                'compare_equal',
                ['compare_value' => $this->getEmptyValue()]
            ]
        );
    }



    /**
     * Get specified index array of formatted list values.
     * Overwrite it to set specific formatting.
     *
     * @param array $tabListValue
     * @return array
     */
    protected function getTabListValueFormat(array $tabListValue)
    {
        // Return result
        return array_map(
            function($value) {
                return $this->getBoolValueFormatSet($value);
            },
            array_values($tabListValue)
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabRuleConfig(
        $boolValueRequired = false,
        array $tabListValue = array()
    )
    {
        // Init var
        $boolValueRequired = (is_bool($boolValueRequired) ? $boolValueRequired : false);
        $tabListValue = (
            $this->checkListValueFormatRequired() ?
                $this->getTabListValueFormat($tabListValue) :
                array_values($tabListValue)
        );
        $tabEmptyRuleConfig = $this->getTabEmptyRuleConfig();
        $tabValidBooleanRuleConfig = $this->getTabValidBooleanRuleConfig();
        $tabValidBooleanRuleConfig[] = array(
            'sub_rule_not',
            [
                'rule_config' => $tabEmptyRuleConfig,
                'error_message_pattern' => '%1$s is empty.'
            ]
        );
        if(count($tabListValue) > 0) {
            $tabValidBooleanRuleConfig[] = array(
                'compare_in',
                ['compare_value' => $tabListValue]
            );
        }

        // Build rule configuration
        $result = (
            $this->checkMultipleRequired() ?
                // Check multiple value
                array_merge(
                    array(
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => $tabValidBooleanRuleConfig,
                                'error_message_pattern' => '%1$s must be an array of valid booleans.'
                            ]
                        ]
                    ),
                    (
                        $boolValueRequired ?
                            array(
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty'],
                                        'error_message_pattern' => '%1$s is empty.'
                                    ]
                                ]
                            ) :
                            array()
                    ),
                    (
                        $this->checkMultipleUniqueRequired() ?
                            array(
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function($strName, $value) {
                                            return (
                                                (!is_array($value)) ||
                                                (count($value) == count(array_unique($value)))
                                            );
                                        },
                                        'error_message_pattern' => '%1$s must contain unique values.'
                                    ]
                                ]
                            ) :
                            array()
                    )
                ) :
                // Check simple value
                (
                    $boolValueRequired ?
                        $tabValidBooleanRuleConfig :
                        array(
                            [
                                'group_sub_rule_or',
                                [
                                    'rule_config' => [
                                        'is-empty' => $tabEmptyRuleConfig,
                                        'is-valid-boolean' => $tabValidBooleanRuleConfig
                                    ],
                                    'error_message_pattern' => '%1$s must be empty or a valid boolean.'
                                ]
                            ]
                        )
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when get action required.
     *
     * @return mixed
     */
    protected function getEmptyValueFormatGet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstBooleanDataType::TAB_CONFIG_KEY_FORMAT_GET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_FORMAT_GET_EMPTY_VALUE] :
                $this->getEmptyValue()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getValueFormatGet($value)
    {
        // Init var
        $emptyValue = $this->getEmptyValue();
        $emptyValueFormatGet = $this->getEmptyValueFormatGet();
        $getValueFormatGet = function($value) use ($emptyValue, $emptyValueFormatGet) {
            return (
                ($value === $emptyValue) ?
                    $emptyValueFormatGet :
                    parent::getValueFormatGet($value)
            );
        };
        $result = (
            $this->checkMultipleRequired() ?
                // Format multiple value
                (
                    is_array($value) ?
                        array_map(
                            function($subValue) use ($getValueFormatGet) {
                                return $getValueFormatGet($subValue);
                            },
                            array_values($value)
                        ) :
                        parent::getValueFormatGet($value)
                ) :
                // Format simple value
                $getValueFormatGet($value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted boolean value,
     * when set action required.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getBoolValueFormatSet($value)
    {
        // Return result
        return (
            (
                is_numeric($value) &&
                in_array($value, array('1', '0', 1, 0))
            ) ?
                (intval($value) == 1) :
                parent::getValueFormatSet($value)
        );
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when set action required.
     *
     * @return mixed
     */
    protected function getEmptyValueFormatSet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstBooleanDataType::TAB_CONFIG_KEY_FORMAT_SET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_FORMAT_SET_EMPTY_VALUE] :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getValueFormatSet($value)
    {
        // Init var
        $boolBoolValueFormatSetRequired = $this->checkBoolValueFormatSetRequired();
        $emptyValue = $this->getEmptyValue();
        $emptyValueFormatSet = $this->getEmptyValueFormatSet();
        $getValueFormatSet = function($value) use ($boolBoolValueFormatSetRequired, $emptyValue, $emptyValueFormatSet) {
            return (
                ($value === $emptyValueFormatSet) ?
                    $emptyValue :
                    (
                        $boolBoolValueFormatSetRequired ?
                            $this->getBoolValueFormatSet($value) :
                            $value
                    )
            );
        };
        $result = (
            $this->checkMultipleRequired() ?
                // Format multiple value
                (
                    is_array($value) ?
                        array_map(
                            function($subValue) use ($getValueFormatSet) {
                                return $getValueFormatSet($subValue);
                            },
                            array_values($value)
                        ) :
                        parent::getValueFormatSet($value)
                ) :
                // Format simple value
                $getValueFormatSet($value)
        );


        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted boolean value,
     * when get action required, to be saved.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getBoolValueSaveFormatGet($value)
    {
        // Return result
        return (
            is_bool($value) ?
                ($value ? '1' : '0') :
                parent::getValueSaveFormatGet($value)
        );
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when get action required, to be saved.
     *
     * @return mixed
     */
    protected function getEmptyValueSaveFormatGet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_EMPTY_VALUE] :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted multiple value,
     * when get action required, to be saved.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getMultiValueSaveFormatGet($value)
    {
        // Return result
        return (
            is_array($value) ?
                json_encode($value) :
                parent::getValueSaveFormatGet($value)
        );
    }



    /**
     * @inheritdoc
     */
    public function getValueSaveFormatGet($value)
    {
        // Init var
        $boolBoolValueSaveFormatGetRequired = $this->checkBoolValueSaveFormatGetRequired();
        $emptyValue = $this->getEmptyValue();
        $emptyValueSaveFormatGet = $this->getEmptyValueSaveFormatGet();
        $getValueSaveFormatGet = function($value) use ($boolBoolValueSaveFormatGetRequired, $emptyValue, $emptyValueSaveFormatGet) {
            return (
                ($value === $emptyValue) ?
                    $emptyValueSaveFormatGet :
                    (
                        $boolBoolValueSaveFormatGetRequired ?
                            $this->getBoolValueSaveFormatGet($value) :
                            $value
                    )
            );
        };

        // Format multiple value, if required
        if($this->checkMultipleRequired())
        {
            if(is_array($value))
            {
                $result = array_map(
                    function($subValue) use ($getValueSaveFormatGet) {
                        return $getValueSaveFormatGet($subValue);
                    },
                    array_values($value)
                );
                $result = (
                    $this->checkMultiValueSaveFormatGetRequired() ?
                        $this->getMultiValueSaveFormatGet($result) :
                        $result
                );
            }
            else
            {
                $result = parent::getValueSaveFormatGet($value);
            }
        }
        // Else: format simple value
        else
        {
            $result = $getValueSaveFormatGet($value);
        }

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted boolean value,
     * when set action required, to be loaded.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getBoolValueSaveFormatSet($value)
    {
        // Return result
        return (
            is_string($value) ?
                ($value == '1') :
                parent::getValueSaveFormatSet($value)
        );
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when set action required, to be loaded.
     *
     * @return mixed
     */
    protected function getEmptyValueSaveFormatSet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstBooleanDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_EMPTY_VALUE] :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted multiple value,
     * when set action required, to be loaded.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getMultiValueSaveFormatSet($value)
    {
        // Return result
        return (
            is_array($resultValue = json_decode($value, true)) ?
                $resultValue :
                parent::getValueSaveFormatSet($value)
        );
    }



    /**
     * @inheritdoc
     */
    public function getValueSaveFormatSet($value)
    {
        // Init var
        $boolBoolValueSaveFormatSetRequired = $this->checkBoolValueSaveFormatSetRequired();
        $emptyValue = $this->getEmptyValue();
        $emptyValueSaveFormatSet = $this->getEmptyValueSaveFormatSet();
        $getValueSaveFormatSet = function($value) use ($boolBoolValueSaveFormatSetRequired, $emptyValue, $emptyValueSaveFormatSet) {
            return (
                ($value === $emptyValueSaveFormatSet) ?
                    $emptyValue :
                    (
                        $boolBoolValueSaveFormatSetRequired ?
                            $this->getBoolValueSaveFormatSet($value) :
                            $value
                    )
            );
        };

        // Format multiple value, if required
        if($this->checkMultipleRequired())
        {
            $result = (
                $this->checkMultiValueSaveFormatSetRequired() ?
                    $this->getMultiValueSaveFormatSet($value) :
                    parent::getValueSaveFormatSet($value)
            );
            if(is_array($result))
            {
                $result = array_map(
                    function($subValue) use ($getValueSaveFormatSet) {
                        return $getValueSaveFormatSet($subValue);
                    },
                    array_values($result)
                );
            }
        }
        // Else: format simple value
        else
        {
            $result = $getValueSaveFormatSet($value);
        }

        // Return result
        return $result;
    }



}