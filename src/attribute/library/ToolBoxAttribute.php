<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\handle_model\attribute\api\AttributeInterface;
use liberty_code\handle_model\attribute\api\AttributeCollectionInterface;



class ToolBoxAttribute extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of attributes,
     * from specified attributes,
     * and specified selection, if provided.
     *
     * Check is selected callable format:
     * boolean function(AttributeInterface $objAttribute):
     * Allows to check if specified attribute is selected.
     *
     * @param AttributeInterface[]|AttributeCollectionInterface $attribute
     * @param null|callable $callCheckIsSelect = null
     * @return AttributeInterface[]
     */
    public static function getTabAttribute(
        $attribute,
        callable $callCheckIsSelect = null
    )
    {
        // Init var
        /** @var AttributeInterface[] $tabAttribute */
        $tabAttribute = (
            ($attribute instanceof AttributeCollectionInterface) ?
                array_map(
                    function($strKey) use ($attribute) {return $attribute->getObjAttribute($strKey);},
                    $attribute->getTabAttributeKey()
                ) :
                (
                    is_array($attribute) ?
                        array_values(array_filter(
                            $attribute,
                            function($attribute) {return ($attribute instanceof AttributeInterface);}
                        )) :
                        array()
                )
        );
        $result = (
            (!is_null($callCheckIsSelect)) ?
                array_values(array_filter(
                    $tabAttribute,
                    function(AttributeInterface $objAttribute) use ($callCheckIsSelect) {
                        return $callCheckIsSelect($objAttribute);
                    }
                )) :
                $tabAttribute
        );

        // Return result
        return $result;
    }



    /**
     * Get attribute,
     * from specified attributes,
     * and specified attribute key.
     *
     * @param AttributeInterface[]|AttributeCollectionInterface $attribute
     * @param string $strKey
     * @return null|AttributeInterface
     */
    public static function getObjAttributeFromAttrKey(
        $attribute,
        $strKey
    )
    {
        // Init var
        $tabAttribute = static::getTabAttribute(
            $attribute,
            function(AttributeInterface $objAttribute) use ($strKey) {
                return (
                    is_string($strKey) &&
                    ($objAttribute->getStrAttributeKey() === $strKey)
                );
            }
        );
        $result = (isset($tabAttribute[0]) ? $tabAttribute[0] : null);

        // Return result
        return $result;
    }



    /**
     * Get index array of attributes,
     * from specified attributes,
     * and specified entity attribute configuration data.
     *
     * @param AttributeInterface[]|AttributeCollectionInterface $attribute
     * @param string $strKey
     * @param mixed $value
     * @return AttributeInterface[]
     */
    public static function getTabAttributeFromEntityAttrConfigData(
        $attribute,
        $strKey,
        $value
    )
    {
        // Return result
        return static::getTabAttribute(
            $attribute,
            function(AttributeInterface $objAttribute) use ($strKey, $value) {
                $tabConfig = $objAttribute->getTabEntityAttrConfig();

                return (
                    is_string($strKey) &&
                    array_key_exists($strKey, $tabConfig) &&
                    ($tabConfig[$strKey] === $value)
                );
            }
        );
    }



    /**
     * Get (first) attribute,
     * from specified attributes,
     * and specified entity attribute configuration data.
     *
     * @param AttributeInterface[]|AttributeCollectionInterface $attribute
     * @param string $strKey
     * @param mixed $value
     * @return null|AttributeInterface
     */
    public static function getObjAttributeFromEntityAttrConfigData(
        $attribute,
        $strKey,
        $value
    )
    {
        // Init var
        $tabAttribute = static::getTabAttributeFromEntityAttrConfigData(
            $attribute,
            $strKey,
            $value
        );
        $result = (isset($tabAttribute[0]) ? $tabAttribute[0] : null);

        // Return result
        return $result;
    }



}