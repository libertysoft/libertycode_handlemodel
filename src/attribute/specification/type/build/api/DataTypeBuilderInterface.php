<?php
/**
 * Description :
 * This class allows to describe behavior of data type builder class.
 * Data type builder allows to populate specified data type collection instance, with data types.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\build\api;

use liberty_code\handle_model\attribute\specification\type\api\DataTypeCollectionInterface;



interface DataTypeBuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified data type collection.
     *
     * @param DataTypeCollectionInterface $objDataTypeCollection
     * @param boolean $boolClear = true
     */
    public function hydrateDataTypeCollection(DataTypeCollectionInterface $objDataTypeCollection, $boolClear = true);
}