<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\factory\standard\library;



class ConstStandardDataTypeFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_DATETIME_FACTORY = 'objDateTimeFactory';



    // Type configuration
    const CONFIG_TYPE_STRING = 'string';
    const CONFIG_TYPE_NUMERIC = 'numeric';
    const CONFIG_TYPE_BOOLEAN = 'boolean';
    const CONFIG_TYPE_DATE = 'date';



}