<?php

// Init var
$strTestRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strTestRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load external library test
require_once($strTestRootAppPath . '/vendor/liberty_code/validation/test/validator/boot/ValidatorBootstrap.php');

// Load test
require_once($strTestRootAppPath . '/src/attribute/test/AttributeTest.php');
require_once($strTestRootAppPath . '/src/entity/test/TestAttrProvider.php');
require_once($strTestRootAppPath . '/src/entity/test/TestEntity.php');

// Use
use liberty_code\handle_model\attribute\repository\model\DefaultSaveAttributeCollection;
use liberty_code\handle_model\entity\test\TestAttrProvider;
use liberty_code\handle_model\entity\test\TestEntity;



// Init var
$objAttributeCollection = new DefaultSaveAttributeCollection();
$objAttrProvider = new TestAttrProvider(
    $objAttributeCollection,
    $objAttributeBuilder
);
$objParamEntity = new TestEntity(
    $objAttrProvider,
    array(),
    $objValidator
);



// Test check/get attribute
$tabKey = array(
    ['key' => 'attr_1', 'value' => 'Test 1'], // Found
    ['key' => 'attr_1', 'value' => ''], // Found, not valid
    ['key' => 'attr_2', 'value' => 7], // Found
    ['key' => 'attr_2', 'value' => 'test'], // Found, not valid
    ['key' => 'attr_2', 'value' => 3], // Found, not valid
    ['key' => 'attr_3', 'value' => true], // Found
    ['key' => 'attr_3', 'value' => 'test'], // Found, not valid
    ['key' => 'attr_3', 'value' => false], // Found
    ['key' => 'attr_4', 'value' => ''], // Found
    ['key' => 'attr_4', 'value' => 'test-7'], // Found, not valid
    ['key' => 'attr_4', 'value' => 'test-3'], // Found
    ['key' => 'attr_5', 'value' => false], // Not found
    ['key' => 10, 'value' => false] // Ko: Key in valid
);

foreach($tabKey as $data)
{
    $strKey = $data['key'];
    $value = $data['value'];
    echo('Test check, get attribute "'.strval($strKey).'": value "'.var_export($value, true).'":<br />');

    try{
        $objParamEntity->setAttributeValue($strKey, $value);

        $tabError = array();
        echo('Check attribute: <pre>');var_dump($objParamEntity->checkAttributeValid($strKey, $tabError));echo('</pre>');
        echo('Get attribute error: <pre>');var_dump($tabError);echo('</pre>');
        echo('Get attribute formatted: <pre>');var_dump($objParamEntity->getAttributeValue($strKey));echo('</pre>');
        echo('Get attribute without formatted: <pre>');var_dump($objParamEntity->getAttributeValue($strKey, false));echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get attribute value save
echo('Test get attribute value save: <pre>');var_dump($objParamEntity->getTabDataSave());echo('</pre>');

echo('<br /><br /><br />');


