<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/attribute/test/AttributeTest.php');

// Use
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\handle_model\attribute\library\ToolBoxAttribute;
use liberty_code\handle_model\attribute\repository\model\DefaultSaveAttributeCollection;
use liberty_code\handle_model\attribute\provider\standard\model\StandardAttrProvider;



// Init var
$objAttributeCollection = new DefaultSaveAttributeCollection();
$objAttrProvider = new StandardAttrProvider(
    $objAttributeCollection,
    array(
        'cache_require' => true,
        'cache_entity_attribute_config_key' => 'entity-attribute-config',
        'cache_entity_attribute_rule_config_key' => 'entity-attribute-rule-config',
        'cache_entity_attribute_value_format_get_key_pattern' => 'entity-attribute-%1$s-value-%2$s-format-get',
        'cache_entity_attribute_value_format_set_key_pattern' => 'entity-attribute-%1$s-value-%2$s-format-set',
        'cache_entity_attribute_value_save_format_get_key_pattern' => 'entity-attribute-%1$s-value-%2$s-save-format-get',
        'cache_entity_attribute_value_save_format_set_key_pattern' => 'entity-attribute-%1$s-value-%2$s-save-format-set'
    ),
    $objCacheRepo
);



// Init builder
$tabDataSrc = array(
    'attr1' => [
        'key' => 'attr_1',
        'attribute_config' => [
            'name' => 'Attr 1',
            'alias' => 'attr-1',
            'default_value' => 'text 1',
            'name_save' => 'attr_1',
        ],
        'data_type' => 'string',
        'value_required' => true
    ],
    'attr_2' => [
        'type' => 'save',
        'attribute_config' => [
            'name' => 'Attr 2',
            'alias' => 'attr-2',
            'default_value' => 5,
        ],
        'data_type' => 'integer',
        'value_required' => true,
        'rule_config' => [
            [
                'compare_between',
                [
                    'greater_compare_value' => 5,
                    'less_compare_value' => 10,
                ]
            ]
        ]
    ],
    [
        'type' => 'save',
        'key' => 'attr_3',
        'attribute_config' => [
            'name' => 'Attr 3',
            'alias' => 'attr-3',
            'default_value' => true,
        ],
        'data_type' => 'boolean'
    ],
    'attr_4' => [
        'data_type' => 'string',
        'list_value' => ['', 'test-1', 'test-2', 'test-3']
    ],
);
$objAttributeBuilder->setTabDataSrc($tabDataSrc);



// Test hydrate attribute collection
echo('Test hydrate attribute collection: <br />');

echo('Before hydrate: <pre>');
var_dump($objAttributeCollection->getTabAttributeKey());
echo('</pre>');

$objAttributeBuilder->hydrateAttributeCollection($objAttributeCollection);

echo('After hydrate: <pre>');
var_dump($objAttributeCollection->getTabAttributeKey());
echo('</pre>');

echo('<br /><br /><br />');



// Test get attribute
echo('Test get attribute: <br />');

foreach($objAttributeCollection->getTabAttributeKey() as $strAttributeKey)
{
    echo('Test get attribute key "'.$strAttributeKey.'": <br />');

    $objAttribute = $objAttributeCollection->getObjAttribute($strAttributeKey);

    echo('Get attribute class: <pre>');var_dump(get_class($objAttribute));echo('</pre>');
    echo('Check attribute key exists: <pre>');var_dump($objAttributeCollection->checkAttributeExists($strAttributeKey));echo('</pre>');
    echo('Get attribute key: <pre>');var_dump($objAttribute->getStrAttributeKey());echo('</pre>');
    echo('Get attribute configuration: <pre>');var_dump($objAttribute->getTabAttributeConfig());echo('</pre>');

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get attribute, from entity attribute configuration
$tabEntityAttrConfigData = array(
    ['test'], // Not found (key not exists)
    ['attr_3'], // Found
    ['test', 'attr-2'], // Not found (entity attribute config key not exists)
    [ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS, 'test'], // Not found (alias not exists)
    [ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS, 'attr-2'], // Found
    [ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE, 'test'], // Not found (save name not exists)
    [ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE, 'attr_2'], // Not found (entity attribute config key save name not exists)
    [ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE, 'attr_1'] // Found
);
echo('Test get attribute, from entity attribute configuration: <br />');

foreach($tabEntityAttrConfigData as $entityAttrConfigData)
{
    $strEntityAttrConfigKey = (isset($entityAttrConfigData[1]) ? $entityAttrConfigData[0] : null);
    $entityAttrConfigValue = (isset($entityAttrConfigData[1]) ? $entityAttrConfigData[1] : $entityAttrConfigData[0]);

    if(is_null($strEntityAttrConfigKey))
    {
        echo('Test get attribute, from key: "'.$entityAttrConfigValue.'": <br />');

        $objAttribute = ToolBoxAttribute::getObjAttributeFromAttrKey(
            $objAttributeCollection,
            $entityAttrConfigValue
        );

        if(!is_null($objAttribute))
        {
            echo('Get attribute key: <pre>');var_dump($objAttribute->getStrAttributeKey());echo('</pre>');
            echo('Get attribute configuration: <pre>');var_dump($objAttribute->getTabAttributeConfig());echo('</pre>');
        }
        else
        {
            echo('Attribute not found<br />');
        }
    }
    else
    {
        echo('Test get attribute, from "' . $strEntityAttrConfigKey . '": "' . strval($entityAttrConfigValue) . '": <br />');

        $tabAttribute = ToolBoxAttribute::getTabAttributeFromEntityAttrConfigData(
            $objAttributeCollection,
            $strEntityAttrConfigKey,
            $entityAttrConfigValue
        );
        foreach($tabAttribute as $objAttribute)
        {
            echo('Get attribute key, from attributes: <pre>');var_dump($objAttribute->getStrAttributeKey());echo('</pre>');
            echo('Get attribute configuration, from attributes: <pre>');var_dump($objAttribute->getTabAttributeConfig());echo('</pre>');
        }

        $objAttribute = ToolBoxAttribute::getObjAttributeFromEntityAttrConfigData(
            $objAttributeCollection,
            $strEntityAttrConfigKey,
            $entityAttrConfigValue
        );

        if(!is_null($objAttribute))
        {
            echo('Get attribute key: <pre>');var_dump($objAttribute->getStrAttributeKey());echo('</pre>');
            echo('Get attribute configuration: <pre>');var_dump($objAttribute->getTabAttributeConfig());echo('</pre>');
        }
        else
        {
            echo('Attribute not found<br />');
        }
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get attribute provider
echo('Test get attribute provider: <br />');

echo('Get attribute configuration: <pre>');
var_dump($objAttrProvider->getTabEntityAttrConfig());
echo('</pre>');

echo('Get rule configuration: <pre>');
var_dump($objAttrProvider->getTabEntityAttrRuleConfig());
echo('</pre>');

$tabAttrValue = array(
    'attr_1' => ['test', '', 7], // Ok
    'test' => [], // Ko: Not found
    'attr_2' => [7, 'test'], // Ok
    'attr_3' => [false, true, 0, 1, '0', '1', 7, 'test']
);
foreach($tabAttrValue as $strAttributeKey => $tabValue)
{
    foreach($tabValue as $value)
    {
        echo('Test value, for following attribute key "'.$strAttributeKey.'":');
        var_dump($value);
        echo('<br />');

        echo('Get format get value: <pre>');
        var_dump($objAttrProvider->getEntityAttrValueFormatGet($strAttributeKey, $value));
        echo('</pre>');

        echo('Get format set value: <pre>');
        var_dump($objAttrProvider->getEntityAttrValueFormatSet($strAttributeKey, $value));
        echo('</pre>');

        echo('Get save format get value: <pre>');
        var_dump($objAttrProvider->getEntityAttrValueSaveFormatGet($strAttributeKey, $value));
        echo('</pre>');

        echo('Get save format set value: <pre>');
        var_dump($objAttrProvider->getEntityAttrValueSaveFormatSet($strAttributeKey, $value));
        echo('</pre>');

        echo('<br />');
    }
}

echo('<br /><br /><br />');



// Test remove all attributes
$objAttributeCollection->removeAttributeAll();

echo('Test remove all attributes: <br />');

echo('Get attribute keys: <pre>');
var_dump($objAttributeCollection->getTabAttributeKey());
echo('</pre>');

echo('Get attribute configuration: <pre>');
var_dump($objAttrProvider->getTabEntityAttrConfig());
echo('</pre>');

echo('Get rule configuration: <pre>');
var_dump($objAttrProvider->getTabEntityAttrRuleConfig());
echo('</pre>');

$objCacheRepo->removeItemAll();

echo('Test cache clear: <br />');

echo('Get attribute configuration: <pre>');
var_dump($objAttrProvider->getTabEntityAttrConfig());
echo('</pre>');

echo('Get rule configuration: <pre>');
var_dump($objAttrProvider->getTabEntityAttrRuleConfig());
echo('</pre>');

echo('<br /><br /><br />');


