<?php
/**
 * Description :
 * This class allows to define default attribute provider class.
 * Can be consider is base of all attribute provider types.
 *
 * Default attribute provider uses the following specified configuration:
 * [
 *     cache_require(optional: got true if not found): true / false,
 *
 *     cache_entity_attribute_config_key(optional: got @see ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_CONFIG_KEY, if not found):
 *         "string key,
 *         used on cache repository,
 *         to store entity attribute configuration",
 *
 *     cache_entity_attribute_rule_config_key(optional: got @see ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_RULE_CONFIG_KEY, if not found):
 *         "string key,
 *         used on cache repository,
 *         to store entity attribute rule configuration",
 *
 *     cache_entity_attribute_value_format_get_key_pattern(optional: got @see ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_VALUE_FORMAT_GET_KEY_PATTERN, if not found):
 *         "string sprintf pattern,
 *         to build key used on cache repository,
 *         to store entity attribute formatted value, when get action required,
 *         where '%1$s' and '%2$s',
 *         replaced by specified attribute key, and specified value hash",
 *
 *     cache_entity_attribute_value_format_set_key_pattern(optional: got @see ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_VALUE_FORMAT_SET_KEY_PATTERN, if not found):
 *         "string sprintf pattern,
 *         to build key used on cache repository,
 *         to store entity attribute formatted value, when set action required,
 *         where '%1$s' and '%2$s',
 *         replaced by specified attribute key, and specified value hash",
 *
 *     cache_entity_attribute_value_save_format_get_key_pattern(optional: got @see ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_VALUE_SAVE_FORMAT_GET_KEY_PATTERN, if not found):
 *         "string sprintf pattern,
 *         to build key used on cache repository,
 *         to store entity attribute formatted value, when get action required, to be saved,
 *         where '%1$s' and '%2$s',
 *         replaced by specified attribute key, and specified value hash",
 *
 *     cache_entity_attribute_value_save_format_set_key_pattern(optional: got @see ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_VALUE_SAVE_FORMAT_SET_KEY_PATTERN, if not found):
 *         "string sprintf pattern,
 *         to build key used on cache repository,
 *         to store entity attribute formatted value, when set action required, to be loaded,
 *         where '%1$s' and '%2$s',
 *         replaced by specified attribute key, and specified value hash",
 *
 *     cache_set_config(optional): [
 *         @see RepositoryInterface::setTabItem() configuration array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\provider\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\handle_model\attribute\provider\api\AttrProviderInterface;

use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\cache\repository\library\ToolBoxRepository;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\handle_model\attribute\provider\library\ConstAttrProvider;
use liberty_code\handle_model\attribute\provider\exception\ConfigInvalidFormatException;
use liberty_code\handle_model\attribute\provider\exception\CacheRepoInvalidFormatException;
use liberty_code\handle_model\attribute\provider\exception\AttrKeyInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method null|RepositoryInterface getObjCacheRepo() Get cache repository object.
 * @method void setObjCacheRepo(null|RepositoryInterface $objCacheRepo) Set cache repository object.
 */
abstract class DefaultAttrProvider extends DefaultBean implements AttrProviderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param RepositoryInterface $objCacheRepo = null
     */
    public function __construct(
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init cache repository, if required
        if(!is_null($objCacheRepo))
        {
            $this->setObjCacheRepo($objCacheRepo);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAttrProvider::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstAttrProvider::DATA_KEY_DEFAULT_CONFIG] = $this->getTabFixConfig();
        }

        if(!$this->beanExists(ConstAttrProvider::DATA_KEY_DEFAULT_CACHE_REPO))
        {
            $this->__beanTabData[ConstAttrProvider::DATA_KEY_DEFAULT_CACHE_REPO] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAttrProvider::DATA_KEY_DEFAULT_CONFIG,
            ConstAttrProvider::DATA_KEY_DEFAULT_CACHE_REPO
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAttrProvider::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value, $this->getTabFixConfig());
                    break;

                case ConstAttrProvider::DATA_KEY_DEFAULT_CACHE_REPO:
                    CacheRepoInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check cache required.
     *
     * @return boolean
     */
    protected function checkCacheRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjCacheRepo())) &&
            (
                (!isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (intval($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_REQUIRE]) !== 0)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array.
     * Overwrite it to implement specific configuration.
     *
     * @return array
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array();
    }



    /**
     * Get specified cache key,
     * to store entity attribute configuration.
     *
     * @return string
     */
    protected function getStrCacheEntityAttrConfigKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_CONFIG_KEY]) ?
                $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_CONFIG_KEY] :
                ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_CONFIG_KEY
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of entity attribute configuration engine.
     *
     * Return array format:
     * @see getTabEntityAttrConfig() return array format.
     *
     * @return array
     */
    abstract protected function getTabEntityAttrConfigEngine();



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            $this->checkCacheRequired() ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $this->getStrCacheEntityAttrConfigKey(),
                    function() {return $this->getTabEntityAttrConfigEngine();},
                    (
                        isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getTabEntityAttrConfigEngine()
        );

        // Return result
        return $result;
    }



    /**
     * Get specified cache key,
     * to store entity attribute rule configuration.
     *
     * @return string
     */
    protected function getStrCacheEntityAttrRuleConfigKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_RULE_CONFIG_KEY]) ?
                $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_RULE_CONFIG_KEY] :
                ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_RULE_CONFIG_KEY
        );

        // Return result
        return $result;
    }



    /**
     * Get entity attribute rule configurations array engine.
     *
     * Return array format:
     * @see getTabEntityAttrRuleConfig() return array format.
     *
     * @return array
     */
    protected function getTabEntityAttrRuleConfigEngine()
    {
        return array();
    }



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrRuleConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            $this->checkCacheRequired() ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $this->getStrCacheEntityAttrRuleConfigKey(),
                    function() {return $this->getTabEntityAttrRuleConfigEngine();},
                    (
                        isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getTabEntityAttrRuleConfigEngine()
        );

        // Return result
        return $result;
    }



    /**
     * Get specified cache key,
     * to store entity attribute formatted value, when get action required,
     * from specified attribute key,
     * and specified value
     *
     * @param string $strKey
     * @param mixed $value
     * @return null|string
     */
    protected function getStrCacheEntityAttrValueFormatGetKey($strKey, $value)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_FORMAT_GET_KEY_PATTERN]) ?
                $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_FORMAT_GET_KEY_PATTERN] :
                ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_VALUE_FORMAT_GET_KEY_PATTERN
        );
        $result = (
            (
                is_string($strKey) &&
                is_string($strValueHash = ToolBoxHash::getStrHash($value))
            ) ?
                sprintf(
                    $strPattern,
                    $strKey,
                    $strValueHash
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted value engine,
     * when get action required.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getEntityAttrValueFormatGetEngine($strKey, $value)
    {
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueFormatGet($strKey, $value)
    {
        // Check attribute key format
        AttrKeyInvalidFormatException::setCheck($strKey);

        // Init var
        $tabConfig = $this->getTabConfig();
        $strCacheKey = $this->getStrCacheEntityAttrValueFormatGetKey($strKey, $value);
        $result = (
            ((!is_null($strCacheKey)) && $this->checkCacheRequired()) ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $strCacheKey,
                    function() use ($strKey, $value) {return $this->getEntityAttrValueFormatGetEngine($strKey, $value);},
                    (
                        isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getEntityAttrValueFormatGetEngine($strKey, $value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified cache key,
     * to store entity attribute formatted value, when set action required,
     * from specified attribute key,
     * and specified value
     *
     * @param string $strKey
     * @param mixed $value
     * @return null|string
     */
    protected function getStrCacheEntityAttrValueFormatSetKey($strKey, $value)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_FORMAT_SET_KEY_PATTERN]) ?
                $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_FORMAT_SET_KEY_PATTERN] :
                ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_VALUE_FORMAT_SET_KEY_PATTERN
        );
        $result = (
            (
                is_string($strKey) &&
                is_string($strValueHash = ToolBoxHash::getStrHash($value))
            ) ?
                sprintf(
                    $strPattern,
                    $strKey,
                    $strValueHash
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted value engine,
     * when set action required.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getEntityAttrValueFormatSetEngine($strKey, $value)
    {
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueFormatSet($strKey, $value)
    {
        // Check attribute key format
        AttrKeyInvalidFormatException::setCheck($strKey);

        // Init var
        $tabConfig = $this->getTabConfig();
        $strCacheKey = $this->getStrCacheEntityAttrValueFormatSetKey($strKey, $value);
        $result = (
            ((!is_null($strCacheKey)) && $this->checkCacheRequired()) ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $strCacheKey,
                    function() use ($strKey, $value) {return $this->getEntityAttrValueFormatSetEngine($strKey, $value);},
                    (
                        isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getEntityAttrValueFormatSetEngine($strKey, $value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified cache key,
     * to store entity attribute formatted value, when get action required, to be saved,
     * from specified attribute key,
     * and specified value
     *
     * @param string $strKey
     * @param mixed $value
     * @return null|string
     */
    protected function getStrCacheEntityAttrValueSaveFormatGetKey($strKey, $value)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_SAVE_FORMAT_GET_KEY_PATTERN]) ?
                $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_SAVE_FORMAT_GET_KEY_PATTERN] :
                ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_VALUE_SAVE_FORMAT_GET_KEY_PATTERN
        );
        $result = (
            (
                is_string($strKey) &&
                is_string($strValueHash = ToolBoxHash::getStrHash($value))
            ) ?
                sprintf(
                    $strPattern,
                    $strKey,
                    $strValueHash
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted value engine,
     * when get action required, to be saved.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getEntityAttrValueSaveFormatGetEngine($strKey, $value)
    {
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatGet($strKey, $value)
    {
        // Check attribute key format
        AttrKeyInvalidFormatException::setCheck($strKey);

        // Init var
        $tabConfig = $this->getTabConfig();
        $strCacheKey = $this->getStrCacheEntityAttrValueSaveFormatGetKey($strKey, $value);
        $result = (
            ((!is_null($strCacheKey)) && $this->checkCacheRequired()) ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $strCacheKey,
                    function() use ($strKey, $value) {return $this->getEntityAttrValueSaveFormatGetEngine($strKey, $value);},
                    (
                        isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getEntityAttrValueSaveFormatGetEngine($strKey, $value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified cache key,
     * to store entity attribute formatted value, when set action required, to be loaded,
     * from specified attribute key,
     * and specified value
     *
     * @param string $strKey
     * @param mixed $value
     * @return null|string
     */
    protected function getStrCacheEntityAttrValueSaveFormatSetKey($strKey, $value)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_SAVE_FORMAT_SET_KEY_PATTERN]) ?
                $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_SAVE_FORMAT_SET_KEY_PATTERN] :
                ConstAttrProvider::CACHE_DEFAULT_ENTITY_ATTR_VALUE_SAVE_FORMAT_SET_KEY_PATTERN
        );
        $result = (
            (
                is_string($strKey) &&
                is_string($strValueHash = ToolBoxHash::getStrHash($value))
            ) ?
                sprintf(
                    $strPattern,
                    $strKey,
                    $strValueHash
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted value engine,
     * when set action required, to be loaded.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getEntityAttrValueSaveFormatSetEngine($strKey, $value)
    {
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatSet($strKey, $value)
    {
        // Check attribute key format
        AttrKeyInvalidFormatException::setCheck($strKey);

        // Init var
        $tabConfig = $this->getTabConfig();
        $strCacheKey = $this->getStrCacheEntityAttrValueSaveFormatSetKey($strKey, $value);
        $result = (
            ((!is_null($strCacheKey)) && $this->checkCacheRequired()) ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $strCacheKey,
                    function() use ($strKey, $value) {return $this->getEntityAttrValueSaveFormatSetEngine($strKey, $value);},
                    (
                        isset($tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrProvider::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getEntityAttrValueSaveFormatSetEngine($strKey, $value)
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setTabConfig(array $tabConfig)
    {
        // Format configuration
        $tabConfig = array_merge($this->getTabFixConfig(), $tabConfig);

        // Set configuration
        $this->beanSet(ConstAttrProvider::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}