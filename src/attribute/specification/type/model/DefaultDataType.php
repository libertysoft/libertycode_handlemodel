<?php
/**
 * Description :
 * This class allows to define default data type class.
 * Can be consider is base of all data type types.
 *
 * Default data type uses the following specified configuration:
 * [
 *     key(optional: got @see getStrHash(), if not found): "string data type key",
 *
 *     type(required): "string type" OR ["string type 1", ..., "string type N"],
 *
 *     order(optional: got 0, if not found): integer
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;

use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\exception\ConfigInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 */
abstract class DefaultDataType extends FixBean implements DataTypeInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstDataType::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstDataType::DATA_KEY_DEFAULT_CONFIG] = $this->getTabFixConfig();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstDataType::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDataType::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value, $this->getTabFixConfig());
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkMatches($strType)
    {
        // Return result
        return (
            is_string($strType) &&
            in_array($strType, $this->getTabType())
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array.
     * Overwrite it to implement specific configuration.
     *
     * @return array
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array();
    }



    /**
     * Get string hash.
     *
     * @return string
     */
    public function getStrHash()
    {
        // Return result
        return sprintf(ConstDataType::HASH_PATTERN, ToolBoxHash::getStrHash($this));
    }



    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDataType::TAB_CONFIG_KEY_KEY]) ?
                $tabConfig[ConstDataType::TAB_CONFIG_KEY_KEY] :
                $this->getStrHash()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabType()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            is_array($tabConfig[ConstDataType::TAB_CONFIG_KEY_TYPE]) ?
                array_values($tabConfig[ConstDataType::TAB_CONFIG_KEY_TYPE]) :
                array($tabConfig[ConstDataType::TAB_CONFIG_KEY_TYPE])
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getIntOrder()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDataType::TAB_CONFIG_KEY_ORDER]) ?
                $tabConfig[ConstDataType::TAB_CONFIG_KEY_ORDER] :
                0
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getValueFormatGet($value)
    {
        // Return result
        return $value;
    }



    /**
     * @inheritdoc
     */
    public function getValueFormatSet($value)
    {
        // Return result
        return $value;
    }



    /**
     * @inheritdoc
     */
    public function getValueSaveFormatGet($value)
    {
        // Return result
        return $value;
    }



    /**
     * @inheritdoc
     */
    public function getValueSaveFormatSet($value)
    {
        // Return result
        return $value;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setTabConfig(array $tabConfig)
    {
        // Format configuration
        $tabConfig = array_merge($this->getTabFixConfig(), $tabConfig);

        // Set configuration
        $this->beanSet(ConstDataType::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}