<?php
/**
 * Description :
 * This class allows to define standard attribute factory class.
 * Standard attribute factory allows to provide and hydrate attribute instance.
 *
 * Standard attribute factory uses the following specified configuration, to get and hydrate attribute:
 * [
 *     -> Configuration key(optional): "attribute key"
 *     type(optional): "default",
 *     Default attribute configuration (@see DefaultAttribute )
 *
 *     OR
 *
 *     -> Configuration key(optional): "attribute key"
 *     type(required): "save",
 *     Default save attribute configuration (@see DefaultSaveAttribute )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\factory\standard\model;

use liberty_code\handle_model\attribute\factory\model\DefaultAttributeFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\handle_model\attribute\library\ConstAttribute;
use liberty_code\handle_model\attribute\api\AttributeInterface;
use liberty_code\handle_model\attribute\model\DefaultAttribute;
use liberty_code\handle_model\attribute\repository\model\DefaultSaveAttribute;
use liberty_code\handle_model\attribute\factory\api\AttributeFactoryInterface;
use liberty_code\handle_model\attribute\factory\standard\library\ConstStandardAttributeFactory;



class StandardAttributeFactory extends DefaultAttributeFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Attribute specification object
     * @var null|AttrSpecInterface
     */
    protected $objAttrSpec;
	




	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttrSpecInterface $objAttrSpec = null
     */
    public function __construct(
        AttributeFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null,
        AttrSpecInterface $objAttrSpec = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objFactory,
            $objProvider
        );

        // Set attribute specification
        $this->setAttrSpec($objAttrSpec);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function hydrateAttribute(AttributeInterface $objAttribute, array $tabConfigFormat)
    {
        // Hydrate attribute, if required
        if(
            ($objAttribute instanceof DefaultAttribute) ||
            ($objAttribute instanceof DefaultSaveAttribute)
        )
        {
            $objAttribute->setAttrSpec($this->objAttrSpec);
        }

        // Call parent method
        parent::hydrateAttribute($objAttribute, $tabConfigFormat);
    }





	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Add configured key as attribute key, if required
            if(!array_key_exists(ConstAttribute::TAB_CONFIG_KEY_KEY, $result))
            {
                $result[ConstAttribute::TAB_CONFIG_KEY_KEY] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrAttributeClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of permission, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardAttributeFactory::CONFIG_TYPE_DEFAULT:
                $result = DefaultAttribute::class;
                break;

            case ConstStandardAttributeFactory::CONFIG_TYPE_SAVE:
                $result = DefaultSaveAttribute::class;
                break;
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set attribute specification object.
     *
     * @param AttrSpecInterface $objAttrSpec = null
     */
    public function setAttrSpec(AttrSpecInterface $objAttrSpec = null)
    {
        // Set data
        $this->objAttrSpec = $objAttrSpec;
    }



}