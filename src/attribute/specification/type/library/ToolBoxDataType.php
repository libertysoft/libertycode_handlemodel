<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeCollectionInterface;



class ToolBoxDataType extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of string types,
     * from specified data type collection.
     *
     * Sort option can be provided:
     * @see DataTypeCollectionInterface::getTabKey() sort option.
     *
     * @param DataTypeCollectionInterface $objDataTypeCollection
     * @param null|boolean $sortAscRequired = null
     * @return array
     */
    public static function getTabType(
        DataTypeCollectionInterface $objDataTypeCollection,
        $sortAscRequired = null
    )
    {
        // Init var
        $result = array();
        $tabKey = $objDataTypeCollection->getTabKey($sortAscRequired);

        // Run each data type
        foreach($tabKey as $strKey)
        {
            // Get types
            $objDataType = $objDataTypeCollection->getObjDataType($strKey);
            $tabType = $objDataType->getTabType();

            // Register types
            $result = array_unique(array_merge($result, $tabType));
        }

        // Return result
        return $result;
    }



    /**
     * Get associative array of data types,
     * which match with specified type,
     * from specified data type collection.
     *
     * Sort option can be provided:
     * @see DataTypeCollectionInterface::getTabKey() sort option.
     *
     * Return array format:
     * [key: data type key => value: data type object]
     *
     * @param DataTypeCollectionInterface $objDataTypeCollection
     * @param string $strType
     * @param null|boolean $sortAscRequired = null
     * @return array
     */
    public static function getTabDataTypeFromType(
        DataTypeCollectionInterface $objDataTypeCollection,
        $strType,
        $sortAscRequired = null
    )
    {
        // Init var
        $result = array();
        $tabKey = $objDataTypeCollection->getTabKey($sortAscRequired);

        // Run each data type
        foreach($tabKey as $strKey)
        {
            // Get data type
            $objDataType = $objDataTypeCollection->getObjDataType($strKey);

            // Register data type, if required
            if($objDataType->checkMatches($strType))
            {
                $result[$strKey] = $objDataType;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get data type,
     * which matches with specified type,
     * from specified data type collection.
     *
     * Sort option can be provided:
     * @see getTabDataTypeFromType() sort option.
     *
     * First option can be provided:
     * - True: on same order, first match taken.
     * - False: on same order, last match taken.
     *
     * @param DataTypeCollectionInterface $objDataTypeCollection
     * @param string $strType
     * @param null|boolean $sortAscRequired = null
     * @param boolean $boolFirstRequired = false
     * @return null|DataTypeInterface
     */
    public static function getObjDataTypeFromType(
        DataTypeCollectionInterface $objDataTypeCollection,
        $strType,
        $sortAscRequired = null,
        $boolFirstRequired = false
    )
    {
        // Init var
        $boolFirstRequired = (is_bool($boolFirstRequired) ? $boolFirstRequired : false);
        $tabDataType = array_values(static::getTabDataTypeFromType(
            $objDataTypeCollection,
            $strType,
            $sortAscRequired
        ));
        $intDataTypeCount = count($tabDataType);
        $result = (
            $intDataTypeCount > 0 ?
                (
                    $boolFirstRequired ?
                        $tabDataType[0] :
                        $tabDataType[($intDataTypeCount - 1)]
                ) :
                null
        );

        // Return result
        return $result;
    }



}