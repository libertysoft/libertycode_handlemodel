<?php
/**
 * This class allows to define default attribute class.
 * Can be consider is base of all attribute types.
 *
 * Default attribute uses the following specified configuration:
 * [
 *     key(required): "string attribute key",
 *
 *     attribute_config(optional: got [], if not found): [
 *         Entity attribute configuration array format: @see ValidatorConfigEntity for one attribute
 *         (attribute key not required)
 *     ],
 *
 *     data_type(required): "string attribute type",
 *
 *     value_required(optional: got false if use not found): true / false,
 *
 *     list_value(optional): [mixed value 1, ..., mixed value N]
 *
 *     rule_config(optional: got [], if not found): [
 *         Entity attribute rule configuration array format:
 *         @see AttributeInterface::getTabEntityAttrRuleConfig() return array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\handle_model\attribute\api\AttributeInterface;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\handle_model\attribute\library\ConstAttribute;
use liberty_code\handle_model\attribute\exception\ConfigInvalidFormatException;



class DefaultAttribute extends FixBean implements AttributeInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Attribute specification object
     * @var null|AttrSpecInterface
     */
    protected $objAttrSpec;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param AttrSpecInterface $objAttrSpec = null
     */
    public function __construct(
        array $tabConfig = null,
        AttrSpecInterface $objAttrSpec = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Set attribute specification
        $this->setAttrSpec($objAttrSpec);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setAttributeConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAttribute::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstAttribute::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAttribute::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAttribute::DATA_KEY_DEFAULT_CONFIG:
                    $objAttrSpec = $this->getObjAttrSpec();
                    ConfigInvalidFormatException::setCheck($value, $objAttrSpec);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check attribute value required.
     *
     * @return boolean
     */
    public function checkAttributeValueRequired()
    {
        // Init var
        $tabConfig = $this->getTabAttributeConfig();
        $result = (
            array_key_exists(ConstAttribute::TAB_CONFIG_KEY_VALUE_REQUIRED, $tabConfig) &&
            (intval($tabConfig[ConstAttribute::TAB_CONFIG_KEY_VALUE_REQUIRED]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get attribute specification object.
     *
     * @return null|AttrSpecInterface
     */
    public function getObjAttrSpec()
    {
        // Return result
        return $this->objAttrSpec;
    }



    /**
     * @inheritdoc
     */
    public function getTabAttributeConfig()
    {
        // Return result
        return $this->beanGet(ConstAttribute::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * @inheritdoc
     */
    public function getStrAttributeKey()
    {
        // Init var
        $tabConfig = $this->getTabAttributeConfig();
        $result = $tabConfig[ConstAttribute::TAB_CONFIG_KEY_KEY];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrConfig()
    {
        // Init var
        $tabConfig = $this->getTabAttributeConfig();
        $tabAttributeConfig = (
            isset($tabConfig[ConstAttribute::TAB_CONFIG_KEY_ATTRIBUTE_CONFIG]) ?
                $tabConfig[ConstAttribute::TAB_CONFIG_KEY_ATTRIBUTE_CONFIG] :
                array()
        );
        $strAttributeKey = $this->getStrAttributeKey();
        $result = array_merge(
            $tabAttributeConfig,
            array(ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => $strAttributeKey)
        );

        // Return result
        return $result;
    }



    /**
     * Get string attribute data type.
     *
     * @return string
     */
    public function getStrAttributeDataType()
    {
        // Init var
        $tabConfig = $this->getTabAttributeConfig();
        $result = $tabConfig[ConstAttribute::TAB_CONFIG_KEY_DATA_TYPE];

        // Return result
        return $result;
    }



    /**
     * Get string attribute list value.
     *
     * @return array
     */
    public function getTabAttributeListValue()
    {
        // Init var
        $tabConfig = $this->getTabAttributeConfig();
        $result = (
            isset($tabConfig[ConstAttribute::TAB_CONFIG_KEY_LIST_VALUE]) ?
                $tabConfig[ConstAttribute::TAB_CONFIG_KEY_LIST_VALUE] :
                array()
        );

        // Return result
        return $result;
    }



    /**
     * Get entity attribute rule configuration array,
     * from attribute data type.
     *
     * Return format:
     * @see AttrSpecInterface::getTabDataTypeRuleConfig() return array format.
     *
     * @return array
     */
    protected function getTabDataTypeRuleConfig()
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $strDataType = $this->getStrAttributeDataType();
        $boolValueRequired = $this->checkAttributeValueRequired();
        $tabListValue = $this->getTabAttributeListValue();
        $tabListValue = (
            ToolBoxTable::checkTabIsIndex($tabListValue) ?
                array_values($tabListValue) :
                array_keys($tabListValue)
        );
        $result = $objAttrSpec->getTabDataTypeRuleConfig(
            $strDataType,
            $boolValueRequired,
            $tabListValue
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrRuleConfig()
    {
        // Init var
        $tabConfig = $this->getTabAttributeConfig();
        $tabDataTypeRuleConfig = $this->getTabDataTypeRuleConfig();
        $tabRuleConfig = (
            isset($tabConfig[ConstAttribute::TAB_CONFIG_KEY_RULE_CONFIG]) ?
                $tabConfig[ConstAttribute::TAB_CONFIG_KEY_RULE_CONFIG] :
                array()
        );
        $result = array_merge(
            $tabDataTypeRuleConfig,
            $tabRuleConfig
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueFormatGet($value)
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $strDataType = $this->getStrAttributeDataType();
        $result = (
            (!is_null($objAttrSpec)) ?
                $objAttrSpec->getDataTypeValueFormatGet($strDataType, $value) :
                $value
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueFormatSet($value)
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $strDataType = $this->getStrAttributeDataType();
        $result = (
            (!is_null($objAttrSpec)) ?
                $objAttrSpec->getDataTypeValueFormatSet($strDataType, $value) :
                $value
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set attribute specification object.
     *
     * @param AttrSpecInterface $objAttrSpec = null
     */
    public function setAttrSpec(AttrSpecInterface $objAttrSpec = null)
    {
        // Set data
        $this->objAttrSpec = $objAttrSpec;
    }



    /**
     * @inheritdoc
     */
    public function setAttributeConfig(array $tabConfig)
    {
        $this->beanSet(ConstAttribute::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}