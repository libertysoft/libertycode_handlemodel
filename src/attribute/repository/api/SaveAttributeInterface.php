<?php
/**
 * Description :
 * This class allows to describe behavior of save attribute class.
 * Save attribute is attribute, used on save entity.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\repository\api;

use liberty_code\handle_model\attribute\api\AttributeInterface;



interface SaveAttributeInterface extends AttributeInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get specified entity attribute formatted value when get action required, to be saved.
     *
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueSaveFormatGet($value);



    /**
     * Get specified entity attribute formatted value when set action required, to be loaded.
     *
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueSaveFormatSet($value);
}