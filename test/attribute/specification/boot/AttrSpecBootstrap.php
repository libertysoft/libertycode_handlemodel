<?php

// Use
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataTypeCollection;
use liberty_code\handle_model\attribute\specification\standard\model\StandardAttrSpec;



// Init cache repository
$objCacheRepo = new DefaultRepository(
    null,
    new DefaultTableRegister()
);

// Init data type collection
$objDataTypeCollection = new DefaultDataTypeCollection();

// Init var
$tabAttrSpecConfig = array(
    'cache_require' => true,
    'cache_list_data_type_key' => 'attribute-spec-data-type',
    'cache_rule_config_key_pattern' => 'attribute-spec-rule-config-%1$s-%2$s-%3$s',
    'cache_value_format_get_key_pattern' => 'attribute-spec-value-format-get-%1$s-%2$s',
    'cache_value_format_set_key_pattern' => 'attribute-spec-value-format-set-%1$s-%2$s',
    'cache_value_save_format_get_key_pattern' => 'attribute-spec-value-save-format-get-%1$s-%2$s',
    'cache_value_save_format_set_key_pattern' => 'attribute-spec-value-save-format-set-%1$s-%2$s',
);
$objAttrSpec = new StandardAttrSpec(
    $objDataTypeCollection,
    $tabAttrSpecConfig,
    $objCacheRepo
);


