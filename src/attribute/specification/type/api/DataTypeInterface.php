<?php
/**
 * Description :
 * This class allows to describe behavior of data type class.
 * Data type allows to design a specific type,
 * which allows to manage entity attribute value (validation, formatting).
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\api;

use liberty_code\model\entity\model\ValidatorConfigEntity;



interface DataTypeInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified type matches,
     * with this data type.
     *
     * @param string $strType
     * @return boolean
     */
    public function checkMatches($strType);





	// Methods getters
	// ******************************************************************************

    /**
     * Get string key (considered as data type id).
     *
     * @return string
     */
    public function getStrKey();



    /**
     * Get index array of string types,
     * matching with this data type.
     *
     * @return array
     */
    public function getTabType();



    /**
     * Get integer order,
     * allowing to sort this data type, with others.
     * Smallest value: highest priority.
     *
     * @return integer
     */
    public function getIntOrder();



    /**
     * Get entity attribute rule configuration array.
     *
     * List values array format:
     * Index array of mixed values.
     * Allows to include a set of potential values,
     * on rule configuration.
     * Empty array considered has no list value required.
     *
     * Return array format:
     * Attribute rule configuration array format,
     * from @see ValidatorConfigEntity::getTabRuleConfig() return array format.
     *
     * @param boolean $boolValueRequired = false
     * @param array $tabListValue = array()
     * @return array
     */
    public function getTabRuleConfig(
        $boolValueRequired = false,
        array $tabListValue = array()
    );



    /**
     * Get specified entity attribute formatted value,
     * when get action required.
     *
     * @param mixed $value
     * @return mixed
     */
    public function getValueFormatGet($value);



    /**
     * Get specified entity attribute formatted value,
     * when set action required.
     *
     * @param mixed $value
     * @return mixed
     */
    public function getValueFormatSet($value);



    /**
     * Get specified entity attribute formatted value,
     * when get action required, to be saved.
     *
     * @param mixed $value
     * @return mixed
     */
    public function getValueSaveFormatGet($value);



    /**
     * Get specified entity attribute formatted value,
     * when set action required, to be loaded.
     *
     * @param mixed $value
     * @return mixed
     */
    public function getValueSaveFormatSet($value);
}