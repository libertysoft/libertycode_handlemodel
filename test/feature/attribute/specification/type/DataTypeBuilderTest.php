<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\test\feature;

use PHPUnit\Framework\TestCase;

use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataTypeCollection;
use liberty_code\handle_model\attribute\specification\type\standard\type_string\model\StringDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\model\NumericDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_boolean\model\BooleanDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\model\DateDataType;
use liberty_code\handle_model\attribute\specification\type\build\exception\DataSrcInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\build\api\DataTypeBuilderInterface;
use liberty_code\handle_model\attribute\specification\type\build\model\DefaultDataTypeBuilder;



/**
 * @cover DataTypeBuilderInterface
 * @cover DefaultDataTypeBuilder
 */
class DataTypeBuilderTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can hydrate data type collection object,
     * from array of source data.
     *
     * @param array $tabDataSrc
     * @param string|array $expectResult
     * @dataProvider providerHydrateDataTypeCollectionFromDataSrc
     */
    public function testCanHydrateDataTypeCollectionFromDataSrc(
        array $tabDataSrc,
        $expectResult
    )
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/../../../..';
        require($strRootAppPath . '/attribute/specification/type/build/boot/DataTypeBuilderBootstrap.php');

        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get hydrated data type collection
        /** @var DefaultDataTypeBuilder $objDataTypeBuilder */
        $objDataTypeCollection = new DefaultDataTypeCollection();
        $objDataTypeBuilder->setTabDataSrc($tabDataSrc);
        $objDataTypeBuilder->hydrateDataTypeCollection($objDataTypeCollection);

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Set assertions (check data type count)
            $tabKey = $objDataTypeCollection->getTabKey();
            $this->assertSame(count($expectResult), count($tabKey));

            // Run each data type
            for($intCpt = 0; $intCpt < count($tabKey); $intCpt++)
            {
                // Get info
                $strExpectClassPath = $expectResult[$intCpt][0];
                $tabExpectConfig = $expectResult[$intCpt][1];
                $strExpectKey = $expectResult[$intCpt][2];

                // Set assertions (check data type detail)
                $strKey = $tabKey[$intCpt];
                /** @var DefaultDataType $objDataType */
                $objDataType = $objDataTypeCollection->getObjDataType($strKey);
                $tabDataTypeConfig = $objDataType->getTabConfig();
                $strDataTypeKey = $objDataType->getStrKey();
                $this->assertSame($strExpectClassPath, get_class($objDataType));
                $this->assertEquals($tabExpectConfig, $tabDataTypeConfig);
                if(!is_null($strExpectKey))
                {
                    $this->assertSame($strExpectKey, $strDataTypeKey);
                }

                // Print
                /*
                echo('Get class path: '. PHP_EOL);var_dump(get_class($objDataType));echo(PHP_EOL);
                echo('Get config: ' . PHP_EOL);var_dump($tabDataTypeConfig);echo(PHP_EOL);
                echo('Get key: ' . PHP_EOL);var_dump($strDataTypeKey);echo(PHP_EOL);
                //*/
            }
        }
    }



    /**
     * Data provider,
     * to test can hydrate data type collection object,
     * from array of source data.
     *
     * @return array
     */
    public function providerHydrateDataTypeCollectionFromDataSrc()
    {
        // Return result
        return array(
            'Hydrate data type collection, from data source: fail to hydrate data type collection (invalid data source format: invalid config format)' => [
                [
                    'key_test_1' => [
                        'type' => 'string',
                        'config' => [
                            'type' => 'string'
                        ]
                    ],
                    [
                        'type' => 'numeric',
                        'config' => [
                            'key' => 'key_test_2',
                            'type' => 'numeric'
                        ]
                    ],
                    'key_test_3' => true,
                    [
                        'type' => 'date',
                        'config' => [
                            'type' => 'date'
                        ]
                    ]
                ],
                DataSrcInvalidFormatException::class
            ],
            'Hydrate data type collection, from data source: fail to hydrate data type collection (invalid data source format: impossible to create data type)' => [
                [
                    'key_test_1' => [
                        'type' => 'string',
                        'config' => [
                            'type' => 'string'
                        ]
                    ],
                    [
                        'type' => 'test',
                        'config' => [
                            'key' => 'key_test_2',
                            'type' => 'numeric'
                        ]
                    ],
                    'key_test_3' => [
                        'type' => 'boolean',
                        'config' => [
                            'key' => 'key_test_3_upd',
                            'type' => 'boolean'
                        ]
                    ],
                    [
                        'type' => 'date',
                        'config' => [
                            'type' => 'date'
                        ]
                    ]
                ],
                DataSrcInvalidFormatException::class
            ],
            'Hydrate data type collection, from data source: success to hydrate data type collection' => [
                [
                    'key_test_1' => [
                        'type' => 'string',
                        'config' => [
                            'type' => 'string'
                        ]
                    ],
                    [
                        'type' => 'numeric',
                        'config' => [
                            'key' => 'key_test_2',
                            'type' => 'numeric'
                        ]
                    ],
                    'key_test_3' => [
                        'type' => 'boolean',
                        'config' => [
                            'key' => 'key_test_3_upd',
                            'type' => 'boolean'
                        ]
                    ],
                    [
                        'type' => 'date',
                        'config' => [
                            'type' => 'date'
                        ]
                    ]
                ],
                [
                    [
                        StringDataType::class,
                        [
                            'key' => 'key_test_1',
                            'type' => 'string'
                        ],
                        'key_test_1'
                    ],
                    [
                        NumericDataType::class,
                        [
                            'key' => 'key_test_2',
                            'type' => 'numeric'
                        ],
                        'key_test_2'
                    ],
                    [
                        BooleanDataType::class,
                        [
                            'key' => 'key_test_3_upd',
                            'type' => 'boolean'
                        ],
                        'key_test_3_upd'
                    ],
                    [
                        DateDataType::class,
                        [
                            'type' => 'date'
                        ],
                        null
                    ]
                ]
            ]
        );
    }



}