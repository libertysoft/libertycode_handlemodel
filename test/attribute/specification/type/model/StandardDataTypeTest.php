<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\test\attribute\specification\type\model;

use PHPUnit\Framework\TestCase;

use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;



/**
 * @cover DataTypeInterface
 * @cover DefaultDataType
 */
abstract class StandardDataTypeTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use standard data type object.
     *
     * @param callable $callableGetObjDataType
     * @param boolean $boolValueRequired
     * @param array $tabListValue
     * @param mixed $valueFormatGet
     * @param mixed $valueFormatSet
     * @param mixed $valueSaveFormatGet
     * @param mixed $valueSaveFormatSet
     * @param string|array $expectResult
     * @param boolean $boolValueFormatGetSameAssertRequired = true
     * @param boolean $boolValueFormatSetSameAssertRequired = true
     * @param boolean $boolValueSaveFormatGetSameAssertRequired = true
     * @param boolean $boolValueSaveFormatSetSameAssertRequired = true
     */
    protected function testCanUseStandardDataType(
        callable $callableGetObjDataType,
        $boolValueRequired,
        array $tabListValue,
        $valueFormatGet,
        $valueFormatSet,
        $valueSaveFormatGet,
        $valueSaveFormatSet,
        $expectResult,
        $boolValueFormatGetSameAssertRequired = true,
        $boolValueFormatSetSameAssertRequired = true,
        $boolValueSaveFormatGetSameAssertRequired = true,
        $boolValueSaveFormatSetSameAssertRequired = true
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get data type
        /** @var DefaultDataType $objDataType */
        $objDataType = $callableGetObjDataType();

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabExpectRuleConfig = $expectResult[0];
            $expectValueFormatGet = $expectResult[1];
            $expectValueFormatSet = $expectResult[2];
            $expectValueSaveFormatGet = $expectResult[3];
            $expectValueSaveFormatSet = $expectResult[4];

            // Set assertion (check data type rule config)
            $tabDataTypeRuleConfig = $objDataType->getTabRuleConfig(
                $boolValueRequired,
                $tabListValue
            );
            $this->assertEquals($tabExpectRuleConfig, $tabDataTypeRuleConfig);

            // Set assertion (check data type formatted value, when get action required)
            $dataTypeValueFormatGet = $objDataType->getValueFormatGet($valueFormatGet);
            if($boolValueFormatGetSameAssertRequired)
            {
                $this->assertSame($expectValueFormatGet, $dataTypeValueFormatGet);
            }
            else
            {
                $this->assertEquals($expectValueFormatGet, $dataTypeValueFormatGet);
            }


            // Set assertion (check data type formatted value, when set action required)
            $dataTypeValueFormatSet = $objDataType->getValueFormatSet($valueFormatSet);
            if($boolValueFormatSetSameAssertRequired)
            {
                $this->assertSame($expectValueFormatSet, $dataTypeValueFormatSet);
            }
            else
            {
                $this->assertEquals($expectValueFormatSet, $dataTypeValueFormatSet);
            }

            // Set assertion (check data type formatted value, when get action required, to be saved)
            $dataTypeValueSaveFormatGet = $objDataType->getValueSaveFormatGet($valueSaveFormatGet);
            if($boolValueSaveFormatGetSameAssertRequired)
            {
                $this->assertSame($expectValueSaveFormatGet, $dataTypeValueSaveFormatGet);
            }
            else
            {
                $this->assertEquals($expectValueSaveFormatGet, $dataTypeValueSaveFormatGet);
            }

            // Set assertion (check data type formatted value, when set action required, to be loaded)
            $dataTypeValueSaveFormatSet = $objDataType->getValueSaveFormatSet($valueSaveFormatSet);
            if($boolValueSaveFormatSetSameAssertRequired)
            {
                $this->assertSame($expectValueSaveFormatSet, $dataTypeValueSaveFormatSet);
            }
            else
            {
                $this->assertEquals($expectValueSaveFormatSet, $dataTypeValueSaveFormatSet);
            }

            // Print
            /*
            echo('Get config: ' . PHP_EOL);var_dump($objDataType->getTabConfig());echo(PHP_EOL);
            echo('Get rule config: ' . PHP_EOL);var_dump($tabDataTypeRuleConfig);echo(PHP_EOL);

            echo(sprintf('Get formatted value ("%1$s"), when get action required: ' . PHP_EOL, strval($valueFormatGet)));
            var_dump($dataTypeValueFormatGet);
            echo(PHP_EOL);

            echo(sprintf('Get formatted value ("%1$s"), when set action required: ' . PHP_EOL, strval($valueFormatSet)));
            var_dump($dataTypeValueFormatSet);
            echo(PHP_EOL);

            echo(sprintf('Get formatted value ("%1$s"), when get action required, to be saved: ' . PHP_EOL, strval($valueSaveFormatGet)));
            var_dump($dataTypeValueSaveFormatGet);
            echo(PHP_EOL);

            echo(sprintf('Get formatted value ("%1$s"), when set action required, to be loaded: ' . PHP_EOL, strval($valueSaveFormatSet)));
            var_dump($dataTypeValueSaveFormatSet);
            echo(PHP_EOL);
            //*/
        }
    }



}