<?php
/**
 * Description :
 * This class allows to describe behavior of attribute provider class.
 * Attribute provider allows to provide attribute information for entity.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\provider\api;

use liberty_code\model\entity\model\ConfigEntity;
use liberty_code\model\entity\model\ValidatorConfigEntity;
use liberty_code\model\entity\repository\model\SaveConfigEntity;



interface AttrProviderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of entity attribute configuration.
     *
     * Return array format:
     * @see ConfigEntity::getTabConfig() return array format.
     * OR
     * @see ValidatorConfigEntity::getTabConfig() return array format.
     * OR
     * @see SaveConfigEntity::getTabConfig() return array format.
     *
     * @return array
     */
    public function getTabEntityAttrConfig();



    /**
     * Get entity attribute rule configurations array.
     *
     * Return array format:
     * @see ValidatorConfigEntity::getTabRuleConfig() return array format.
     *
     * @return array
     */
    public function getTabEntityAttrRuleConfig();



    /**
     * Get specified entity attribute formatted value,
     * when get action required.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueFormatGet($strKey, $value);



    /**
     * Get specified entity attribute formatted value,
     * when set action required.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueFormatSet($strKey, $value);



    /**
     * Get specified entity attribute formatted value,
     * when get action required, to be saved.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueSaveFormatGet($strKey, $value);



    /**
     * Get specified entity attribute formatted value,
     * when set action required, to be loaded.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueSaveFormatSet($strKey, $value);
}