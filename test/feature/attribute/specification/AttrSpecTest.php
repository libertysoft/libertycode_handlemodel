<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\test\feature\attribute\specification;

use PHPUnit\Framework\TestCase;

use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataTypeCollection;
use liberty_code\handle_model\attribute\specification\type\build\model\DefaultDataTypeBuilder;
use liberty_code\handle_model\attribute\specification\exception\DataTypeInvalidFormatException;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\handle_model\attribute\specification\model\DefaultAttrSpec;
use liberty_code\handle_model\attribute\specification\standard\exception\ConfigInvalidFormatException;
use liberty_code\handle_model\attribute\specification\standard\model\StandardAttrSpec;



/**
 * @cover AttrSpecInterface
 * @cover DefaultAttrSpec
 * @cover StandardAttrSpec
 */
class AttrSpecTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use attribute specification object.
     *
     * @param array $tabDataSrc
     * @param array $tabConfig
     * @param mixed $strDataType
     * @param boolean $boolValueRequired
     * @param array $tabListValue
     * @param mixed $valueFormatGet
     * @param mixed $valueFormatSet
     * @param mixed $valueSaveFormatGet
     * @param mixed $valueSaveFormatSet
     * @param string|array $expectResult
     * @dataProvider providerUseAttrSpec
     */
    public function testCanUseAttrSpec(
        array $tabDataSrc,
        array $tabConfig,
        $strDataType,
        $boolValueRequired,
        array $tabListValue,
        $valueFormatGet,
        $valueFormatSet,
        $valueSaveFormatGet,
        $valueSaveFormatSet,
        $expectResult
    )
    {
        // Load
        $strAttrSpecRootAppPath = dirname(__FILE__) . '/../../..';
        require($strAttrSpecRootAppPath . '/attribute/specification/type/build/boot/DataTypeBuilderBootstrap.php');
        require($strAttrSpecRootAppPath . '/attribute/specification/boot/AttrSpecBootstrap.php');
        /**
         * @var DefaultRepository $objCacheRepo
         * @var array $tabAttrSpecConfig
         */

        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Init data type collection
        /** @var DefaultDataTypeCollection $objDataTypeCollection */
        /** @var DefaultDataTypeBuilder $objDataTypeBuilder */
        $objDataTypeBuilder->setTabDataSrc($tabDataSrc);
        $objDataTypeBuilder->hydrateDataTypeCollection($objDataTypeCollection, true);

        // Init attribute specification
        /** @var StandardAttrSpec $objAttrSpec */
        $objAttrSpec->setTabConfig($tabConfig);
        $objAttrSpec->setTabConfig(array_merge(
            $tabAttrSpecConfig,
            $tabConfig
        ));

        // Get data type info
        $tabDataType = $objAttrSpec->getTabDataType();
        $tabDataTypeRuleConfig = $objAttrSpec->getTabDataTypeRuleConfig(
            $strDataType,
            $boolValueRequired,
            $tabListValue
        );
        $dataTypeValueFormatGet = $objAttrSpec->getDataTypeValueFormatGet($strDataType, $valueFormatGet);
        $dataTypeValueFormatSet = $objAttrSpec->getDataTypeValueFormatSet($strDataType, $valueFormatSet);
        $dataTypeValueSaveFormatGet = $objAttrSpec->getDataTypeValueSaveFormatGet($strDataType, $valueSaveFormatGet);
        $dataTypeValueSaveFormatSet = $objAttrSpec->getDataTypeValueSaveFormatSet($strDataType, $valueSaveFormatSet);

        // Get data type info, from cache, if required
        $tabCacheDataType = array();
        $tabCacheDataTypeRuleConfig = array();
        $cacheDataTypeValueFormatGet = null;
        $cacheDataTypeValueFormatSet = null;
        $cacheDataTypeValueSaveFormatGet = null;
        $cacheDataTypeValueSaveFormatSet = null;
        if($objAttrSpec->checkCacheRequired())
        {
            // Init data type collection
            $objDataTypeCollection->removeDataTypeAll();

            // Get data validation, from cache
            $tabCacheDataType = $objAttrSpec->getTabDataType();
            $tabCacheDataTypeRuleConfig = $objAttrSpec->getTabDataTypeRuleConfig(
                $strDataType,
                $boolValueRequired,
                $tabListValue
            );
            $cacheDataTypeValueFormatGet = $objAttrSpec->getDataTypeValueFormatGet($strDataType, $valueFormatGet);
            $cacheDataTypeValueFormatSet = $objAttrSpec->getDataTypeValueFormatSet($strDataType, $valueFormatSet);
            $cacheDataTypeValueSaveFormatGet = $objAttrSpec->getDataTypeValueSaveFormatGet($strDataType, $valueSaveFormatGet);
            $cacheDataTypeValueSaveFormatSet = $objAttrSpec->getDataTypeValueSaveFormatSet($strDataType, $valueSaveFormatSet);
        }

        // Set assertions
        $tabExpectDataType = (isset($expectResult[0]) ? $expectResult[0] : null);
        $tabExpectDataTypeRuleConfig = (isset($expectResult[1]) ? $expectResult[1] : null);
        $expectDataTypeValueFormatGet = (isset($expectResult[2]) ? $expectResult[2] : null);
        $expectDataTypeValueFormatSet = (isset($expectResult[3]) ? $expectResult[3] : null);
        $expectDataTypeValueSaveFormatGet = (isset($expectResult[4]) ? $expectResult[4] : null);
        $expectDataTypeValueSaveFormatSet = (isset($expectResult[5]) ? $expectResult[5] : null);

        // Set assertions (check attribute specification data type info)
        $this->assertSame($tabExpectDataType, $tabDataType);
        $this->assertEquals($tabExpectDataTypeRuleConfig, $tabDataTypeRuleConfig);
        $this->assertEquals($expectDataTypeValueFormatGet, $dataTypeValueFormatGet);
        $this->assertEquals($expectDataTypeValueFormatSet, $dataTypeValueFormatSet);
        $this->assertEquals($expectDataTypeValueSaveFormatGet, $dataTypeValueSaveFormatGet);
        $this->assertEquals($expectDataTypeValueSaveFormatSet, $dataTypeValueSaveFormatSet);

        // Set assertions (check attribute specification cache)
        if($objAttrSpec->checkCacheRequired())
        {
            $this->assertSame($tabDataType, $tabCacheDataType);
            $this->assertEquals($tabDataTypeRuleConfig, $tabCacheDataTypeRuleConfig);
            $this->assertEquals($dataTypeValueFormatGet, $cacheDataTypeValueFormatGet);
            $this->assertEquals($dataTypeValueFormatSet, $cacheDataTypeValueFormatSet);
            $this->assertEquals($dataTypeValueSaveFormatGet, $cacheDataTypeValueSaveFormatGet);
            $this->assertEquals($dataTypeValueSaveFormatSet, $cacheDataTypeValueSaveFormatSet);
        }
        else
        {
            // Set assertions (check cache not used)
            $this->assertSame(0, count($objCacheRepo->getTabSearchKey()));
        }

        // Print
        /*
        echo('Get data types: ' . PHP_EOL);var_dump($tabDataType);echo(PHP_EOL);
        echo(sprintf('Get data type ("%1$s") rule config: ' . PHP_EOL, strval($strDataType)));var_dump($tabDataTypeRuleConfig);echo(PHP_EOL);

        echo(sprintf('Get data type ("%1$s") formatted value ("%2$s"), when get action required: ' . PHP_EOL, strval($strDataType), strval($valueFormatGet)));
        var_dump($dataTypeValueFormatGet);
        echo(PHP_EOL);

        echo(sprintf('Get data type ("%1$s") formatted value ("%2$s"), when set action required: ' . PHP_EOL, strval($strDataType), strval($valueFormatSet)));
        var_dump($dataTypeValueFormatSet);
        echo(PHP_EOL);

        echo(sprintf('Get data type ("%1$s") formatted value ("%2$s"), when get action required, to be saved: ' . PHP_EOL, strval($strDataType), strval($valueSaveFormatGet)));
        var_dump($dataTypeValueSaveFormatGet);
        echo(PHP_EOL);

        echo(sprintf('Get data type ("%1$s") formatted value ("%2$s"), when set action required, to be loaded: ' . PHP_EOL, strval($strDataType), strval($valueSaveFormatSet)));
        var_dump($dataTypeValueSaveFormatSet);
        echo(PHP_EOL);

        echo(sprintf('Get data type ("%1$s") validation cache key: ', strval($strDataType)) . PHP_EOL);
        var_dump($objCacheRepo->getTabSearchKey());
        echo(PHP_EOL);
        //*/
    }



    /**
     * Data provider,
     * to test can use attribute specification object.
     *
     * @return array
     */
    public function providerUseAttrSpec()
    {
        // Init var
        $tabDataSrc = array(
            [
                'type' => 'string',
                'config' => [
                    'type' => 'string'
                ]
            ],
            [
                'type' => 'numeric',
                'config' => [
                    'type' => 'numeric'
                ]
            ],
            [
                'type' => 'numeric',
                'config' => [
                    'type' => 'numeric',
                    'greater_compare_value' => 0
                ]
            ],
            [
                'type' => 'boolean',
                'config' => [
                    'type' => 'boolean'
                ]
            ],
            [
                'type' => 'date',
                'config' => [
                    'type' => 'date'
                ]
            ]
        );
        $tabAscDataType = array('string', 'numeric', 'boolean', 'date');
        $tabDescDataType = array('date', 'boolean', 'numeric', 'string');

        // Return result
        return array(
            'Use attribute specification: fail to search data type (invalid data type format)' => [
                $tabDataSrc,
                [],
                7,
                true,
                [],
                1,
                '1',
                1,
                '1',
                DataTypeInvalidFormatException::class,
            ],
            'Use attribute specification: fail to search data type (data type not found)' => [
                $tabDataSrc,
                [],
                'num',
                true,
                [],
                1,
                '1',
                1,
                '1',
                DataTypeInvalidFormatException::class,
            ],
            'Use attribute specification: success to use attribute specification (without selection data type last)' => [
                $tabDataSrc,
                [
                    'cache_require' => false
                ],
                'numeric',
                true,
                [],
                1,
                '1',
                1,
                '1',
                [
                    $tabAscDataType,
                    [
                        [
                            'type_numeric',
                            [
                                'string_enable_require' => false,
                                'integer_only_require' => false
                            ]
                        ],
                        [
                            'compare_greater',
                            [
                                'compare_value' => 0,
                                'equal_enable_require' => true
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => null]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ]
                    ],
                    1,
                    1,
                    '1',
                    1,
                ]
            ],
            'Use attribute specification: fail to configure attribute specification (invalid selection data type first option format)' => [
                $tabDataSrc,
                [
                    'select_data_type_first_require' => 'test'
                ],
                'numeric',
                true,
                [],
                1,
                '1',
                1,
                '1',
                ConfigInvalidFormatException::class,
            ],
            'Use attribute specification: success to use attribute specification (with sort data type ASC, selection data type first)' => [
                $tabDataSrc,
                [
                    'select_data_type_first_require' => true
                ],
                'numeric',
                true,
                [],
                1,
                '1',
                1,
                '1',
                [
                    $tabAscDataType,
                    [
                        [
                            'type_numeric',
                            [
                                'string_enable_require' => false,
                                'integer_only_require' => false
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => null]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ]
                    ],
                    1,
                    1,
                    '1',
                    1,
                ]
            ],
            'Use attribute specification: fail to configure attribute specification (invalid sort data type ASC option format)' => [
                $tabDataSrc,
                [
                    'sort_data_type_asc_require' => [],
                    'select_data_type_first_require' => true
                ],
                'numeric',
                true,
                [],
                1,
                '1',
                1,
                '1',
                ConfigInvalidFormatException::class,
            ],
            'Use attribute specification: success to use attribute specification (with selection data type first; without sort data type ASC)' => [
                $tabDataSrc,
                [
                    'sort_data_type_asc_require' => false,
                    'select_data_type_first_require' => true
                ],
                'numeric',
                true,
                [],
                1,
                '1',
                1,
                '1',
                [
                    $tabDescDataType,
                    [
                        [
                            'type_numeric',
                            [
                                'string_enable_require' => false,
                                'integer_only_require' => false
                            ]
                        ],
                        [
                            'compare_greater',
                            [
                                'compare_value' => 0,
                                'equal_enable_require' => true
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => null]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ]
                    ],
                    1,
                    1,
                    '1',
                    1,
                ]
            ],
        );
    }



}