<?php
/**
 * Description :
 * This class allows to define handle save configured entity class.
 * Handle save configured entity is save configured entity, using attribute provider,
 * to build and manage its attributes.
 *
 * Handle save configured entity allows to configure attributes from following configuration (feature 'getTabConfig'):
 * [
 *     Save configured entity configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\entity\repository\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\handle_model\attribute\provider\api\AttrProviderInterface;



class HandleSaveConfigEntity extends SaveConfigEntity
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Attribute provider object
     * @var AttrProviderInterface
     */
    protected $objAttrProvider;



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttrProviderInterface $objAttrProvider
     */
    public function __construct(
        AttrProviderInterface $objAttrProvider,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Set attribute provider
        $this->setAttrProvider($objAttrProvider);

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get attribute provider object.
     *
     * @return AttrProviderInterface
     */
    public function getObjAttrProvider()
    {
        // Return result
        return $this->objAttrProvider;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return $this->getObjAttrProvider()->getTabEntityAttrConfig();
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return $this->getObjAttrProvider()->getTabEntityAttrRuleConfig();
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Return result
        return $this->getObjAttrProvider()->getEntityAttrValueFormatGet($strKey, $value);
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Return result
        return $this->getObjAttrProvider()->getEntityAttrValueFormatSet($strKey, $value);
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Return result
        return $this->getObjAttrProvider()->getEntityAttrValueSaveFormatGet($strKey, $value);
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Return result
        return $this->getObjAttrProvider()->getEntityAttrValueSaveFormatSet($strKey, $value);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set attribute provider object.
     *
     * @param AttrProviderInterface $objAttrProvider
     */
    public function setAttrProvider(AttrProviderInterface $objAttrProvider)
    {
        // Set data
        $this->objAttrProvider = $objAttrProvider;
    }



}