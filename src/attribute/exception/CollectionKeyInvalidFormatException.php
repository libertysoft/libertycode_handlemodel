<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\exception;

use liberty_code\handle_model\attribute\library\ConstAttribute;



class CollectionKeyInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $key
     */
	public function __construct($key) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstAttribute::EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT, strval($key));
	}
	
	
	
}