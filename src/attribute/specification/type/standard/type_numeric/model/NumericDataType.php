<?php
/**
 * This class allows to define numeric data type class.
 * Numeric data type is data type,
 * which allows to manage entity attribute numeric value.
 * Can be consider is base of all numeric data type types.
 *
 * Numeric data type uses the following specified configuration:
 * [
 *     Default data type configuration,
 *
 *     integer_require(optional: got false, if not found): true / false,
 *
 *     greater_compare_value(optional: no comparison done, if not found): numeric greater value to compare,
 *
 *     greater_equal_enable_require(optional: got true, if not found): true / false,
 *
 *     less_compare_value(optional: no comparison done, if not found): numeric less value to compare,
 *
 *     less_equal_enable_require(optional: got true, if not found): true / false,
 *
 *     empty_value(optional: got null, if not found): mixed value,
 *
 *     multiple_require(optional: got false, if not found):
 *         true / false
 *         If true, consider entity attribute value as index array of numeric values,
 *
 *     multiple_unique_require(optional: got false, if not found):
 *         true / false
 *         In case multiple required, if true, each value must be unique,
 *
 *     format_list_value_require(optional: got true, if not found): true / false,
 *
 *     format_get_empty_value(optional: got configuration empty_value, if not found): mixed value,
 *
 *     format_set_numeric_value_require(optional: got true, if not found): true / false,
 *
 *     format_set_empty_value(optional: got "", if not found): mixed value,
 *
 *     save_format_get_numeric_value_require(optional: got true, if not found): true / false,
 *
 *     save_format_get_empty_value(optional: got "", if not found): mixed value,
 *
 *     save_format_get_multiple_value_require(optional: got true, if not found): true / false,
 *
 *     save_format_set_numeric_value_require(optional: got true, if not found): true / false,
 *
 *     save_format_set_empty_value(optional: got "", if not found): mixed value,
 *
 *     save_format_set_multiple_value_require(optional: got true, if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\standard\type_numeric\model;

use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\library\ConstNumericDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\exception\ConfigInvalidFormatException;



class NumericDataType extends DefaultDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        //$result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDataType::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check integer required.
     *
     * @return boolean
     */
    public function checkIntegerRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_INTEGER_REQUIRE]) &&
            (intval($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_INTEGER_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check greater equal enable required.
     *
     * @return boolean
     */
    protected function checkGreaterEqualEnableRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE])) ||
            (intval($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check less equal enable required.
     *
     * @return boolean
     */
    protected function checkLessEqualEnableRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE])) ||
            (intval($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check multiple required.
     * If true, entity attribute value is considered as index array of numeric values.
     *
     * @return boolean
     */
    public function checkMultipleRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) &&
            (intval($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check multiple unique required.
     * In case multiple required, if true, each value must be unique.
     *
     * @return boolean
     */
    public function checkMultipleUniqueRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) &&
            (intval($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting list values required.
     *
     * @return boolean
     */
    public function checkListValueFormatRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute numeric value required,
     * when set action required.
     *
     * @return boolean
     */
    public function checkNumValueFormatSetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_SET_NUMERIC_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_SET_NUMERIC_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute numeric value required,
     * when get action required, to be saved.
     *
     * @return boolean
     */
    public function checkNumValueSaveFormatGetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_NUMERIC_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_NUMERIC_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute multiple value required,
     * when get action required, to be saved.
     *
     * @return boolean
     */
    public function checkMultiValueSaveFormatGetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute numeric value required,
     * when set action required, to be loaded.
     *
     * @return boolean
     */
    public function checkNumValueSaveFormatSetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_NUMERIC_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_NUMERIC_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute multiple value required,
     * when set action required, to be loaded.
     *
     * @return boolean
     */
    public function checkMultiValueSaveFormatSetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get empty value.
     *
     * @return mixed
     */
    protected function getEmptyValue()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstNumericDataType::TAB_CONFIG_KEY_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_EMPTY_VALUE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get entity attribute rule configuration array,
     * for numeric value.
     * Overwrite it to set specific validation.
     *
     * Return format:
     * @see getTabRuleConfig() return array format.
     *
     * @return array
     */
    protected function getTabValidNumericRuleConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $intGreaterCompareValue = (
            isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_GREATER_COMPARE_VALUE]) ?
                $tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_GREATER_COMPARE_VALUE] :
                null
        );
        $intLessCompareValue = (
            isset($tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_LESS_COMPARE_VALUE]) ?
                $tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_LESS_COMPARE_VALUE] :
                null
        );

        // Build rule configuration
        $result = array(
            [
                'type_numeric',
                [
                    'string_enable_require' => false,
                    'integer_only_require' => $this->checkIntegerRequired()
                ]
            ]
        );

        if(!is_null($intGreaterCompareValue))
        {
            $result[] = array(
                'compare_greater',
                [
                    'compare_value' => $intGreaterCompareValue,
                    'equal_enable_require' => $this->checkGreaterEqualEnableRequired()
                ]
            );
        }

        if(!is_null($intLessCompareValue))
        {
            $result[] = array(
                'compare_less',
                [
                    'compare_value' => $intLessCompareValue,
                    'equal_enable_require' => $this->checkLessEqualEnableRequired()
                ]
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get entity attribute rule configuration array,
     * for empty value.
     *
     * Return format:
     * @see getTabRuleConfig() return array format.
     *
     * @return array
     */
    protected function getTabEmptyRuleConfig()
    {
        // Return result
        return array(
            [
                'compare_equal',
                ['compare_value' => $this->getEmptyValue()]
            ]
        );
    }



    /**
     * Get specified index array of formatted list values.
     * Overwrite it to set specific formatting.
     *
     * @param array $tabListValue
     * @return array
     */
    protected function getTabListValueFormat(array $tabListValue)
    {
        // Return result
        return array_map(
            function($value) {
                return $this->getNumValueFormatSet($value);
            },
            array_values($tabListValue)
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabRuleConfig(
        $boolValueRequired = false,
        array $tabListValue = array()
    )
    {
        // Init var
        $boolValueRequired = (is_bool($boolValueRequired) ? $boolValueRequired : false);
        $tabListValue = (
            $this->checkListValueFormatRequired() ?
                $this->getTabListValueFormat($tabListValue) :
                array_values($tabListValue)
        );
        $tabEmptyRuleConfig = $this->getTabEmptyRuleConfig();
        $tabValidNumericRuleConfig = $this->getTabValidNumericRuleConfig();
        $tabValidNumericRuleConfig[] = array(
            'sub_rule_not',
            [
                'rule_config' => $tabEmptyRuleConfig,
                'error_message_pattern' => '%1$s is empty.'
            ]
        );
        if(count($tabListValue) > 0) {
            $tabValidNumericRuleConfig[] = array(
                'compare_in',
                ['compare_value' => $tabListValue]
            );
        }

        // Build rule configuration
        $result = (
            $this->checkMultipleRequired() ?
                // Check multiple value
                array_merge(
                    array(
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => $tabValidNumericRuleConfig,
                                'error_message_pattern' => '%1$s must be an array of valid numerics.'
                            ]
                        ]
                    ),
                    (
                        $boolValueRequired ?
                            array(
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty'],
                                        'error_message_pattern' => '%1$s is empty.'
                                    ]
                                ]
                            ) :
                            array()
                    ),
                    (
                        $this->checkMultipleUniqueRequired() ?
                            array(
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function($strName, $value) {
                                            return (
                                                (!is_array($value)) ||
                                                (count($value) == count(array_unique($value)))
                                            );
                                        },
                                        'error_message_pattern' => '%1$s must contain unique values.'
                                    ]
                                ]
                            ) :
                            array()
                    )
                ) :
                // Check simple value
                (
                    $boolValueRequired ?
                        $tabValidNumericRuleConfig :
                        array(
                            [
                                'group_sub_rule_or',
                                [
                                    'rule_config' => [
                                        'is-empty' => $tabEmptyRuleConfig,
                                        'is-valid-numeric' => $tabValidNumericRuleConfig
                                    ],
                                    'error_message_pattern' => '%1$s must be empty or a valid numeric.'
                                ]
                            ]
                        )
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when get action required.
     *
     * @return mixed
     */
    protected function getEmptyValueFormatGet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_GET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_GET_EMPTY_VALUE] :
                $this->getEmptyValue()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getValueFormatGet($value)
    {
        // Init var
        $emptyValue = $this->getEmptyValue();
        $emptyValueFormatGet = $this->getEmptyValueFormatGet();
        $getValueFormatGet = function($value) use ($emptyValue, $emptyValueFormatGet) {
            return (
                ($value === $emptyValue) ?
                    $emptyValueFormatGet :
                    parent::getValueFormatGet($value)
            );
        };
        $result = (
            $this->checkMultipleRequired() ?
                // Format multiple value
                (
                    is_array($value) ?
                        array_map(
                            function($subValue) use ($getValueFormatGet) {
                                return $getValueFormatGet($subValue);
                            },
                            array_values($value)
                        ) :
                        parent::getValueFormatGet($value)
                ) :
                // Format simple value
                $getValueFormatGet($value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted numeric value,
     * when set action required.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getNumValueFormatSet($value)
    {
        // Return result
        return (
            is_numeric($value) ?
                ($this->checkIntegerRequired() ? intval($value) : floatval($value)) :
                parent::getValueFormatSet($value)
        );
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when set action required.
     *
     * @return mixed
     */
    protected function getEmptyValueFormatSet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_SET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_SET_EMPTY_VALUE] :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getValueFormatSet($value)
    {
        // Init var
        $boolNumValueFormatSetRequired = $this->checkNumValueFormatSetRequired();
        $emptyValue = $this->getEmptyValue();
        $emptyValueFormatSet = $this->getEmptyValueFormatSet();
        $getValueFormatSet = function($value) use ($boolNumValueFormatSetRequired, $emptyValue, $emptyValueFormatSet) {
            return (
                ($value === $emptyValueFormatSet) ?
                    $emptyValue :
                    (
                        $boolNumValueFormatSetRequired ?
                            $this->getNumValueFormatSet($value) :
                            $value
                    )
            );
        };
        $result = (
            $this->checkMultipleRequired() ?
                // Format multiple value
                (
                    is_array($value) ?
                        array_map(
                            function($subValue) use ($getValueFormatSet) {
                                return $getValueFormatSet($subValue);
                            },
                            array_values($value)
                        ) :
                        parent::getValueFormatSet($value)
                ) :
                // Format simple value
                $getValueFormatSet($value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted numeric value,
     * when get action required, to be saved.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getNumValueSaveFormatGet($value)
    {
        // Return result
        return (
            (is_int($value) || is_float($value)) ?
                strval($value) :
                parent::getValueSaveFormatGet($value)
        );
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when get action required, to be saved.
     *
     * @return mixed
     */
    protected function getEmptyValueSaveFormatGet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_EMPTY_VALUE] :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted multiple value,
     * when get action required, to be saved.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getMultiValueSaveFormatGet($value)
    {
        // Return result
        return (
            is_array($value) ?
                json_encode($value) :
                parent::getValueSaveFormatGet($value)
        );
    }



    /**
     * @inheritdoc
     */
    public function getValueSaveFormatGet($value)
    {
        // Init var
        $boolNumValueSaveFormatGetRequired = $this->checkNumValueSaveFormatGetRequired();
        $emptyValue = $this->getEmptyValue();
        $emptyValueSaveFormatGet = $this->getEmptyValueSaveFormatGet();
        $getValueSaveFormatGet = function($value) use ($boolNumValueSaveFormatGetRequired, $emptyValue, $emptyValueSaveFormatGet) {
            return (
                ($value === $emptyValue) ?
                    $emptyValueSaveFormatGet :
                    (
                        $boolNumValueSaveFormatGetRequired ?
                            $this->getNumValueSaveFormatGet($value) :
                            $value
                    )
            );
        };

        // Format multiple value, if required
        if($this->checkMultipleRequired())
        {
            if(is_array($value))
            {
                $result = array_map(
                    function($subValue) use ($getValueSaveFormatGet) {
                        return $getValueSaveFormatGet($subValue);
                    },
                    array_values($value)
                );
                $result = (
                    $this->checkMultiValueSaveFormatGetRequired() ?
                        $this->getMultiValueSaveFormatGet($result) :
                        $result
                );
            }
            else
            {
                $result = parent::getValueSaveFormatGet($value);
            }
        }
        // Else: format simple value
        else
        {
            $result = $getValueSaveFormatGet($value);
        }

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted numeric value,
     * when set action required, to be loaded.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getNumValueSaveFormatSet($value)
    {
        // Return result
        return (
            is_numeric($value) ?
                ($this->checkIntegerRequired() ? intval($value) : floatval($value)) :
                parent::getValueSaveFormatSet($value)
        );
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when set action required, to be loaded.
     *
     * @return mixed
     */
    protected function getEmptyValueSaveFormatSet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_EMPTY_VALUE] :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted multiple value,
     * when set action required, to be loaded.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getMultiValueSaveFormatSet($value)
    {
        // Return result
        return (
            is_array($resultValue = json_decode($value, true)) ?
                $resultValue :
                parent::getValueSaveFormatSet($value)
        );
    }



    /**
     * @inheritdoc
     */
    public function getValueSaveFormatSet($value)
    {
        // Init var
        $boolNumValueSaveFormatSetRequired = $this->checkNumValueSaveFormatSetRequired();
        $emptyValue = $this->getEmptyValue();
        $emptyValueSaveFormatSet = $this->getEmptyValueSaveFormatSet();
        $getValueSaveFormatSet = function($value) use ($boolNumValueSaveFormatSetRequired, $emptyValue, $emptyValueSaveFormatSet) {
            return (
                ($value === $emptyValueSaveFormatSet) ?
                    $emptyValue :
                    (
                        $boolNumValueSaveFormatSetRequired ?
                            $this->getNumValueSaveFormatSet($value) :
                            $value
                    )
            );
        };

        // Format multiple value, if required
        if($this->checkMultipleRequired())
        {
            $result = (
                $this->checkMultiValueSaveFormatSetRequired() ?
                    $this->getMultiValueSaveFormatSet($value) :
                    parent::getValueSaveFormatSet($value)
            );
            if(is_array($result))
            {
                $result = array_map(
                    function($subValue) use ($getValueSaveFormatSet) {
                        return $getValueSaveFormatSet($subValue);
                    },
                    array_values($result)
                );
            }
        }
        // Else: format simple value
        else
        {
            $result = $getValueSaveFormatSet($value);
        }

        // Return result
        return $result;
    }



}