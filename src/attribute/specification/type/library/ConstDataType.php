<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\library;



class ConstDataType
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
	
	

    // Configuration
    const TAB_CONFIG_KEY_KEY = 'key';
    const TAB_CONFIG_KEY_TYPE = 'type';
    const TAB_CONFIG_KEY_ORDER = 'order';

    // Hash configuration
    const HASH_PATTERN = 'data_type_%1$s';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default data type configuration standard.';
	const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Key invalid! The key "%1$s" must be a valid string in collection.';
	const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Value invalid! The value "%1$s" must be a data type object in collection.';



}