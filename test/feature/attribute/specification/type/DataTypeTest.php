<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\test\feature\attribute\specification\type;

use PHPUnit\Framework\TestCase;

use liberty_code\handle_model\attribute\specification\type\library\ToolBoxDataType;
use liberty_code\handle_model\attribute\specification\type\exception\ConfigInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeCollectionInterface;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataTypeCollection;
use liberty_code\handle_model\test\attribute\specification\type\model\Test1DataType;
use liberty_code\handle_model\test\attribute\specification\type\model\Test2DataType;



/**
 * @cover ToolBoxDataType
 * @cover DataTypeInterface
 * @cover DataTypeCollectionInterface
 * @cover DefaultDataType
 * @cover DefaultDataTypeCollection
 */
class DataTypeTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use data type object.
     *
     * @param boolean $boolFixDataTypeRequired
     * @param null|array $tabConfig
     * @param mixed $strType
     * @param string|array $expectResult
     * @dataProvider providerUseDataType
     */
    public function testCanUseDataType(
        $boolFixDataTypeRequired,
        $tabConfig,
        $strType,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get data type
        $objDataType = (
            $boolFixDataTypeRequired ?
                new Test2DataType($tabConfig):
                new Test1DataType($tabConfig)
        );

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabExpectConfig = $expectResult[0];
            $strExpectKey = $expectResult[1];
            $tabExpectType = $expectResult[2];
            $intExpectOrder = $expectResult[3];
            $boolExpectTypeMatches = $expectResult[4];

            // Set assertions (check data type detail)
            $tabDataTypeConfig = $objDataType->getTabConfig();
            $strDataTypeKey = $objDataType->getStrKey();
            $tabDataTypeType = $objDataType->getTabType();
            $intDataTypeOrder = $objDataType->getIntOrder();
            if(!is_null($strExpectKey))
            {
                $this->assertSame($strExpectKey, $strDataTypeKey);
            }
            $this->assertEquals($tabExpectConfig, $tabDataTypeConfig);
            $this->assertSame($tabExpectType, $tabDataTypeType);
            $this->assertSame($intExpectOrder, $intDataTypeOrder);

            // Set assertions (check data type match)
            $boolDataTypeTypeMatches = $objDataType->checkMatches($strType);
            $this->assertSame($boolExpectTypeMatches, $boolDataTypeTypeMatches);

            // Print
            /*
            echo('Get config: ' . PHP_EOL);var_dump($tabDataTypeConfig);echo(PHP_EOL);
            echo('Get key: ' . PHP_EOL);var_dump($strDataTypeKey);echo(PHP_EOL);
            echo('Get types: ' . PHP_EOL);var_dump($tabDataTypeType);echo(PHP_EOL);

            echo(sprintf('Check type "%1$s" matches: ' . PHP_EOL, strval($strType)));
                var_dump($boolDataTypeTypeMatches);
            echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can use data type object.
     *
     * @return array
     */
    public function providerUseDataType()
    {
        // Return result
        return array(
            'Use data type: fail to create data type (invalid key format)' => [
                false,
                [
                    'key' => 7,
                    'type' => [
                        'string',
                        'str'
                    ],
                    'order' => 1
                ],
                'str',
                ConfigInvalidFormatException::class
            ],
            'Use data type: fail to create data type (invalid type format)' => [
                false,
                [
                    'key' => 'key-test',
                    'type' => true,
                    'order' => 1
                ],
                'str',
                ConfigInvalidFormatException::class
            ],
            'Use data type: fail to create data type (type not found)' => [
                false,
                [
                    'key' => 'key-test',
                    'order' => 1
                ],
                'str',
                ConfigInvalidFormatException::class
            ],
            'Use data type: fail to create data type (invalid order format)' => [
                false,
                [
                    'key' => 'key-test',
                    'type' => [
                        'string',
                        'str'
                    ],
                    'order' => 7.7
                ],
                'str',
                ConfigInvalidFormatException::class
            ],
            'Use data type: fail to find type' => [
                false,
                [
                    'key' => 'key-test',
                    'type' => [
                        'string',
                        'str'
                    ],
                    'order' => 1
                ],
                'test',
                [
                    [
                        'key' => 'key-test',
                        'type' => [
                            'string',
                            'str'
                        ],
                        'order' => 1
                    ],
                    'key-test',
                    [
                        'string',
                        'str'
                    ],
                    1,
                    false
                ]
            ],
            'Use data type: success to find type' => [
                false,
                [
                    'key' => 'key-test',
                    'type' => [
                        'string',
                        'str'
                    ],
                    'order' => 1
                ],
                'str',
                [
                    [
                        'key' => 'key-test',
                        'type' => [
                            'string',
                            'str'
                        ],
                        'order' => 1
                    ],
                    'key-test',
                    [
                        'string',
                        'str'
                    ],
                    1,
                    true
                ]
            ],
            'Use data type (with fixed configuration): fail to create data type (invalid type format)' => [
                true,
                [
                    'type' => 'test'
                ],
                'string',
                ConfigInvalidFormatException::class
            ],
            'Use data type (with fixed configuration): fail to find type (invalid type format)' => [
                true,
                null,
                7,
                [
                    [
                        'type' => 'string'
                    ],
                    null,
                    [
                        'string'
                    ],
                    0,
                    false
                ]
            ],
            'Use data type (with fixed configuration): success to find type' => [
                true,
                [
                    'key' => 'key-test'
                ],
                'string',
                [
                    [
                        'key' => 'key-test',
                        'type' => 'string'
                    ],
                    'key-test',
                    [
                        'string'
                    ],
                    0,
                    true
                ]
            ]
        );
    }



    /**
     * Test can use data type collection object.
     *
     * @param mixed|array $tabConfig
     * @param mixed $key
     * @param mixed $strType
     * @param mixed $sortAscRequired
     * @param mixed $firstRequired
     * @param null|array $expectResult
     * @dataProvider providerUseDataTypeCollection
     */
    public function testCanUseDataTypeCollection(
        $tabConfig,
        $key,
        $strType,
        $sortAscRequired,
        $firstRequired,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get data type collection
        $tabDataType = (
            is_array($tabConfig) ?
                array_map(
                    function($tabConfig) {
                        return new Test1DataType($tabConfig);
                    },
                    $tabConfig
                ):
                $tabConfig
        );
        $objDataTypeCollection = new DefaultDataTypeCollection();
        $objDataTypeCollection->setTabDataType($tabDataType);

        // Print
        /*
        foreach($objDataTypeCollection->getTabKey() as $strKey)
        {
            $objDataType = $objDataTypeCollection->getObjDataType($strKey);

            echo('Get key: ' . PHP_EOL);var_dump($objDataType->getStrKey());echo(PHP_EOL);
            echo('Get config: ' . PHP_EOL);var_dump($objDataType->getTabConfig());echo(PHP_EOL);
        }
        //*/

        // Set assertions
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabExpectKey = $expectResult[0];
            $tabExpectType = $expectResult[1];
            $tabExpectConfigFromKey = $expectResult[2];
            $strExpectKeyFromType = $expectResult[3];

            // Set assertions (check data type setting)
            $tabKey = $objDataTypeCollection->getTabKey($sortAscRequired);
            $tabType = ToolBoxDataType::getTabType($objDataTypeCollection, $sortAscRequired);
            $this->assertSame($tabExpectKey, $tabKey);
            $this->assertSame($tabExpectType, $tabType);

            // Set assertions (check exists, data type detail)
            $boolExists = $objDataTypeCollection->checkExists($key);
            /** @var Test1DataType $objDataType */
            $objDataType = $objDataTypeCollection->getObjDataType($key);
            if(is_null($tabExpectConfigFromKey))
            {
                $this->assertSame(false, $boolExists);
                $this->assertEquals(null, $objDataType);
            }
            else
            {
                $this->assertSame(true, $boolExists);
                $this->assertInstanceOf(Test1DataType::class, $objDataType);
                $this->assertEquals($tabExpectConfigFromKey, $objDataType->getTabConfig());
            }

            // Set assertions (check type searching)
            $objDateTypeFromType = ToolBoxDataType::getObjDataTypeFromType(
                $objDataTypeCollection,
                $strType,
                $sortAscRequired,
                $firstRequired
            );
            $this->assertSame(
                $strExpectKeyFromType,
                (
                    is_null($objDateTypeFromType) ?
                        $objDateTypeFromType :
                        $objDateTypeFromType->getStrKey()
                )
            );

            // Set assertion (check data type removing)
            $objDataTypeCollection->removeDataTypeAll();
            $this->assertSame(array(), $objDataTypeCollection->getTabKey());
        }
    }



    /**
     * Data provider,
     * to test can use data type collection object.
     *
     * @return array
     */
    public function providerUseDataTypeCollection()
    {
        // Init var
        $tabConfig = array(
            [
                'key' => 'str_1',
                'type' => [
                    'string',
                    'str'
                ]
            ],
            [
                'key' => 'str_2',
                'type' => [
                    'string[]',
                    'str'
                ],
                'order' => 1
            ],
            [
                'key' => 'str_3',
                'type' => 'str'
            ],
            [
                'key' => 'str_4',
                'type' => 'string*[]'
            ]
        );
        $tabKey = array('str_1', 'str_2', 'str_3', 'str_4');
        $tabAscKey = array('str_1', 'str_3', 'str_4', 'str_2');
        $tabDescKey = array('str_2', 'str_4', 'str_3', 'str_1');
        $tabType = array('string', 'str', 'string[]', 'string*[]');
        $tabAscType = array('string', 'str', 'string*[]', 'string[]');
        $tabDescType = array('string[]', 'str', 'string*[]', 'string');

        // Return result
        return array(
            'Use data type collection: fail to set array of data types' => [
                'test',
                'test',
                'test',
                null,
                false,
                [
                    [],
                    [],
                    null,
                    null
                ]
            ],
            'Use data type collection: fail to search key "test" and search type "test"' => [
                $tabConfig,
                'test',
                'test',
                'test',
                'test',
                [
                    $tabKey,
                    $tabType,
                    null,
                    null
                ]
            ],
            'Use data type collection: fail to search key "test" and search type "test" (invalid type format)' => [
                $tabConfig,
                'test',
                true,
                null,
                false,
                [
                    $tabKey,
                    $tabType,
                    null,
                    null,
                    null
                ]
            ],
            'Use data type collection: success use data type collection (search key "str_3" and search type "string")' => [
                $tabConfig,
                'str_3',
                'string',
                'test',
                'test',
                [
                    $tabKey,
                    $tabType,
                    [
                        'key' => 'str_3',
                        'type' => 'str'
                    ],
                    'str_1'
                ]
            ],
            'Use data type collection: success use data type collection (search key "str_4" and search type "str" (first))' => [
                $tabConfig,
                'str_4',
                'str',
                null,
                true,
                [
                    $tabKey,
                    $tabType,
                    [
                        'key' => 'str_4',
                        'type' => 'string*[]'
                    ],
                    'str_1'
                ]
            ],
            'Use data type collection: success use data type collection (search key "str_4" and search type "str" (last))' => [
                $tabConfig,
                'str_4',
                'str',
                null,
                false,
                [
                    $tabKey,
                    $tabType,
                    [
                        'key' => 'str_4',
                        'type' => 'string*[]'
                    ],
                    'str_3'
                ]
            ],
            'Use data type collection: success use data type collection (search key "str_2" and search type "str" (ASC, first))' => [
                $tabConfig,
                'str_2',
                'str',
                true,
                true,
                [
                    $tabAscKey,
                    $tabAscType,
                    [
                        'key' => 'str_2',
                        'type' => [
                            'string[]',
                            'str'
                        ],
                        'order' => 1
                    ],
                    'str_1'
                ]
            ],
            'Use data type collection: success use data type collection (search key "str_2" and search type "str" (ASC, last))' => [
                $tabConfig,
                'str_2',
                'str',
                true,
                false,
                [
                    $tabAscKey,
                    $tabAscType,
                    [
                        'key' => 'str_2',
                        'type' => [
                            'string[]',
                            'str'
                        ],
                        'order' => 1
                    ],
                    'str_2'
                ]
            ],
            'Use data type collection: success use data type collection (search key 2 and search type "str" (DESC, first))' => [
                $tabConfig,
                'str_3',
                'str',
                false,
                true,
                [
                    $tabDescKey,
                    $tabDescType,
                    [
                        'key' => 'str_3',
                        'type' => 'str'
                    ],
                    'str_2'
                ]
            ],
            'Use data type collection: success use data type collection (search key 2 and search type "str" (DESC, last))' => [
                $tabConfig,
                'str_3',
                'str',
                false,
                false,
                [
                    $tabDescKey,
                    $tabDescType,
                    [
                        'key' => 'str_3',
                        'type' => 'str'
                    ],
                    'str_1'
                ]
            ]
        );
    }



}