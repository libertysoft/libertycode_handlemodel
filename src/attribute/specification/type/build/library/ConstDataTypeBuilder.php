<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\build\library;



class ConstDataTypeBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Properties
    const DATA_KEY_DEFAULT_FACTORY = 'objFactory';
    const DATA_KEY_DEFAULT_DATA_SRC = 'tabDataSrc';
	
	
	
	// Exception message
    const EXCEPT_MSG_FACTORY_INVALID_FORMAT = 'Following data type factory "%1$s" invalid! It must be null or a factory object.';
    const EXCEPT_MSG_DATA_SRC_INVALID_FORMAT =
        'Following data source "%1$s" invalid! 
        The data source must be an array and following the default data type builder data source standard.';



}