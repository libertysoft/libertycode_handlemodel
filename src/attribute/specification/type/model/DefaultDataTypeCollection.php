<?php
/**
 * Description :
 * This class allows to define default data type collection class.
 * key: data type key => data type.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;
use liberty_code\handle_model\attribute\specification\type\exception\CollectionKeyInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\exception\CollectionValueInvalidFormatException;



class DefaultDataTypeCollection extends DefaultBean implements DataTypeCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var DataTypeInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods check
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjDataType($strKey)));
    }
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabKey($sortAscRequired = null)
    {
        // Init var
        $boolSortAscRequired = (is_bool($sortAscRequired) ? $sortAscRequired : null);
        $result = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);

        // Sort data types, if required
        if(is_bool($boolSortAscRequired))
        {
            usort(
                $result,
                function($strKey1, $strKey2)
                {
                    $intOrder1 = $this->getObjDataType($strKey1)->getIntOrder();
                    $intOrder2 = $this->getObjDataType($strKey2)->getIntOrder();
                    $result = (($intOrder1 < $intOrder2) ? -1 : (($intOrder1 > $intOrder2) ? 1 : 0));

                    return $result;
                }
            );

            // Get descending sort, if required
            if(!$boolSortAscRequired)
            {
                krsort($result);
                $result = array_values($result);
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjDataType($strKey)
    {
        // Init var
        $result = null;

        // Try to get data type object, if found
        try
        {
            if($this->beanDataExists($strKey))
            {
                $result = $this->beanGetData($strKey);
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
    }



	
	
	// Methods setters
	// ******************************************************************************

	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setDataType(DataTypeInterface $objDataType)
	{
		// Init var
		$strKey = $objDataType->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objDataType);
		
		// return result
		return $strKey;
	}



    /**
     * @inheritdoc
     */
    public function setTabDataType($tabDataType)
    {
        // Init var
        $result = array();

        // Case index array of data types
        if(is_array($tabDataType))
        {
            // Run all data types and for each, try to set
            foreach($tabDataType as $dataType)
            {
                $strKey = $this->setDataType($dataType);
                $result[] = $strKey;
            }
        }
        // Case collection of data types
        else if($tabDataType instanceof DataTypeCollectionInterface)
        {
            // Run all data types and for each, try to set
            foreach($tabDataType->getTabKey() as $strKey)
            {
                $objDataType = $tabDataType->getObjDataType($strKey);
                $strKey = $this->setDataType($objDataType);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeDataType($strKey)
    {
        // Init var
        $result = $this->getObjDataType($strKey);

        // Remove data type
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeDataTypeAll()
    {
        // Ini var
        $tabKey = $this->getTabKey();

        foreach($tabKey as $strKey)
        {
            $this->removeDataType($strKey);
        }
    }



}