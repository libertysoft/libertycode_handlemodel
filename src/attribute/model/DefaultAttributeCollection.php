<?php
/**
 * This class allows to define default attribute collection class.
 * key: attribute key => attribute.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\handle_model\attribute\api\AttributeCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\handle_model\attribute\library\ToolBoxEntity;
use liberty_code\handle_model\attribute\api\AttributeInterface;
use liberty_code\handle_model\attribute\exception\CollectionKeyInvalidFormatException;
use liberty_code\handle_model\attribute\exception\CollectionValueInvalidFormatException;



class DefaultAttributeCollection extends DefaultBean implements AttributeCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var AttributeInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrAttributeKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkAttributeExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjAttribute($strKey)));
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabAttributeKey()
    {
        // Return result
        return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
    }



    /**
     * @inheritdoc
     */
    public function getObjAttribute($strKey)
    {
        // Init var
        $result = null;

        // Try to get attribute object, if found
        try
        {
            if($this->beanDataExists($strKey))
            {
                $result = $this->beanGetData($strKey);
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws CollectionValueInvalidFormatException
     */
    public function setAttribute(AttributeInterface $objAttribute)
    {
        // Init var
        $strKey = $objAttribute->getStrAttributeKey();

        // Register instance
        $this->beanPutData($strKey, $objAttribute);

        // return result
        return $strKey;
    }



    /**
     * @inheritdoc
     */
    public function setTabAttribute($tabAttribute)
    {
        // Init var
        $result = array();

        // Case index array of attributes
        if(is_array($tabAttribute))
        {
            // Run all attributes and for each, try to set
            foreach($tabAttribute as $attribute)
            {
                $strKey = $this->setAttribute($attribute);
                $result[] = $strKey;
            }
        }
        // Case collection of attributes
        else if($tabAttribute instanceof AttributeCollectionInterface)
        {
            // Run all attributes and for each, try to set
            foreach($tabAttribute->getTabAttributeKey() as $strKey)
            {
                $objAttribute = $tabAttribute->getObjAttribute($strKey);
                $strKey = $this->setAttribute($objAttribute);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeAttribute($strKey)
    {
        // Init var
        $result = $this->getObjAttribute($strKey);

        // Remove attribute
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeAttributeAll()
    {
        // Ini var
        $tabKey = $this->getTabAttributeKey();

        foreach($tabKey as $strKey)
        {
            $this->removeAttribute($strKey);
        }
    }





    // Methods entity attribute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabEntityAttrConfig()
    {
        // Ini var
        $tabAttribute = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_VALUE);
        $result = ToolBoxEntity::getTabAttributeConfig($tabAttribute);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrRuleConfig()
    {
        // Ini var
        $tabAttribute = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_VALUE);
        $result = ToolBoxEntity::getTabAttributeRuleConfig($tabAttribute);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueFormatGet($strKey, $value)
    {
        // Return result
        return ToolBoxEntity::getAttributeValueFormatGet(
            $this,
            $strKey,
            $value
        );
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueFormatSet($strKey, $value)
    {
        // Return result
        return ToolBoxEntity::getAttributeValueFormatSet(
            $this,
            $strKey,
            $value
        );
    }



}