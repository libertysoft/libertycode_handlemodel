<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\standard\exception;

use Exception;

use liberty_code\handle_model\attribute\specification\standard\library\ConstStandardAttrSpec;



class ConfigInvalidFormatException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstStandardAttrSpec::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param array $config
     * @return boolean
     */
    protected static function checkConfigIsValid(array $config)
    {
        // Init var
        $result =
            // Check valid sort data type ASC required option
            (
                (!isset($config[ConstStandardAttrSpec::TAB_CONFIG_KEY_SORT_DATA_TYPE_ASC_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstStandardAttrSpec::TAB_CONFIG_KEY_SORT_DATA_TYPE_ASC_REQUIRE]) ||
                    is_int($config[ConstStandardAttrSpec::TAB_CONFIG_KEY_SORT_DATA_TYPE_ASC_REQUIRE]) ||
                    (
                        is_string($config[ConstStandardAttrSpec::TAB_CONFIG_KEY_SORT_DATA_TYPE_ASC_REQUIRE]) &&
                        ctype_digit($config[ConstStandardAttrSpec::TAB_CONFIG_KEY_SORT_DATA_TYPE_ASC_REQUIRE])
                    )
                )
            ) &&

            // Check valid selection data type first required option
            (
                (!isset($config[ConstStandardAttrSpec::TAB_CONFIG_KEY_SELECT_DATA_TYPE_FIRST_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstStandardAttrSpec::TAB_CONFIG_KEY_SELECT_DATA_TYPE_FIRST_REQUIRE]) ||
                    is_int($config[ConstStandardAttrSpec::TAB_CONFIG_KEY_SELECT_DATA_TYPE_FIRST_REQUIRE]) ||
                    (
                        is_string($config[ConstStandardAttrSpec::TAB_CONFIG_KEY_SELECT_DATA_TYPE_FIRST_REQUIRE]) &&
                        ctype_digit($config[ConstStandardAttrSpec::TAB_CONFIG_KEY_SELECT_DATA_TYPE_FIRST_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }



}