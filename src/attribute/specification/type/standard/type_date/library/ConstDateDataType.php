<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\standard\type_date\library;



class ConstDateDataType
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_DATETIME_FACTORY = 'objDateTimeFactory';



    // Configuration
    const TAB_CONFIG_KEY_GREATER_COMPARE_VALUE = 'greater_compare_value';
    const TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE = 'greater_equal_enable_require';
    const TAB_CONFIG_KEY_LESS_COMPARE_VALUE = 'less_compare_value';
    const TAB_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE = 'less_equal_enable_require';
    const TAB_CONFIG_KEY_EMPTY_VALUE = 'empty_value';
    const TAB_CONFIG_KEY_MULTIPLE_REQUIRE = 'multiple_require';
    const TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE = 'multiple_unique_require';
    const TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE = 'format_list_value_require';
    const TAB_CONFIG_KEY_FORMAT_GET_DATETIME_VALUE_REQUIRE = 'format_get_datetime_value_require';
    const TAB_CONFIG_KEY_FORMAT_GET_EMPTY_VALUE = 'format_get_empty_value';
    const TAB_CONFIG_KEY_FORMAT_SET_DATETIME_VALUE_REQUIRE = 'format_set_datetime_value_require';
    const TAB_CONFIG_KEY_FORMAT_SET_EMPTY_VALUE = 'format_set_empty_value';
    const TAB_CONFIG_KEY_SAVE_FORMAT_GET_DATETIME_VALUE_REQUIRE = 'save_format_get_datetime_value_require';
    const TAB_CONFIG_KEY_SAVE_FORMAT_GET_EMPTY_VALUE = 'save_format_get_empty_value';
    const TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE = 'save_format_get_multiple_value_require';
    const TAB_CONFIG_KEY_SAVE_FORMAT_SET_DATETIME_VALUE_REQUIRE = 'save_format_set_datetime_value_require';
    const TAB_CONFIG_KEY_SAVE_FORMAT_SET_EMPTY_VALUE = 'save_format_set_empty_value';
    const TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE = 'save_format_set_multiple_value_require';



    // Exception message constants
    const EXCEPT_MSG_DATETIME_FACTORY_INVALID_FORMAT =
        'Following datetime factory "%1$s" invalid! It must be a datetime factory object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the date data type configuration standard.';



}