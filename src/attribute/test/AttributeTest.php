<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/attribute/test/AttrSpecTest.php');

// Use
use liberty_code\handle_model\attribute\factory\standard\model\StandardAttributeFactory;
use liberty_code\handle_model\attribute\build\model\DefaultBuilder;



// Init factory
$objAttributeFactory = new StandardAttributeFactory(
    null,
    null,
    $objAttrSpec
);

// Init builder
$objAttributeBuilder = new DefaultBuilder($objAttributeFactory);


