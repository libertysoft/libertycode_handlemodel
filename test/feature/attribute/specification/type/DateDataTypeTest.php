<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\test\feature\attribute\specification\type;

use liberty_code\handle_model\test\attribute\specification\type\model\StandardDataTypeTest;

use DateTime;
use DateTimeZone;
use liberty_code\model\datetime\factory\model\DefaultDateTimeFactory;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\exception\ConfigInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\model\DateDataType;



/**
 * @cover DataTypeInterface
 * @cover DefaultDataType
 * @cover DateDataType
 */
class DateDataTypeTest extends StandardDataTypeTest
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use date data type object.
     *
     * @param null|array $tabConfig
     * @param boolean $boolValueRequired
     * @param array $tabListValue
     * @param mixed $valueFormatGet
     * @param mixed $valueFormatSet
     * @param mixed $valueSaveFormatGet
     * @param mixed $valueSaveFormatSet
     * @param string|array $expectResult
     * @dataProvider providerUseDateDataType
     */
    public function testCanUseDateDataType(
        $tabConfig,
        $boolValueRequired,
        array $tabListValue,
        $valueFormatGet,
        $valueFormatSet,
        $valueSaveFormatGet,
        $valueSaveFormatSet,
        $expectResult
    )
    {
        // Init var
        $callableGetObjDataType = function() use ($tabConfig) {
            // Load
            $strRootAppPath = dirname(__FILE__) . '/../../../..';
            require($strRootAppPath . '/attribute/specification/type/boot/DataTypeBootstrap.php');

            /** @var DefaultDateTimeFactory $objDateTimeFactory */
            return new DateDataType($objDateTimeFactory, $tabConfig);
        };

        // Test
        $this->testCanUseStandardDataType(
            $callableGetObjDataType,
            $boolValueRequired,
            $tabListValue,
            $valueFormatGet,
            $valueFormatSet,
            $valueSaveFormatGet,
            $valueSaveFormatSet,
            $expectResult,
            false,
            false,
            false,
            false
        );
    }



    /**
     * Data provider,
     * to test can use date data type object.
     *
     * @return array
     */
    public function providerUseDateDataType()
    {
        // Init var
        $strTz = 'UTC';
        $objTz = new DateTimeZone($strTz);

        $strFormat = 'Y-m-d H:i:s';
        $strDt = '2022-10-23 17:00:00';
        $objDt = DateTime::createFromFormat($strFormat, $strDt, $objTz);

        $strGreaterDt = '2022-10-20 17:00:00';
        $objGreaterDt = DateTime::createFromFormat($strFormat, $strGreaterDt, $objTz);

        $strLessDt = '2022-10-25 17:00:00';
        $objLessDt = DateTime::createFromFormat($strFormat, $strLessDt, $objTz);

        $strListDt1 = '2022-10-21 17:00:00';
        $objListDt1 = DateTime::createFromFormat($strFormat, $strListDt1, $objTz);

        $strListDt2 = '2022-10-21 17:00:00';
        $objListDt2 = DateTime::createFromFormat($strFormat, $strListDt2, $objTz);

        $strListDt3 = '2022-10-21 17:00:00';
        $objListDt3 = DateTime::createFromFormat($strFormat, $strListDt3, $objTz);

        // Return result
        return array(
            'Use date data type: fail to create date data type (invalid greater value format)' => [
                [
                    'type' => 'date',
                    'greater_compare_value' => 7
                ],
                false,
                [],
                $objDt,
                $strDt,
                $objDt,
                sprintf('[%1$s]', $strDt),
                ConfigInvalidFormatException::class
            ],
            'Use date data type: fail to create date data type (invalid greater equal option format)' => [
                [
                    'type' => 'date',
                    'greater_equal_enable_require' => []
                ],
                false,
                [],
                $objDt,
                $strDt,
                $objDt,
                sprintf('[%1$s]', $strDt),
                ConfigInvalidFormatException::class
            ],
            'Use date data type: fail to create date data type (invalid format list value option format)' => [
                [
                    'type' => 'date',
                    'format_list_value_require' => []
                ],
                false,
                [],
                $objDt,
                $strDt,
                $objDt,
                sprintf('[%1$s]', $strDt),
                ConfigInvalidFormatException::class
            ],
            'Use date data type: fail to create date data type (invalid format value option (for setting) format)' => [
                [
                    'type' => 'date',
                    'format_set_datetime_value_require' => 'test'
                ],
                false,
                [],
                $objDt,
                $strDt,
                $objDt,
                sprintf('[%1$s]', $strDt),
                ConfigInvalidFormatException::class
            ],
            'Use date data type: fail to create date data type (invalid format value option (for loading) format)' => [
                [
                    'type' => 'date',
                    'save_format_set_datetime_value_require' => 'test'
                ],
                false,
                [],
                $objDt,
                $strDt,
                $objDt,
                sprintf('[%1$s]', $strDt),
                ConfigInvalidFormatException::class
            ],
            'Use date data type: success to use date data type (simple; with rule options, required, list, formatting)' => [
                [
                    'type' => 'date',
                    'greater_compare_value' => $strGreaterDt,
                    'less_compare_value' => $strLessDt
                ],
                true,
                [$strListDt1, $strListDt2, $strListDt3],
                $objDt,
                $strDt,
                $objDt,
                sprintf('[%1$s]', $strDt),
                [
                    [
                        'type_date',
                        [
                            'compare_greater',
                            [
                                'compare_value' => $objGreaterDt,
                                'equal_enable_require' => true
                            ]
                        ],
                        [
                            'compare_less',
                            [
                                'compare_value' => $objLessDt,
                                'equal_enable_require' => true
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => null]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ],
                        [
                            'compare_in',
                            [
                                'compare_value' => [$objListDt1, $objListDt2, $objListDt3]
                            ]
                        ]
                    ],
                    $strDt,
                    $objDt,
                    sprintf('[%1$s]', $strDt),
                    $objDt,
                ]
            ],
            'Use date data type: success to use date data type (simple; with rule options, custom empty value, formatting; without required, list)' => [
                [
                    'type' => 'date',
                    'greater_compare_value' => $strGreaterDt,
                    'greater_equal_enable_require' => false,
                    'less_compare_value' => $strLessDt,
                    'less_equal_enable_require' => false,
                    'empty_value' => 'null',
                    'format_get_empty_value' => null,
                    'format_set_empty_value' => null,
                    'save_format_get_empty_value' => '[null]',
                    'save_format_set_empty_value' => '[null]',
                ],
                false,
                [],
                'null',
                null,
                'null',
                '[null]',
                [
                    [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-empty' => [
                                        [
                                            'compare_equal',
                                            ['compare_value' => 'null']
                                        ]
                                    ],
                                    'is-valid-date' => [
                                        'type_date',
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => $objGreaterDt,
                                                'equal_enable_require' => false
                                            ]
                                        ],
                                        [
                                            'compare_less',
                                            [
                                                'compare_value' => $objLessDt,
                                                'equal_enable_require' => false
                                            ]
                                        ],
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => [
                                                    [
                                                        'compare_equal',
                                                        ['compare_value' => 'null']
                                                    ]
                                                ],
                                                'error_message_pattern' => '%1$s is empty.'
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be empty or a valid date.'
                            ]
                        ]
                    ],
                    null,
                    'null',
                    '[null]',
                    'null',
                ]
            ],
            'Use date data type: success to use date data type (simple; with required, empty value formatting only; without list)' => [
                [
                    'type' => 'date',
                    'format_list_value_require' => false,
                    'format_get_datetime_value_require' => false,
                    'format_set_datetime_value_require' => false,
                    'save_format_get_datetime_value_require' => false,
                    'save_format_set_datetime_value_require' => false
                ],
                true,
                [$strListDt1, $strListDt2, $strListDt3],
                $objDt,
                $strDt,
                $objDt,
                sprintf('[%1$s]', $strDt),
                [
                    [
                        'type_date',
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => null]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ],
                        [
                            'compare_in',
                            [
                                'compare_value' => [$strListDt1, $strListDt2, $strListDt3]
                            ]
                        ]
                    ],
                    $objDt,
                    $strDt,
                    $objDt,
                    sprintf('[%1$s]', $strDt),
                ]
            ],
            'Use date data type: fail to get formatted values, from date data type (simple; with required, formatting; without list)' => [
                [
                    'type' => 'date'
                ],
                true,
                [],
                'test',
                'test',
                'test',
                'test',
                [
                    [
                        'type_date',
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => null]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ]
                    ],
                    'test',
                    'test',
                    'test',
                    'test'
                ]
            ],
            'Use date data type: fail to create date data type (invalid multiple option format)' => [
                [
                    'type' => 'date',
                    'multiple_require' => 'test'
                ],
                false,
                [],
                [$objListDt1, $objListDt2, $objListDt3],
                [$strListDt1, $objListDt2, $strListDt3],
                [$objListDt1, $objListDt2, $objListDt3],
                json_encode([
                    sprintf('[%1$s]', $strListDt1),
                    sprintf('[%1$s]', $strListDt2),
                    sprintf('[%1$s]', $strListDt3)
                ]),
                ConfigInvalidFormatException::class
            ],
            'Use date data type: fail to create date data type (invalid multiple unique option format)' => [
                [
                    'type' => 'date',
                    'multiple_unique_require' => []
                ],
                false,
                [],
                [$objListDt1, $objListDt2, $objListDt3],
                [$strListDt1, $objListDt2, $strListDt3],
                [$objListDt1, $objListDt2, $objListDt3],
                json_encode([
                    sprintf('[%1$s]', $strListDt1),
                    sprintf('[%1$s]', $strListDt2),
                    sprintf('[%1$s]', $strListDt3)
                ]),
                ConfigInvalidFormatException::class
            ],
            'Use date data type: fail to create date data type (invalid format multiple value option (for saving) format)' => [
                [
                    'type' => 'date',
                    'save_format_get_multiple_value_require' => 'test'
                ],
                false,
                [],
                [$objListDt1, $objListDt2, $objListDt3],
                [$strListDt1, $objListDt2, $strListDt3],
                [$objListDt1, $objListDt2, $objListDt3],
                json_encode([
                    sprintf('[%1$s]', $strListDt1),
                    sprintf('[%1$s]', $strListDt2),
                    sprintf('[%1$s]', $strListDt3)
                ]),
                ConfigInvalidFormatException::class
            ],


            'Use date data type: success to use date data type (multiple; with required, formatting; without unique)' => [
                [
                    'type' => 'date',
                    'multiple_require' => true
                ],
                true,
                [],
                [$objListDt1, $objListDt2, $objListDt3],
                [$strListDt1, $objListDt2, $strListDt3],
                [$objListDt1, $objListDt2, $objListDt3],
                json_encode([
                    sprintf('[%1$s]', $strListDt1),
                    sprintf('[%1$s]', $strListDt2),
                    sprintf('[%1$s]', $strListDt3)
                ]),
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    'type_date',
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'compare_equal',
                                                    ['compare_value' => null]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid dates.'
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => ['is_empty'],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ]
                    ],
                    [$strListDt1, $strListDt2, $strListDt3],
                    [$objListDt1, $objListDt2, $objListDt3],
                    json_encode([
                        sprintf('[%1$s]', $strListDt1),
                        sprintf('[%1$s]', $strListDt2),
                        sprintf('[%1$s]', $strListDt3)
                    ]),
                    [$objListDt1, $objListDt2, $objListDt3]
                ]
            ],
            'Use date data type: success to use date data type (multiple; with unique, formatting; without required, multiple formatting)' => [
                [
                    'type' => 'date',
                    'multiple_require' => true,
                    'multiple_unique_require' => true,
                    'save_format_get_multiple_value_require' => false,
                    'save_format_set_multiple_value_require' => false
                ],
                false,
                [],
                [$objListDt1, $objListDt2, $objListDt3],
                [$strListDt1, $objListDt2, $strListDt3],
                [$objListDt1, $objListDt2, $objListDt3],
                json_encode([
                    sprintf('[%1$s]', $strListDt1),
                    sprintf('[%1$s]', $strListDt2),
                    sprintf('[%1$s]', $strListDt3)
                ]),
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    'type_date',
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'compare_equal',
                                                    ['compare_value' => null]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid dates.'
                            ]
                        ],
                        [
                            'callable',
                            [
                                'valid_callable' => function($strName, $value) {
                                    return (
                                        (!is_array($value)) ||
                                        (count($value) == count(array_unique($value)))
                                    );
                                },
                                'error_message_pattern' => '%1$s must contain unique values.'
                            ]
                        ]
                    ],
                    [$strListDt1, $strListDt2, $strListDt3],
                    [$objListDt1, $objListDt2, $objListDt3],
                    [
                        sprintf('[%1$s]', $strListDt1),
                        sprintf('[%1$s]', $strListDt2),
                        sprintf('[%1$s]', $strListDt3)
                    ],
                    json_encode([
                        sprintf('[%1$s]', $strListDt1),
                        sprintf('[%1$s]', $strListDt2),
                        sprintf('[%1$s]', $strListDt3)
                    ])
                ]
            ],
            'Use date data type: fail to get formatted values, from date data type (multiple; with multiple formatting; without required, unique, formatting)' => [
                [
                    'type' => 'date',
                    'multiple_require' => true,
                    'save_format_get_datetime_value_require' => false,
                    'save_format_set_datetime_value_require' => false
                ],
                false,
                [],
                'test',
                'test',
                [$objListDt1, $objListDt2, $objListDt3],
                json_encode([
                    sprintf('[%1$s]', $strListDt1),
                    sprintf('[%1$s]', $strListDt2),
                    sprintf('[%1$s]', $strListDt3)
                ]),
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    'type_date',
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'compare_equal',
                                                    ['compare_value' => null]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid dates.'
                            ]
                        ]
                    ],
                    'test',
                    'test',
                    json_encode([$objListDt1, $objListDt2, $objListDt3]),
                    [
                        sprintf('[%1$s]', $strListDt1),
                        sprintf('[%1$s]', $strListDt2),
                        sprintf('[%1$s]', $strListDt3)
                    ]
                ]
            ]
        );
    }



}