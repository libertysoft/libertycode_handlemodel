<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\exception;

use Exception;

use liberty_code\handle_model\attribute\specification\library\ConstAttrSpec;



class DataTypeInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $type
     */
	public function __construct($type)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstAttrSpec::EXCEPT_MSG_DATA_TYPE_INVALID_FORMAT,
            mb_strimwidth(strval($type), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified type has valid format.
	 * 
     * @param mixed $type
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($type)
    {
		// Init var
		$result = (is_string($type) && (trim($type) != ''));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($type);
		}
		
		// Return result
		return $result;
    }
	
	
	
}