<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\exception;

use Exception;

use liberty_code\handle_model\attribute\specification\library\ConstAttrSpec;



class ConfigInvalidFormatException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstAttrSpec::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param array $config
     * @return boolean
     */
    protected static function checkConfigIsValid(array $config)
    {
        // Init var
        $result =
            // Check valid cache required option
            (
                (!isset($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_REQUIRE]) ||
                    is_int($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_REQUIRE]) ||
                    (
                        is_string($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_REQUIRE]) &&
                        ctype_digit($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_REQUIRE])
                    )
                )
            ) &&

            // Check valid cache data type key
            (
                (!isset($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_DATA_TYPE_KEY])) ||
                (
                    is_string($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_DATA_TYPE_KEY]) &&
                    (trim($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_DATA_TYPE_KEY]) != '')
                )
            ) &&

            // Check valid cache rule configuration key pattern
            (
                (!isset($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_RULE_CONFIG_KEY_PATTERN])) ||
                (
                    is_string($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_RULE_CONFIG_KEY_PATTERN]) &&
                    (trim($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_RULE_CONFIG_KEY_PATTERN]) != '')
                )
            ) &&

            // Check valid cache value format get key pattern
            (
                (!isset($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_GET_KEY_PATTERN])) ||
                (
                    is_string($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_GET_KEY_PATTERN]) &&
                    (trim($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_GET_KEY_PATTERN]) != '')
                )
            ) &&

            // Check valid cache value format set key pattern
            (
                (!isset($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_SET_KEY_PATTERN])) ||
                (
                    is_string($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_SET_KEY_PATTERN]) &&
                    (trim($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_SET_KEY_PATTERN]) != '')
                )
            ) &&

            // Check valid cache value save format get key pattern
            (
                (!isset($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_GET_KEY_PATTERN])) ||
                (
                    is_string($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_GET_KEY_PATTERN]) &&
                    (trim($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_GET_KEY_PATTERN]) != '')
                )
            ) &&

            // Check valid cache value save format set key pattern
            (
                (!isset($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_SET_KEY_PATTERN])) ||
                (
                    is_string($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_SET_KEY_PATTERN]) &&
                    (trim($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_SET_KEY_PATTERN]) != '')
                )
            ) &&

            // Check valid cache set configuration
            (
                (!isset($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG])) ||
                is_array($config[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG])
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }



}