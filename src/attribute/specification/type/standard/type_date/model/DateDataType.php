<?php
/**
 * This class allows to define date data type class.
 * Date data type is data type,
 * which allows to manage entity attribute date value.
 * Can be consider is base of all date data type types.
 *
 * Date data type uses the following specified configuration:
 * [
 *     Default data type configuration,
 *
 *     greater_compare_value(optional: no comparison done, if not found): date greater value (string|DateTime) to compare,
 *
 *     greater_equal_enable_require(optional: got true, if not found): true / false,
 *
 *     less_compare_value(optional: no comparison done, if not found): date less value (string|DateTime) to compare,
 *
 *     less_equal_enable_require(optional: got true, if not found): true / false,
 *
 *     empty_value(optional: got null, if not found): mixed value,
 *
 *     multiple_require(optional: got false, if not found):
 *         true / false
 *         If true, consider entity attribute value as index array of date values,
 *
 *     multiple_unique_require(optional: got false, if not found):
 *         true / false
 *         In case multiple required, if true, each value must be unique,
 *
 *     format_list_value_require(optional: got true, if not found): true / false,
 *
 *     format_get_datetime_value_require(optional: got true, if not found): true / false,
 *
 *     format_get_empty_value(optional: got configuration empty_value, if not found): mixed value,
 *
 *     format_set_datetime_value_require(optional: got true, if not found): true / false,
 *
 *     format_set_empty_value(optional: got "", if not found): mixed value,
 *
 *     save_format_get_datetime_value_require(optional: got true, if not found): true / false,
 *
 *     save_format_get_empty_value(optional: got "", if not found): mixed value,
 *
 *     save_format_get_multiple_value_require(optional: got true, if not found): true / false,
 *
 *     save_format_set_datetime_value_require(optional: got true, if not found): true / false,
 *
 *     save_format_set_empty_value(optional: got "", if not found): mixed value,
 *
 *     save_format_set_multiple_value_require(optional: got true, if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\standard\type_date\model;

use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;

use DateTime;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\library\ConstDateDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\exception\DateTimeFactoryInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\exception\ConfigInvalidFormatException;



/**
 * @method DateTimeFactoryInterface getObjDateTimeFactory() Get datetime factory object.
 * @method void setObjDateTimeFactory(DateTimeFactoryInterface $objDateTimeFactory) Set datetime factory object.
 */
class DateDataType extends DefaultDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct($tabConfig);

        // Init datetime factory
        $this->setObjDateTimeFactory($objDateTimeFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstDateDataType::DATA_KEY_DEFAULT_DATETIME_FACTORY))
        {
            $this->__beanTabData[ConstDateDataType::DATA_KEY_DEFAULT_DATETIME_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstDateDataType::DATA_KEY_DEFAULT_DATETIME_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDateDataType::DATA_KEY_DEFAULT_DATETIME_FACTORY:
                    DateTimeFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstDataType::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check greater equal enable required.
     *
     * @return boolean
     */
    public function checkGreaterEqualEnableRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE])) ||
            (intval($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check less equal enable required.
     *
     * @return boolean
     */
    public function checkLessEqualEnableRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE])) ||
            (intval($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check multiple required.
     * If true, entity attribute value is considered as index array of date values.
     *
     * @return boolean
     */
    public function checkMultipleRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) &&
            (intval($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check multiple unique required.
     * In case multiple required, if true, each value must be unique.
     *
     * @return boolean
     */
    public function checkMultipleUniqueRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) &&
            (intval($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting list values required.
     *
     * @return boolean
     */
    public function checkListValueFormatRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute datetime value required,
     * when get action required.
     *
     * @return boolean
     */
    public function checkDtValueFormatGetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_FORMAT_SET_DATETIME_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_FORMAT_SET_DATETIME_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute datetime value required,
     * when set action required.
     *
     * @return boolean
     */
    public function checkDtValueFormatSetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_FORMAT_SET_DATETIME_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_FORMAT_SET_DATETIME_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute datetime value required,
     * when get action required, to be saved.
     *
     * @return boolean
     */
    public function checkDtValueSaveFormatGetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_DATETIME_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_DATETIME_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute multiple value required,
     * when get action required, to be saved.
     *
     * @return boolean
     */
    public function checkMultiValueSaveFormatGetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute datetime value required,
     * when set action required, to be loaded.
     *
     * @return boolean
     */
    public function checkDtValueSaveFormatSetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_DATETIME_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_DATETIME_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute multiple value required,
     * when set action required, to be loaded.
     *
     * @return boolean
     */
    public function checkMultiValueSaveFormatSetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get empty value.
     *
     * @return mixed
     */
    protected function getEmptyValue()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDateDataType::TAB_CONFIG_KEY_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstDateDataType::TAB_CONFIG_KEY_EMPTY_VALUE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get entity attribute rule configuration array,
     * for datetime value.
     * Overwrite it to set specific validation.
     *
     * Return format:
     * @see getTabRuleConfig() return array format.
     *
     * @return array
     */
    protected function getTabValidDateTimeRuleConfig()
    {
        // Init var
        $objDateTimeFactory = $this->getObjDateTimeFactory();
        $tabConfig = $this->getTabConfig();
        $dtGreaterCompareValue = (
            isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_GREATER_COMPARE_VALUE]) ?
                $tabConfig[ConstDateDataType::TAB_CONFIG_KEY_GREATER_COMPARE_VALUE] :
                null
        );
        $dtGreaterCompareValue = (
            (is_string($dtGreaterCompareValue)) ?
                $objDateTimeFactory->getObjDtFromSet($dtGreaterCompareValue, true) :
                (
                    ($dtGreaterCompareValue instanceof DateTime) ?
                        $objDateTimeFactory->getObjRefDt($dtGreaterCompareValue) :
                        null
                )
        );
        $dtLessCompareValue = (
            isset($tabConfig[ConstDateDataType::TAB_CONFIG_KEY_LESS_COMPARE_VALUE]) ?
                $tabConfig[ConstDateDataType::TAB_CONFIG_KEY_LESS_COMPARE_VALUE] :
                null
        );
        $dtLessCompareValue = (
            (is_string($dtLessCompareValue)) ?
                $objDateTimeFactory->getObjDtFromSet($dtLessCompareValue, true) :
                (
                    ($dtLessCompareValue instanceof DateTime) ?
                        $objDateTimeFactory->getObjRefDt($dtLessCompareValue) :
                        null
                )
        );

        // Build rule configuration
        $result = array(
            'type_date'
        );

        if(!is_null($dtGreaterCompareValue))
        {
            $result[] = array(
                'compare_greater',
                [
                    'compare_value' => $dtGreaterCompareValue,
                    'equal_enable_require' => $this->checkGreaterEqualEnableRequired()
                ]
            );
        }

        if(!is_null($dtLessCompareValue))
        {
            $result[] = array(
                'compare_less',
                [
                    'compare_value' => $dtLessCompareValue,
                    'equal_enable_require' => $this->checkLessEqualEnableRequired()
                ]
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get entity attribute rule configuration array,
     * for empty value.
     *
     * Return format:
     * @see getTabRuleConfig() return array format.
     *
     * @return array
     */
    protected function getTabEmptyRuleConfig()
    {
        // Return result
        return array(
            [
                'compare_equal',
                ['compare_value' => $this->getEmptyValue()]
            ]
        );
    }



    /**
     * Get specified index array of formatted list values.
     * Overwrite it to set specific formatting.
     *
     * @param array $tabListValue
     * @return array
     */
    protected function getTabListValueFormat(array $tabListValue)
    {
        // Return result
        return array_map(
            function($value) {
                return $this->getDtValueFormatSet($value);
            },
            array_values($tabListValue)
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabRuleConfig(
        $boolValueRequired = false,
        array $tabListValue = array()
    )
    {
        // Init var
        $boolValueRequired = (is_bool($boolValueRequired) ? $boolValueRequired : false);
        $tabListValue = (
            $this->checkListValueFormatRequired() ?
                $this->getTabListValueFormat($tabListValue) :
                array_values($tabListValue)
        );
        $tabEmptyRuleConfig = $this->getTabEmptyRuleConfig();
        $tabValidDateTimeRuleConfig = $this->getTabValidDateTimeRuleConfig();
        $tabValidDateTimeRuleConfig[] = array(
            'sub_rule_not',
            [
                'rule_config' => $tabEmptyRuleConfig,
                'error_message_pattern' => '%1$s is empty.'
            ]
        );
        if(count($tabListValue) > 0) {
            $tabValidDateTimeRuleConfig[] = array(
                'compare_in',
                ['compare_value' => $tabListValue]
            );
        }

        // Build rule configuration
        $result = (
            $this->checkMultipleRequired() ?
                // Check multiple value
                array_merge(
                    array(
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => $tabValidDateTimeRuleConfig,
                                'error_message_pattern' => '%1$s must be an array of valid dates.'
                            ]
                        ]
                    ),
                    (
                        $boolValueRequired ?
                            array(
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty'],
                                        'error_message_pattern' => '%1$s is empty.'
                                    ]
                                ]
                            ) :
                            array()
                    ),
                    (
                        $this->checkMultipleUniqueRequired() ?
                            array(
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function($strName, $value) {
                                            return (
                                                (!is_array($value)) ||
                                                (count($value) == count(array_unique($value)))
                                            );
                                        },
                                        'error_message_pattern' => '%1$s must contain unique values.'
                                    ]
                                ]
                            ) :
                            array()
                    )
                ) :
                // Check simple value
                (
                    $boolValueRequired ?
                        $tabValidDateTimeRuleConfig :
                        array(
                            [
                                'group_sub_rule_or',
                                [
                                    'rule_config' => [
                                        'is-empty' => $tabEmptyRuleConfig,
                                        'is-valid-date' => $tabValidDateTimeRuleConfig
                                    ],
                                    'error_message_pattern' => '%1$s must be empty or a valid date.'
                                ]
                            ]
                        )
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted datetime value,
     * when get action required.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getDtValueFormatGet($value)
    {
        // Init var
        $objDateTimeFactory = $this->getObjDateTimeFactory();
        $result = (
            ($value instanceof DateTime) ?
                $objDateTimeFactory->getStrGetDt($value) :
                $value
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when get action required.
     *
     * @return mixed
     */
    protected function getEmptyValueFormatGet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDateDataType::TAB_CONFIG_KEY_FORMAT_GET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstDateDataType::TAB_CONFIG_KEY_FORMAT_GET_EMPTY_VALUE] :
                $this->getEmptyValue()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getValueFormatGet($value)
    {
        // Init var
        $boolDtValueFormatGetRequired = $this->checkDtValueFormatGetRequired();
        $emptyValue = $this->getEmptyValue();
        $emptyValueFormatGet = $this->getEmptyValueFormatGet();
        $getValueFormatGet = function($value) use ($boolDtValueFormatGetRequired, $emptyValue, $emptyValueFormatGet) {
            return (
                ($value === $emptyValue) ?
                    $emptyValueFormatGet :
                    (
                        $boolDtValueFormatGetRequired ?
                            $this->getDtValueFormatGet($value) :
                            $value
                    )
            );
        };
        $result = (
            $this->checkMultipleRequired() ?
                // Format multiple value
                (
                    is_array($value) ?
                        array_map(
                            function($subValue) use ($getValueFormatGet) {
                                return $getValueFormatGet($subValue);
                            },
                            array_values($value)
                        ) :
                        parent::getValueFormatGet($value)
                ) :
                // Format simple value
                $getValueFormatGet($value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted datetime value,
     * when set action required.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getDtValueFormatSet($value)
    {
        // Init var
        $objDateTimeFactory = $this->getObjDateTimeFactory();
        $result = (
            (
                is_string($value) &&
                (!is_null($dt = $objDateTimeFactory->getObjDtFromSet($value, true)))
            ) ?
                $dt :
                (
                    ($value instanceof DateTime) ?
                        $objDateTimeFactory->getObjRefDt($value) :
                        $value
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when set action required.
     *
     * @return mixed
     */
    protected function getEmptyValueFormatSet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDateDataType::TAB_CONFIG_KEY_FORMAT_SET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstDateDataType::TAB_CONFIG_KEY_FORMAT_SET_EMPTY_VALUE] :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getValueFormatSet($value)
    {
        // Init var
        $boolDtValueFormatSetRequired = $this->checkDtValueFormatSetRequired();
        $emptyValue = $this->getEmptyValue();
        $emptyValueFormatSet = $this->getEmptyValueFormatSet();
        $getValueFormatSet = function($value) use ($boolDtValueFormatSetRequired, $emptyValue, $emptyValueFormatSet) {
            return (
                ($value === $emptyValueFormatSet) ?
                    $emptyValue :
                    (
                        $boolDtValueFormatSetRequired ?
                            $this->getDtValueFormatSet($value) :
                            $value
                    )
            );
        };
        $result = (
            $this->checkMultipleRequired() ?
                // Format multiple value
                (
                    is_array($value) ?
                        array_map(
                            function($subValue) use ($getValueFormatSet) {
                                return $getValueFormatSet($subValue);
                            },
                            array_values($value)
                        ) :
                        parent::getValueFormatSet($value)
                ) :
                // Format simple value
                $getValueFormatSet($value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted datetime value,
     * when get action required, to be saved.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getDtValueSaveFormatGet($value)
    {
        // Init var
        $objDateTimeFactory = $this->getObjDateTimeFactory();
        $result = (
            ($value instanceof DateTime) ?
                $objDateTimeFactory->getStrSaveDt($value) :
                parent::getValueSaveFormatGet($value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when get action required, to be saved.
     *
     * @return mixed
     */
    protected function getEmptyValueSaveFormatGet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_EMPTY_VALUE] :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted multiple value,
     * when get action required, to be saved.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getMultiValueSaveFormatGet($value)
    {
        // Return result
        return (
            is_array($value) ?
                json_encode($value) :
                parent::getValueSaveFormatGet($value)
        );
    }



    /**
     * @inheritdoc
     */
    public function getValueSaveFormatGet($value)
    {
        // Init var
        $boolDtValueSaveFormatGetRequired = $this->checkDtValueSaveFormatGetRequired();
        $emptyValue = $this->getEmptyValue();
        $emptyValueSaveFormatGet = $this->getEmptyValueSaveFormatGet();
        $getValueSaveFormatGet = function($value) use ($boolDtValueSaveFormatGetRequired, $emptyValue, $emptyValueSaveFormatGet) {
            return (
                ($value === $emptyValue) ?
                    $emptyValueSaveFormatGet :
                    (
                        $boolDtValueSaveFormatGetRequired ?
                            $this->getDtValueSaveFormatGet($value) :
                            $value
                    )
            );
        };

        // Format multiple value, if required
        if($this->checkMultipleRequired())
        {
            if(is_array($value))
            {
                $result = array_map(
                    function($subValue) use ($getValueSaveFormatGet) {
                        return $getValueSaveFormatGet($subValue);
                    },
                    array_values($value)
                );
                $result = (
                    $this->checkMultiValueSaveFormatGetRequired() ?
                        $this->getMultiValueSaveFormatGet($result) :
                        $result
                );
            }
            else
            {
                $result = parent::getValueSaveFormatGet($value);
            }
        }
        // Else: format simple value
        else
        {
            $result = $getValueSaveFormatGet($value);
        }

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted datetime value,
     * when set action required, to be loaded.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getDtValueSaveFormatSet($value)
    {
        // Init var
        $objDateTimeFactory = $this->getObjDateTimeFactory();
        $result = (
            (
                is_string($value) &&
                (!is_null($dt = $objDateTimeFactory->getObjDtFromSave($value, true)))
            ) ?
                $dt :
                parent::getValueSaveFormatSet($value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted empty value,
     * when set action required, to be loaded.
     *
     * @return mixed
     */
    protected function getEmptyValueSaveFormatSet()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_EMPTY_VALUE, $tabConfig) ?
                $tabConfig[ConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_EMPTY_VALUE] :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted multiple value,
     * when set action required, to be loaded.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getMultiValueSaveFormatSet($value)
    {
        // Return result
        return (
            is_array($resultValue = json_decode($value, true)) ?
                $resultValue :
                parent::getValueSaveFormatSet($value)
        );
    }



    /**
     * @inheritdoc
     */
    public function getValueSaveFormatSet($value)
    {
        // Init var
        $boolDtValueSaveFormatSetRequired = $this->checkDtValueSaveFormatSetRequired();
        $emptyValue = $this->getEmptyValue();
        $emptyValueSaveFormatSet = $this->getEmptyValueSaveFormatSet();
        $getValueSaveFormatSet = function($value) use ($boolDtValueSaveFormatSetRequired, $emptyValue, $emptyValueSaveFormatSet) {
            return (
                ($value === $emptyValueSaveFormatSet) ?
                    $emptyValue :
                    (
                        $boolDtValueSaveFormatSetRequired ?
                            $this->getDtValueSaveFormatSet($value) :
                            $value
                    )
            );
        };

        // Format multiple value, if required
        if($this->checkMultipleRequired())
        {
            $result = (
                $this->checkMultiValueSaveFormatSetRequired() ?
                    $this->getMultiValueSaveFormatSet($value) :
                    parent::getValueSaveFormatSet($value)
            );
            if(is_array($result))
            {
                $result = array_map(
                    function($subValue) use ($getValueSaveFormatSet) {
                        return $getValueSaveFormatSet($subValue);
                    },
                    array_values($result)
                );
            }
        }
        // Else: format simple value
        else
        {
            $result = $getValueSaveFormatSet($value);
        }

        // Return result
        return $result;
    }



}