<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\provider\standard\library;



class ConstStandardAttrProvider
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_ATTRIBUTE_COLLECTION = 'objAttributeCollection';



	// Exception message constants
    const EXCEPT_MSG_ATTRIBUTE_COLLECTION_INVALID_FORMAT =
        'Following attribute collection "%1$s" invalid! It must be a valid attribute collection object.';



}