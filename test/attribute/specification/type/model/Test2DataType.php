<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\test\attribute\specification\type\model;

use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;



class Test2DataType extends DefaultDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstDataType::TAB_CONFIG_KEY_TYPE => 'string'
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabRuleConfig(
        $boolValueRequired = false,
        array $tabListValue = array()
    )
    {
        // Return result
        return array(
            'type_string'
        );
    }



}