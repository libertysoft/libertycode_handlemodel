<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\handle_model\attribute\api\AttributeInterface;
use liberty_code\handle_model\attribute\api\AttributeCollectionInterface;



class ToolBoxEntity extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get entity index array of attribute configuration,
     * from specified index array of attribute objects.
     *
     * Return array format:
     * @see AttributeCollectionInterface::getTabEntityAttrConfig() return array format.
     *
     * @param AttributeInterface[] $tabAttribute
     * @return array
     */
    public static function getTabAttributeConfig(array $tabAttribute)
    {
        // Init var
        $result = array();

        // Run each attribute
        foreach($tabAttribute as $objAttribute)
        {
            // Set attribute configuration, if required
            if($objAttribute instanceof AttributeInterface)
            {
                $result[] = $objAttribute->getTabEntityAttrConfig();
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get entity rule configurations array,
     * from specified index array of attribute objects.
     *
     * Return array format:
     * @see AttributeCollectionInterface::getTabEntityAttrRuleConfig() return array format.
     *
     * @param AttributeInterface[] $tabAttribute
     * @return array
     */
    public static function getTabAttributeRuleConfig(array $tabAttribute)
    {
        // Init var
        $result = array();

        // Run each attribute
        foreach($tabAttribute as $objAttribute)
        {
            // Set rule configuration, if required
            if($objAttribute instanceof AttributeInterface)
            {
                $strAttributeKey = $objAttribute->getStrAttributeKey();
                $tabRuleConfig = $objAttribute->getTabEntityAttrRuleConfig();
                $result[$strAttributeKey] = $tabRuleConfig;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted value when get action required,
     * from specified attribute key,
     * from specified attribute collection.
     *
     * @param AttributeCollectionInterface $objAttributeCollection
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public static function getAttributeValueFormatGet(
        AttributeCollectionInterface $objAttributeCollection,
        $strKey,
        $value
    )
    {
        // Init var
        $objAttribute = $objAttributeCollection->getObjAttribute($strKey);
        $result = (
            (!is_null($objAttribute)) ?
                $objAttribute->getEntityAttrValueFormatGet($value) :
                $value
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted value when set action required,
     * from specified attribute key,
     * from specified attribute collection.
     *
     * @param AttributeCollectionInterface $objAttributeCollection
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public static function getAttributeValueFormatSet(
        AttributeCollectionInterface $objAttributeCollection,
        $strKey,
        $value
    )
    {
        // Init var
        $objAttribute = $objAttributeCollection->getObjAttribute($strKey);
        $result = (
            (!is_null($objAttribute)) ?
                $objAttribute->getEntityAttrValueFormatSet($value) :
                $value
        );

        // Return result
        return $result;
    }



}