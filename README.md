LibertyCode_HandleModel
=======================



Description
-----------

Library contains handle model components, 
to handle model components.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Framework library implementation requirement
-----------------------------------------

1. Library repository: liberty_code/validation: version 1.0

    - Standard rules implementation: 
        
        Default attribute specification provides entity attribute rule configuration,
        compatible for validator, containing all standard rules, 
        added on its collection rule collection.
        
    - Collection rule collection implementation: 
        
        Default attribute specification provides entity attribute rule configuration,
        compatible for validator, using collection rule collection, 
        to require its rule configuration formatting.
        
---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/handle_model ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/handle_model": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Attribute specification data type

Data type represents a specific type, 
allowing to manage entity attribute value, in specific way.

_Elements_

- DataType

    Allows to design specific type,
    to manage entity attribute value (validation, formatting).
    
- StringDataType
    
    Extends data type features. 
    Allows to manage entity attribute string value.

- NumericDataType
    
    Extends data type features. 
    Allows to manage entity attribute numeric value.
 
- BooleanDataType

    Extends data type features. 
    Allows to manage entity attribute boolean value.

- DateDataType

    Extends data type features. 
    Allows to manage entity attribute date value.
    
- DataTypeCollection

    Allows to design collection of data types.
    Allows to retrieve specified data type, 
    from specified list of data types.
    
- DataTypeFactory

    Allows to design data type factory, 
    to provide new or specified data type instance,
    from specified configuration.
    
- BaseDataTypeFactory

    Extends data type factory features. 
    Can be consider as base of all data type factories.
    
- StandardDataTypeFactory

    Extends base data type factory features. 
    Provides data type instance.
    
- DataTypeBuilder
    
    Uses array of source data to hydrate data type collection.

_Example_

```php
// Get data type factory
use liberty_code\model\datetime\factory\model\DefaultDateTimeFactory;
use liberty_code\handle_model\attribute\specification\type\factory\standard\model\StandardDataTypeFactory;
$dataTypeFactory = new StandardDataTypeFactory(
    new DefaultDateTimeFactory(array(...))
);
...
// Get new data type, from configuration
$dataType = $dataTypeFactory->getObjDataType(array(...));
...
// Check matches with specific type
var_dump($dataType->checkMatches(...));
...
// Get entity attribute rule configuration
var_dump($dataType->getTabRuleConfig());
...
// Get entity attribute formatted value
var_dump($dataType->getValueFormatGet(...));
var_dump($dataType->getValueFormatSet(...));
var_dump($dataType->getValueSaveFormatGet(...));
var_dump($dataType->getValueSaveFormatSet(...));
...
// Get data type collection
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataTypeCollection;
$dataTypeCollection = new DefaultDataTypeCollection();
...
// Get data type builder
use liberty_code\handle_model\attribute\specification\type\build\model\DefaultDataTypeBuilder;
$dataTypeBuilder = new DefaultDataTypeBuilder($dataTypeFactory);
...
// Hydrate data type collection
$dataTypeBuilder->setTabDataSrc(array(...));
$dataTypeBuilder->hydrateDataTypeCollection($dataTypeCollection);
...
foreach($dataTypeCollection->getTabKey() as $key) {
    echo($dataTypeCollection->getObjDataType($key)->getStrKey() .'<br />');
}
/**
 * Show: 
 * Data type key 1
 * ...
 * Data type key N
 */
...
```

#### Attribute specification

Attribute specification allows to define specific scope of data types,
to manage entity attribute value.

_Elements_

- AttrSpec

    Allows to provide a scope of data types, 
    to manage entity attribute value (validation, formatting).
    
- StandardAttrSpec

    Extends attribute specification features. 
    Uses data type objects,
    to manage entity attribute value.

_Example_

```php
// Get attribute specification
use liberty_code\handle_model\attribute\specification\standard\model\StandardAttrSpec;
$attrSpec = new StandardAttrSpec($dataTypeCollection, array(...));
...
// Get array of available data types
var_dump($attrSpec->getTabDataType());
...
// Get entity attribute rule configuration, from specific data type
$dataType = '...';
var_dump($attrSpec->getTabDataTypeRuleConfig($dataType));
...
// Get entity attribute formatted value, from specific data type
var_dump($attrSpec->getDataTypeValueFormatGet($dataType, ...));
var_dump($attrSpec->getDataTypeValueFormatSet($dataType, ...));
var_dump($attrSpec->getDataTypeValueSaveFormatGet($dataType, ...));
var_dump($attrSpec->getDataTypeValueSaveFormatSet($dataType, ...));
...
```

#### Attribute

Attribute contains configuration to design specific attribute,
can be used in configured entities.

_Elements_

- Attribute

    Contains configuration, 
    to design specific attribute,
    used on entity.

- AttributeCollection

    Allows to design collection of attributes.
    Allows to provide entity attributes configuration.

- SaveAttribute

    Extends attribute features. 
    Can be used on save entity.

- SaveAttributeCollection

    Extends attribute collection features. 
    Allows to provide save entity attributes configuration.

- AttributeFactory

    Allows to design attribute factory, 
    to provide new or specified attribute instances,
    from specified configuration.
    
- StandardAttributeFactory

    Extends attribute factory features. 
    Provides attribute instance.
    
- Builder
    
    Builder allows to hydrate attribute collection, 
    with attributes.
    
_Example_

```php
// Get attribute factory
use liberty_code\handle_model\attribute\factory\standard\model\StandardAttributeFactory;
$attributeFactory = new StandardAttributeFactory(null, $provider, $attrSpec);
...
// Get attribute builder
use liberty_code\handle_model\attribute\build\model\DefaultBuilder;
$attributeBuilder = new DefaultBuilder($attributeFactory);
...
// Get attribute collection
use liberty_code\handle_model\attribute\repository\model\DefaultSaveAttributeCollection;
$attributeCollection = new DefaultSaveAttributeCollection();
...
// Hydrate attribute collection
$attributeBuilder->setTabDataSrc(array(...));
$attributeBuilder->hydrateAttributeCollection($attributeCollection);
...
foreach($attributeCollection->getTabAttributeKey() as $attributeKey) {
    echo($attributeCollection->getObjAttribute($attributeKey)->getStrAttributeKey() .'<br />');
}
/**
 * Show: 
 * Attribute key 1
 * ...
 * Attribute key N
 */
...
```

#### Attribute provider

Attribute provider allows to provide attribute information for entity.

_Elements_

- AttrProvider

    Allows to provide attribute information for entity.
    
- StandardAttrProvider

    Extends attribute provider features. 
    Uses attribute objects,
    to provide attribute information for entity.
    
_Example_

```php
use liberty_code\handle_model\attribute\provider\standard\model\StandardAttrProvider;
$attrProvider = new StandardAttrProvider($attributeCollection);
...
// Get entity attribute configuration array
var_dump($attrProvider->getTabEntityAttrConfig());
...
// Get entity attribute rule configurations array
var_dump($attrProvider->getTabEntityAttrRuleConfig());
...
```

#### Entity

Handle configured entity uses attribute provider, 
to build and manage its attributes.

_Elements_

- HandleValidatorConfigEntity

    Extends validator configured entity features. 
    Uses attribute provider, 
    to build and manage its attributes.
    
- HandleSaveConfigEntity

    Extends save configured entity features. 
    Uses attribute provider, 
    to build and manage its attributes.
    
_Example_

```php
...
// Get new entity
use liberty_code\handle_model\entity\model\HandleValidatorConfigEntity
$handleValidatorConfigEntity = new HandleValidatorConfigEntity(
    $attrProvider,
    array(),
    $validator
);
...
if($handleValidatorConfigEntity->checkAttributeValid()) {
    $tabAttributeKey = $handleValidatorConfigEntity->getTabAttributeKey();
    foreach($tabAttributeKey as $attributeKey) {
        echo($handleValidatorConfigEntity->$attributeKey . '<br />');
    }
}
/**
 * Show: 
 * Entity attribute 1 value
 * ...
 * Entity attribute N value
 */
...
```

---



Test
----

#### Unit test

Unit tests allows to test components features, 
and to automate their validation.

1. Requirement
    
    - Composer
    
        It requires composer installation.
        For more information: https://getcomposer.org
        
    - Command: Dependencies installation
    
        ```sh
        php composer.phar install
        ```
    
2. Command: Run unit tests
    
    ```sh
    vendor\bin\phpunit
    ```

3. Note

    It uses PHPUnit to handle unit tests.
    For more information: https://phpunit.readthedocs.io

---


