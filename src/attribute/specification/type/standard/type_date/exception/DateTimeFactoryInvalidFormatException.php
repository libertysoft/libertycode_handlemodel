<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\standard\type_date\exception;

use Exception;

use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\library\ConstDateDataType;



class DateTimeFactoryInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $factory
     */
	public function __construct($factory)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDateDataType::EXCEPT_MSG_DATETIME_FACTORY_INVALID_FORMAT,
            mb_strimwidth(strval($factory), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified factory has valid format.
	 * 
     * @param mixed $factory
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($factory)
    {
		// Init var
		$result = (
            (!is_null($factory)) &&
			($factory instanceof DateTimeFactoryInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($factory);
		}
		
		// Return result
		return $result;
    }
	
	
	
}