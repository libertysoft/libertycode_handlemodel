<?php
/**
 * Description :
 * This class allows to define default data type builder class.
 * Default data type builder allows to populate data type collection,
 * from a specified array of source data.
 *
 * Default data type builder uses the following specified source data, to hydrate data type collection:
 * [
 *     // Data type 1
 *     @see DataTypeFactoryInterface configuration array format,
 *
 *     ...,
 *
 *     // Data type N
 *     ...
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\handle_model\attribute\specification\type\build\api\DataTypeBuilderInterface;

use liberty_code\handle_model\attribute\specification\type\api\DataTypeCollectionInterface;
use liberty_code\handle_model\attribute\specification\type\factory\api\DataTypeFactoryInterface;
use liberty_code\handle_model\attribute\specification\type\build\library\ConstDataTypeBuilder;
use liberty_code\handle_model\attribute\specification\type\build\exception\FactoryInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\build\exception\DataSrcInvalidFormatException;



/**
 * @method DataTypeFactoryInterface getObjFactory() Get data type factory object.
 * @method array getTabDataSrc() get data source array.
 * @method void setObjFactory(DataTypeFactoryInterface $objFactory) Set data type factory object.
 * @method void setTabDataSrc(array $tabDataSrc) Set data source array.
 */
class DefaultDataTypeBuilder extends FixBean implements DataTypeBuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DataTypeFactoryInterface $objFactory
     * @param array $tabDataSrc = array()
     */
    public function __construct(
        DataTypeFactoryInterface $objFactory,
        array $tabDataSrc = array()
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init data type factory
        $this->setObjFactory($objFactory);

        // Init data source
        $this->setTabDataSrc($tabDataSrc);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstDataTypeBuilder::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstDataTypeBuilder::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        if(!$this->beanExists(ConstDataTypeBuilder::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->__beanTabData[ConstDataTypeBuilder::DATA_KEY_DEFAULT_DATA_SRC] = array();
        }
    }



    /**
     * @inheritdoc
     * @throws DataSrcInvalidFormatException
     */
    public function hydrateDataTypeCollection(DataTypeCollectionInterface $objDataTypeCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabDataType = array();
        foreach($tabDataSrc as $key => $tabConfig)
        {
            // Get data type
            $key = (is_string($key) ? $key : null);
            $objDataType = (
                is_array($tabConfig) ?
                    $objFactory->getObjDataType($tabConfig, $key) :
                    null
            );

            // Check data type
            if(is_null($objDataType))
            {
                throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
            }

            // Register data type
            $tabDataType[] = $objDataType;
        }

        // Clear data type collection, if required
        if($boolClear)
        {
            $objDataTypeCollection->removeDataTypeAll();
        }

        // Register data types on collection
        $objDataTypeCollection->setTabDataType($tabDataType);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstDataTypeBuilder::DATA_KEY_DEFAULT_FACTORY,
            ConstDataTypeBuilder::DATA_KEY_DEFAULT_DATA_SRC
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstDataTypeBuilder::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

				case ConstDataTypeBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}
	
	
	
}