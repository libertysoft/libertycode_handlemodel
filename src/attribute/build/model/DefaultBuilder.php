<?php
/**
 * Description :
 * This class allows to define default builder class.
 * Default builder allows to populate attribute collection,
 * from a specified array of source data.
 *
 * Default builder uses the following specified source data, to hydrate attribute collection:
 * [
 *     // Attribute configuration 1
 *     Attribute factory configuration (@see AttributeFactoryInterface ),
 *
 *     ...,
 *
 *     // Attribute configuration N
 *     ...
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\handle_model\attribute\build\api\BuilderInterface;

use liberty_code\handle_model\attribute\api\AttributeCollectionInterface;
use liberty_code\handle_model\attribute\factory\api\AttributeFactoryInterface;
use liberty_code\handle_model\attribute\build\library\ConstBuilder;
use liberty_code\handle_model\attribute\build\exception\FactoryInvalidFormatException;
use liberty_code\handle_model\attribute\build\exception\DataSrcInvalidFormatException;



/**
 * @method AttributeFactoryInterface getObjFactory() Get attribute factory object.
 * @method void setObjFactory(AttributeFactoryInterface $objFactory) Set attribute factory object.
 * @method array getTabDataSrc() get data source array.
 * @method void setTabDataSrc(array $tabDataSrc) Set data source array.
 */
class DefaultBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttributeFactoryInterface $objFactory
     * @param array $tabDataSrc = array()
     */
    public function __construct(
        AttributeFactoryInterface $objFactory,
        array $tabDataSrc = array()
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init attribute factory
        $this->setObjFactory($objFactory);

        // Init data source
        $this->setTabDataSrc($tabDataSrc);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC] = array();
        }
    }



    /**
     * @inheritdoc
     */
    public function hydrateAttributeCollection(AttributeCollectionInterface $objAttributeCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();


        // Run each data source
        $tabAttribute = array();
        foreach($tabDataSrc as $key => $tabConfig)
        {
            // Get new attribute
            $key = (is_string($key) ? $key : null);
            $objAttribute = $objFactory->getObjAttribute($tabConfig, $key);

            // Register attribute, if found
            if(!is_null($objAttribute))
            {
                $tabAttribute[] = $objAttribute;
            }
            // Throw exception if attribute not found, from data source
            else
            {
                throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
            }
        }

        // Clear attributes from collection, if required
        if($boolClear)
        {
            $objAttributeCollection->removeAttributeAll();
        }

        // Register attributes on collection
        $objAttributeCollection->setTabAttribute($tabAttribute);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstBuilder::DATA_KEY_DEFAULT_FACTORY,
            ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstBuilder::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}

	
	
}