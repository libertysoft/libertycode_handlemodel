<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\standard\type_string\library;



class ConstStringDataType
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_MULTILINE_REQUIRE = 'multiline_require';
    const TAB_CONFIG_KEY_REGEXP = 'regexp';
    const TAB_CONFIG_KEY_SIZE_MIN = 'size_min';
    const TAB_CONFIG_KEY_SIZE_MAX = 'size_max';
    const TAB_CONFIG_KEY_MULTIPLE_REQUIRE = 'multiple_require';
    const TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE = 'multiple_unique_require';
    const TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE = 'format_list_value_require';
    const TAB_CONFIG_KEY_FORMAT_SET_STRING_VALUE_REQUIRE = 'format_set_string_value_require';
    const TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE = 'save_format_get_multiple_value_require';
    const TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE = 'save_format_set_multiple_value_require';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the string data type configuration standard.';



}