<?php

// Init var
$strAttrSpecRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strAttrSpecRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strAttrSpecRootAppPath . '/include/Include.php');

// Load test
require($strAttrSpecRootAppPath . '/test/attribute/specification/type/build/boot/DataTypeBuilderBootstrap.php');
require($strAttrSpecRootAppPath . '/test/attribute/specification/boot/AttrSpecBootstrap.php');



// Init data type collection
$tabDataSrc = array(
    [
        'type' => 'string',
        'config' => [
            'type' => 'string'
        ]
    ],
    [
        'type' => 'numeric',
        'config' => [
            'type' => 'numeric'
        ]
    ],
    [
        'type' => 'numeric',
        'config' => [
            'type' => 'integer',
            'integer_require' => true,
            'greater_compare_value' => 0
        ]
    ],
    [
        'type' => 'boolean',
        'config' => [
            'type' => 'boolean'
        ]
    ],
    [
        'type' => 'date',
        'config' => [
            'type' => 'date'
        ]
    ]
);
$objDataTypeBuilder->setTabDataSrc($tabDataSrc);
$objDataTypeBuilder->hydrateDataTypeCollection($objDataTypeCollection, true);


