<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\library;



class ConstAttrSpec
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_CACHE_REPO = 'objCacheRepo';



    // Configuration
    const TAB_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_CONFIG_KEY_CACHE_DATA_TYPE_KEY = 'cache_data_type_key';
    const TAB_CONFIG_KEY_CACHE_RULE_CONFIG_KEY_PATTERN = 'cache_rule_config_key_pattern';
    const TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_GET_KEY_PATTERN = 'cache_value_format_get_key_pattern';
    const TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_SET_KEY_PATTERN = 'cache_value_format_set_key_pattern';
    const TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_GET_KEY_PATTERN = 'cache_value_save_format_get_key_pattern';
    const TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_SET_KEY_PATTERN = 'cache_value_save_format_set_key_pattern';
    const TAB_CONFIG_KEY_CACHE_SET_CONFIG = 'cache_set_config';

    // Cache configuration
    const CACHE_DEFAULT_DATA_TYPE_KEY = 'data_type';
    const CACHE_DEFAULT_RULE_CONFIG_KEY_PATTERN = 'rule_config_%1$s%2$s%3$s';
    const CACHE_DEFAULT_VALUE_FORMAT_GET_KEY_PATTERN = 'value_format_get_%1$s%2$s';
    const CACHE_DEFAULT_VALUE_FORMAT_SET_KEY_PATTERN = 'value_format_set_%1$s%2$s';
    const CACHE_DEFAULT_VALUE_SAVE_FORMAT_GET_KEY_PATTERN = 'value_save_format_get_%1$s%2$s';
    const CACHE_DEFAULT_VALUE_SAVE_FORMAT_SET_KEY_PATTERN = 'value_save_format_set_%1$s%2$s';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default attribute specification configuration standard.';
    const EXCEPT_MSG_CACHE_REPO_INVALID_FORMAT = 'Following cache repository "%1$s" invalid! It must be a cache repository object.';
    const EXCEPT_MSG_DATA_TYPE_INVALID_FORMAT = 'Following data type "%1$s" invalid! The type must be a valid string, not empty.';



}