<?php
/**
 * This class allows to define default save attribute collection class.
 * key: attribute key => attribute|save attribute.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\repository\model;

use liberty_code\handle_model\attribute\model\DefaultAttributeCollection;
use liberty_code\handle_model\attribute\repository\api\SaveAttributeCollectionInterface;

use liberty_code\handle_model\attribute\repository\library\ToolBoxEntity;



class DefaultSaveAttributeCollection extends DefaultAttributeCollection implements SaveAttributeCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods entity attribute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatGet($strKey, $value)
    {
        // Return result
        return ToolBoxEntity::getAttributeValueSaveFormatGet(
            $this,
            $strKey,
            $value
        );
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatSet($strKey, $value)
    {
        // Return result
        return ToolBoxEntity::getAttributeValueSaveFormatSet(
            $this,
            $strKey,
            $value
        );
    }



}