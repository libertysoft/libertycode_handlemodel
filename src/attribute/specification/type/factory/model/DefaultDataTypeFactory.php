<?php
/**
 * Description :
 * This class allows to define default data type factory class.
 * Can be consider is base of all data type factory type.
 *
 * Default data type factory uses the following specified configuration, to get and hydrate data type:
 * [
 *     type(optional): "string constant to determine data type type"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\handle_model\attribute\specification\type\factory\api\DataTypeFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;
use liberty_code\handle_model\attribute\specification\type\factory\library\ConstDataTypeFactory;
use liberty_code\handle_model\attribute\specification\type\factory\exception\FactoryInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|DataTypeFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|DataTypeFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultDataTypeFactory extends DefaultFactory implements DataTypeFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DataTypeFactoryInterface $objFactory = null
     */
    public function __construct(
        DataTypeFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init data type factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstDataTypeFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstDataTypeFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified data type.
     * Overwrite it to set specific hydration.
     *
     * @param DataTypeInterface $objDataType
     * @param array $tabConfigFormat
     */
    protected function hydrateDataType(DataTypeInterface $objDataType, array $tabConfigFormat)
    {

    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstDataTypeFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDataTypeFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified data type object.
     *
     * @param DataTypeInterface $objDataType
     * @param array $tabConfigFormat
     * @return boolean
     */
    protected function checkConfigIsValid(DataTypeInterface $objDataType, array $tabConfigFormat)
    {
        // Init var
        $strDataTypeClassPath = $this->getStrDataTypeClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strDataTypeClassPath)) &&
            ($strDataTypeClassPath == get_class($objDataType))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param null|string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = (
            array_key_exists(ConstDataTypeFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat) ?
                $tabConfigFormat[ConstDataTypeFactory::TAB_CONFIG_KEY_TYPE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string class path of data type,
     * from specified configured type.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrDataTypeClassPathFromType($strConfigType);



    /**
     * Get string class path of data type engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     */
    protected function getStrDataTypeClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrDataTypeClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrDataTypeClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrDataTypeClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrDataTypeClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance data type,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|DataTypeInterface
     */
    protected function getObjDataTypeNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrDataTypeClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance data type engine.
     *
     * @param array $tabConfigFormat
     * @param null|DataTypeInterface $objDataType = null
     * @return null|DataTypeInterface
     */
    protected function getObjDataTypeEngine(
        array $tabConfigFormat,
        DataTypeInterface $objDataType = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objDataType = (
            is_null($objDataType) ?
                $this->getObjDataTypeNew($strConfigType) :
                $objDataType
        );

        // Get and hydrate data type, if required
        if(
            (!is_null($objDataType)) &&
            $this->checkConfigIsValid($objDataType, $tabConfigFormat)
        )
        {
            $this->hydrateDataType($objDataType, $tabConfigFormat);
            $result = $objDataType;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjDataType(
        array $tabConfig,
        $strConfigKey = null,
        DataTypeInterface $objDataType = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjDataTypeEngine($tabConfigFormat, $objDataType);

        // Get data type from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjDataType($tabConfig, $strConfigKey, $objDataType);
        }

        // Return result
        return $result;
    }



}