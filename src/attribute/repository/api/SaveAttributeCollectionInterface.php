<?php
/**
 * Description :
 * This class allows to describe behavior of save attribute collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\repository\api;

use liberty_code\handle_model\attribute\api\AttributeCollectionInterface;



interface SaveAttributeCollectionInterface extends AttributeCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods entity attribute
    // ******************************************************************************

    /**
     * Get specified entity attribute formatted value when get action required, to be saved,
     * from specified attribute key.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueSaveFormatGet($strKey, $value);



    /**
     * Get specified entity attribute formatted value when set action required, to be loaded,
     * from specified attribute key.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueSaveFormatSet($strKey, $value);
}