<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\provider\standard\exception;

use liberty_code\handle_model\attribute\api\AttributeCollectionInterface;
use liberty_code\handle_model\attribute\provider\standard\library\ConstStandardAttrProvider;



class AttributeCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $attributeCollection
     */
	public function __construct($attributeCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStandardAttrProvider::EXCEPT_MSG_ATTRIBUTE_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($attributeCollection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified attribute collection has valid format.
	 * 
     * @param mixed $attributeCollection
     * @param null|string $strFixClassPath = null
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($attributeCollection, $strFixClassPath = null)
    {
		// Init var
        $strFixClassPath = (is_string($strFixClassPath) ? $strFixClassPath : null);
		$result = (
			(!is_null($attributeCollection)) &&
			($attributeCollection instanceof AttributeCollectionInterface) &&
            (
                is_null($strFixClassPath) ||
                ($attributeCollection instanceof $strFixClassPath)
            )
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($attributeCollection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}