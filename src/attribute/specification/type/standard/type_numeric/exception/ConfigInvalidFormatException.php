<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\standard\type_numeric\exception;

use Exception;

use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\library\ConstNumericDataType;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstNumericDataType::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid integer required option
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_INTEGER_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNumericDataType::TAB_CONFIG_KEY_INTEGER_REQUIRE]) ||
                    is_int($config[ConstNumericDataType::TAB_CONFIG_KEY_INTEGER_REQUIRE]) ||
                    (
                        is_string($config[ConstNumericDataType::TAB_CONFIG_KEY_INTEGER_REQUIRE]) &&
                        ctype_digit($config[ConstNumericDataType::TAB_CONFIG_KEY_INTEGER_REQUIRE])
                    )
                )
            ) &&

            // Check valid greater compare value
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_GREATER_COMPARE_VALUE])) ||
                is_numeric($config[ConstNumericDataType::TAB_CONFIG_KEY_GREATER_COMPARE_VALUE])
            ) &&

            // Check valid greater equal enable required option
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNumericDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE]) ||
                    is_int($config[ConstNumericDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE]) ||
                    (
                        is_string($config[ConstNumericDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE]) &&
                        ctype_digit($config[ConstNumericDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE])
                    )
                )
            ) &&

            // Check valid less compare value
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_LESS_COMPARE_VALUE])) ||
                is_numeric($config[ConstNumericDataType::TAB_CONFIG_KEY_LESS_COMPARE_VALUE])
            ) &&

            // Check valid less equal enable required option
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNumericDataType::TAB_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE]) ||
                    is_int($config[ConstNumericDataType::TAB_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE]) ||
                    (
                        is_string($config[ConstNumericDataType::TAB_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE]) &&
                        ctype_digit($config[ConstNumericDataType::TAB_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE])
                    )
                )
            ) &&

            // Check valid multiple required option
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) ||
                    is_int($config[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) ||
                    (
                        is_string($config[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) &&
                        ctype_digit($config[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE])
                    )
                )
            ) &&

            // Check valid multiple unique required option
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) ||
                    is_int($config[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) ||
                    (
                        is_string($config[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) &&
                        ctype_digit($config[ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE])
                    )
                )
            ) &&

            // Check valid formatting list value required option
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE]) ||
                    is_int($config[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE]) ||
                    (
                        is_string($config[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE]) &&
                        ctype_digit($config[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE])
                    )
                )
            ) &&

            // Check valid formatting entity attribute numeric value required option, when set action required
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_SET_NUMERIC_VALUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_SET_NUMERIC_VALUE_REQUIRE]) ||
                    is_int($config[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_SET_NUMERIC_VALUE_REQUIRE]) ||
                    (
                        is_string($config[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_SET_NUMERIC_VALUE_REQUIRE]) &&
                        ctype_digit($config[ConstNumericDataType::TAB_CONFIG_KEY_FORMAT_SET_NUMERIC_VALUE_REQUIRE])
                    )
                )
            ) &&

            // Check valid formatting entity attribute numeric value required option, when get action required, to be saved
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_NUMERIC_VALUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_NUMERIC_VALUE_REQUIRE]) ||
                    is_int($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_NUMERIC_VALUE_REQUIRE]) ||
                    (
                        is_string($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_NUMERIC_VALUE_REQUIRE]) &&
                        ctype_digit($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_NUMERIC_VALUE_REQUIRE])
                    )
                )
            ) &&

            // Check valid formatting entity attribute multiple value required option, when get action required, to be saved
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE]) ||
                    is_int($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE]) ||
                    (
                        is_string($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE]) &&
                        ctype_digit($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE])
                    )
                )
            ) &&

            // Check valid formatting entity attribute numeric value required option, when set action required, to be loaded
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_NUMERIC_VALUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_NUMERIC_VALUE_REQUIRE]) ||
                    is_int($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_NUMERIC_VALUE_REQUIRE]) ||
                    (
                        is_string($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_NUMERIC_VALUE_REQUIRE]) &&
                        ctype_digit($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_NUMERIC_VALUE_REQUIRE])
                    )
                )
            ) &&

            // Check valid formatting entity attribute multiple value required option, when set action required, to be loaded
            (
                (!isset($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE]) ||
                    is_int($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE]) ||
                    (
                        is_string($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE]) &&
                        ctype_digit($config[ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}