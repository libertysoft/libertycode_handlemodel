<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\repository\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\handle_model\attribute\repository\api\SaveAttributeInterface;
use liberty_code\handle_model\attribute\repository\api\SaveAttributeCollectionInterface;



class ToolBoxEntity extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get specified entity attribute formatted value when get action required, to be saved,
     * from specified attribute key,
     * from specified attribute collection.
     *
     * @param SaveAttributeCollectionInterface $objAttributeCollection
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public static function getAttributeValueSaveFormatGet(
        SaveAttributeCollectionInterface $objAttributeCollection,
        $strKey,
        $value
    )
    {
        // Init var
        $objAttribute = $objAttributeCollection->getObjAttribute($strKey);
        $result = (
            (
                (!is_null($objAttribute)) &&
                ($objAttribute instanceof SaveAttributeInterface)
            ) ?
                $objAttribute->getEntityAttrValueSaveFormatGet($value) :
                $value
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted value when set action required, to be loaded,
     * from specified attribute key,
     * from specified attribute collection.
     *
     * @param SaveAttributeCollectionInterface $objAttributeCollection
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public static function getAttributeValueSaveFormatSet(
        SaveAttributeCollectionInterface $objAttributeCollection,
        $strKey,
        $value
    )
    {
        // Init var
        $objAttribute = $objAttributeCollection->getObjAttribute($strKey);
        $result = (
            (
                (!is_null($objAttribute)) &&
                ($objAttribute instanceof SaveAttributeInterface)
            ) ?
                $objAttribute->getEntityAttrValueSaveFormatSet($value) :
                $value
        );

        // Return result
        return $result;
    }



}