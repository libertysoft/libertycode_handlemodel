<?php
/**
 * Description :
 * This class allows to define standard attribute provider class.
 * Standard attribute provider uses attribute objects,
 * to provide attribute information for entity.
 *
 * Standard attribute provider uses the following specified configuration:
 * [
 *     Default attribute provider configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\provider\standard\model;

use liberty_code\handle_model\attribute\provider\model\DefaultAttrProvider;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\handle_model\attribute\api\AttributeCollectionInterface;
use liberty_code\handle_model\attribute\repository\api\SaveAttributeCollectionInterface;
use liberty_code\handle_model\attribute\provider\standard\library\ConstStandardAttrProvider;
use liberty_code\handle_model\attribute\provider\standard\exception\AttributeCollectionInvalidFormatException;



/**
 * @method AttributeCollectionInterface getObjAttributeCollection() Get attribute collection object.
 * @method void setObjAttributeCollection(AttributeCollectionInterface $objAttributeCollection) Set attribute collection object.
 */
class StandardAttrProvider extends DefaultAttrProvider
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttributeCollectionInterface $objAttributeCollection
     */
    public function __construct(
        AttributeCollectionInterface $objAttributeCollection,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $objCacheRepo
        );

        // Init attribute collection
        $this->setObjAttributeCollection($objAttributeCollection);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstStandardAttrProvider::DATA_KEY_DEFAULT_ATTRIBUTE_COLLECTION))
        {
            $this->__beanTabData[ConstStandardAttrProvider::DATA_KEY_DEFAULT_ATTRIBUTE_COLLECTION] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstStandardAttrProvider::DATA_KEY_DEFAULT_ATTRIBUTE_COLLECTION
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstStandardAttrProvider::DATA_KEY_DEFAULT_ATTRIBUTE_COLLECTION:
                    AttributeCollectionInvalidFormatException::setCheck(
                        $value,
                        $this->getStrFixAttributeCollectionClassPath()
                    );
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed attribute collection class path.
     * Null means no specific class path fixed.
     * Overwrite it to implement specific class path.
     *
     * @return null|string
     */
    protected function getStrFixAttributeCollectionClassPath()
    {
        // Return result
        return null;
    }



    /**
     * @inheritdoc
     */
    protected function getTabEntityAttrConfigEngine()
    {
        // Init var
        $objAttributeCollection = $this->getObjAttributeCollection();
        $result = $objAttributeCollection->getTabEntityAttrConfig();

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabEntityAttrRuleConfigEngine()
    {
        // Init var
        $objAttributeCollection = $this->getObjAttributeCollection();
        $result = $objAttributeCollection->getTabEntityAttrRuleConfig();

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getEntityAttrValueFormatGetEngine($strKey, $value)
    {
        // Init var
        $objAttributeCollection = $this->getObjAttributeCollection();
        $result = $objAttributeCollection->getEntityAttrValueFormatGet($strKey, $value);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getEntityAttrValueFormatSetEngine($strKey, $value)
    {
        // Init var
        $objAttributeCollection = $this->getObjAttributeCollection();
        $result = $objAttributeCollection->getEntityAttrValueFormatSet($strKey, $value);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getEntityAttrValueSaveFormatGetEngine($strKey, $value)
    {
        // Init var
        $objAttributeCollection = $this->getObjAttributeCollection();
        $result = (
            ($objAttributeCollection instanceof SaveAttributeCollectionInterface) ?
                $objAttributeCollection->getEntityAttrValueSaveFormatGet($strKey, $value) :
                $value
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getEntityAttrValueSaveFormatSetEngine($strKey, $value)
    {
        // Init var
        $objAttributeCollection = $this->getObjAttributeCollection();
        $result = (
            ($objAttributeCollection instanceof SaveAttributeCollectionInterface) ?
                $objAttributeCollection->getEntityAttrValueSaveFormatSet($strKey, $value) :
                $value
        );

        // Return result
        return $result;
    }



}