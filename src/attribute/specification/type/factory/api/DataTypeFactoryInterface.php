<?php
/**
 * Description :
 * This class allows to describe behavior of data type factory class.
 * Data type factory allows to provide new or specified data type instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined data type types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\factory\api;

use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;



interface DataTypeFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of data type,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param null|string $strConfigKey = null
     * @return null|string
     */
    public function getStrDataTypeClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified data type object,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param null|string $strConfigKey = null
     * @param null|DataTypeInterface $objDataType = null
     * @return null|DataTypeInterface
     */
    public function getObjDataType(
        array $tabConfig,
        $strConfigKey = null,
        DataTypeInterface $objDataType = null
    );



}