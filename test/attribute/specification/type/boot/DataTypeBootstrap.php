<?php

// Use
use liberty_code\model\datetime\factory\library\ConstDateTimeFactory;
use liberty_code\model\datetime\factory\model\DefaultDateTimeFactory;



// Init var
$tabDateTimeFactoryConfig = array(
    ConstDateTimeFactory::TAB_CONFIG_KEY_REF_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_GET_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT => 'Y-m-d H:i:s',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SET_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT => 'Y-m-d H:i:s',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_DATETIME_FORMAT => '[Y-m-d H:i:s]'
);
$objDateTimeFactory = new DefaultDateTimeFactory($tabDateTimeFactoryConfig);


