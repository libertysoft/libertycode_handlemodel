<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\provider\library;



class ConstAttrProvider
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_CACHE_REPO = 'objCacheRepo';



    // Configuration
    const TAB_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_CONFIG_KEY = 'cache_entity_attribute_config_key';
    const TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_RULE_CONFIG_KEY = 'cache_entity_attribute_rule_config_key';
    const TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_FORMAT_GET_KEY_PATTERN = 'cache_entity_attribute_value_format_get_key_pattern';
    const TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_FORMAT_SET_KEY_PATTERN = 'cache_entity_attribute_value_format_set_key_pattern';
    const TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_SAVE_FORMAT_GET_KEY_PATTERN = 'cache_entity_attribute_value_save_format_get_key_pattern';
    const TAB_CONFIG_KEY_CACHE_ENTITY_ATTR_VALUE_SAVE_FORMAT_SET_KEY_PATTERN = 'cache_entity_attribute_value_save_format_set_key_pattern';
    const TAB_CONFIG_KEY_CACHE_SET_CONFIG = 'cache_set_config';

    // Cache configuration
    const CACHE_DEFAULT_ENTITY_ATTR_CONFIG_KEY = 'entity_attribute_config';
    const CACHE_DEFAULT_ENTITY_ATTR_RULE_CONFIG_KEY = 'entity_attribute_rule_config';
    const CACHE_DEFAULT_ENTITY_ATTR_VALUE_FORMAT_GET_KEY_PATTERN = 'entity_attribute_value_format_get_%1$s%2$s';
    const CACHE_DEFAULT_ENTITY_ATTR_VALUE_FORMAT_SET_KEY_PATTERN = 'entity_attribute_value_format_set_%1$s%2$s';
    const CACHE_DEFAULT_ENTITY_ATTR_VALUE_SAVE_FORMAT_GET_KEY_PATTERN = 'entity_attribute_value_save_format_get_%1$s%2$s';
    const CACHE_DEFAULT_ENTITY_ATTR_VALUE_SAVE_FORMAT_SET_KEY_PATTERN = 'entity_attribute_value_save_format_set_%1$s%2$s';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default attribute provider configuration standard.';
    const EXCEPT_MSG_CACHE_REPO_INVALID_FORMAT = 'Following cache repository "%1$s" invalid! It must be a cache repository object.';
    const EXCEPT_MSG_ATTR_KEY_INVALID_FORMAT = 'Following attribute key "%1$s" invalid! The key must be a string, not empty.';



}