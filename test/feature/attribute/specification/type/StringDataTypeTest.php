<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\test\feature\attribute\specification\type;

use liberty_code\handle_model\test\attribute\specification\type\model\StandardDataTypeTest;

use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_string\exception\ConfigInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\standard\type_string\model\StringDataType;



/**
 * @cover DataTypeInterface
 * @cover DefaultDataType
 * @cover StringDataType
 */
class StringDataTypeTest extends StandardDataTypeTest
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use string data type object.
     *
     * @param null|array $tabConfig
     * @param boolean $boolValueRequired
     * @param array $tabListValue
     * @param mixed $valueFormatGet
     * @param mixed $valueFormatSet
     * @param mixed $valueSaveFormatGet
     * @param mixed $valueSaveFormatSet
     * @param string|array $expectResult
     * @dataProvider providerUseStringDataType
     */
    public function testCanUseStringDataType(
        $tabConfig,
        $boolValueRequired,
        array $tabListValue,
        $valueFormatGet,
        $valueFormatSet,
        $valueSaveFormatGet,
        $valueSaveFormatSet,
        $expectResult
    )
    {
        // Init var
        $callableGetObjDataType = function() use ($tabConfig) {
            return new StringDataType($tabConfig);
        };

        // Test
        $this->testCanUseStandardDataType(
            $callableGetObjDataType,
            $boolValueRequired,
            $tabListValue,
            $valueFormatGet,
            $valueFormatSet,
            $valueSaveFormatGet,
            $valueSaveFormatSet,
            $expectResult
        );
    }



    /**
     * Data provider,
     * to test can use string data type object.
     *
     * @return array
     */
    public function providerUseStringDataType()
    {
        // Return result
        return array(
            'Use string data type: fail to create string data type (invalid REGEXP format)' => [
                [
                    'type' => 'string',
                    'regexp' => true
                ],
                false,
                [],
                'Value 1',
                1,
                'Value 1',
                'Value 1',
                ConfigInvalidFormatException::class
            ],
            'Use string data type: fail to create string data type (invalid minimum size format)' => [
                [
                    'type' => 'string',
                    'size_min' => 0
                ],
                false,
                [],
                'Value 1',
                1,
                'Value 1',
                'Value 1',
                ConfigInvalidFormatException::class
            ],
            'Use string data type: fail to create string data type (invalid maximum size format)' => [
                [
                    'type' => 'string',
                    'size_max' => 'test'
                ],
                false,
                [],
                'Value 1',
                1,
                'Value 1',
                'Value 1',
                ConfigInvalidFormatException::class
            ],
            'Use string data type: fail to create string data type (invalid sizes format)' => [
                [
                    'type' => 'string',
                    'size_min' => 80,
                    'size_max' => 30
                ],
                false,
                [],
                'Value 1',
                1,
                'Value 1',
                'Value 1',
                ConfigInvalidFormatException::class
            ],
            'Use string data type: fail to create string data type (invalid format list value option format)' => [
                [
                    'type' => 'string',
                    'format_list_value_require' => []
                ],
                false,
                [],
                'Value 1',
                1,
                'Value 1',
                'Value 1',
                ConfigInvalidFormatException::class
            ],
            'Use string data type: success to use string data type (simple; with rule options, required, list, formatting; without multiline)' => [
                [
                    'type' => 'string',
                    'regexp' => '#.*#',
                    'size_min' => 1,
                    'size_max' => 30
                ],
                true,
                ['Value 1', 2, 'Value 3', 4],
                'Value 1',
                1,
                'Value 1',
                'Value 1',
                [
                    [
                        'type_string',
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'group_sub_rule_or',
                                        [
                                            'rule_config' => [
                                                'is-multiline' => [
                                                    [
                                                        'string_contain',
                                                        ['contain_value' => PHP_EOL]
                                                    ],
                                                    [
                                                        'string_contain',
                                                        ['contain_value' => "\n"]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is multiline.'
                            ]
                        ],
                        [
                            'string_regexp',
                            ['regexp' => '#.*#']
                        ],
                        [
                            'sub_rule_size',
                            [
                                'rule_config' => [
                                    [
                                        'compare_greater',
                                        [
                                            'compare_value' => 1
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s did not reach minimum size (1)'
                            ]
                        ],
                        [
                            'sub_rule_size',
                            [
                                'rule_config' => [
                                    [
                                        'compare_less',
                                        [
                                            'compare_value' => 30
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s has exceeded maximum size (30)'
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    'type_string',
                                    'is_empty'
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ],
                        [
                            'compare_in',
                            [
                                'compare_value' => ['Value 1', '2', 'Value 3', '4']
                            ]
                        ]
                    ],
                    'Value 1',
                    '1',
                    'Value 1',
                    'Value 1',
                ]
            ],
            'Use string data type: success to use string data type (simple; with multiline, list; without rule options, required, formatting)' => [
                [
                    'type' => 'string',
                    'multiline_require' => true,
                    'format_list_value_require' => false,
                    'format_set_string_value_require' => false
                ],
                false,
                ['Value 1', 2, 'Value 3', 4],
                'Value 1',
                1,
                'Value 1',
                'Value 1',
                [
                    [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-empty' => [
                                        'type_string',
                                        'is_empty'
                                    ],
                                    'is-valid-string' =>  [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => [
                                                    'type_string',
                                                    'is_empty'
                                                ],
                                                'error_message_pattern' => '%1$s is empty.'
                                            ]
                                        ],
                                        [
                                            'compare_in',
                                            [
                                                'compare_value' => ['Value 1', 2, 'Value 3', 4]
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be empty or a valid string.'
                            ]
                        ]
                    ],
                    'Value 1',
                    1,
                    'Value 1',
                    'Value 1',
                ]
            ],
            'Use string data type: fail to get formatted values, from string data type (simple; with formatting; without multiline, rule options, required, list)' => [
                [
                    'type' => 'string'
                ],
                false,
                [],
                'Value 1',
                [],
                'Value 1',
                'Value 1',
                [
                    [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-empty' => [
                                        'type_string',
                                        'is_empty'
                                    ],
                                    'is-valid-string' =>  [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => [
                                                    [
                                                        'group_sub_rule_or',
                                                        [
                                                            'rule_config' => [
                                                                'is-multiline' => [
                                                                    [
                                                                        'string_contain',
                                                                        ['contain_value' => PHP_EOL]
                                                                    ],
                                                                    [
                                                                        'string_contain',
                                                                        ['contain_value' => "\n"]
                                                                    ]
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ],
                                                'error_message_pattern' => '%1$s is multiline.'
                                            ]
                                        ],
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => [
                                                    'type_string',
                                                    'is_empty'
                                                ],
                                                'error_message_pattern' => '%1$s is empty.'
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be empty or a valid string.'
                            ]
                        ]
                    ],
                    'Value 1',
                    [],
                    'Value 1',
                    'Value 1',
                ]
            ],
            'Use string data type: fail to create string data type (invalid multiple option format)' => [
                [
                    'type' => 'string',
                    'multiple_require' => 'test'
                ],
                false,
                [],
                ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                ['Value 1', 2, 'Value 3', 4],
                ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                ConfigInvalidFormatException::class
            ],
            'Use string data type: fail to create string data type (invalid multiple unique option format)' => [
                [
                    'type' => 'string',
                    'multiple_unique_require' => [],
                ],
                false,
                [],
                ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                ['Value 1', 2, 'Value 3', 4],
                ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                ConfigInvalidFormatException::class
            ],
            'Use string data type: success to use string data type (multiple; with required, formatting; without unique)' => [
                [
                    'type' => 'string',
                    'multiple_require' => true
                ],
                true,
                [],
                ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                ['Value 1', 2, 'Value 3', 4],
                ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                json_encode(['Value 1', 'Value 2', 'Value 3', 'Value 4']),
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    'type_string',
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'group_sub_rule_or',
                                                    [
                                                        'rule_config' => [
                                                            'is-multiline' => [
                                                                [
                                                                    'string_contain',
                                                                    ['contain_value' => PHP_EOL]
                                                                ],
                                                                [
                                                                    'string_contain',
                                                                    ['contain_value' => "\n"]
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is multiline.'
                                        ]
                                    ],
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                'type_string',
                                                'is_empty'
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid strings.'
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => ['is_empty'],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ]
                    ],
                    ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                    ['Value 1', '2', 'Value 3', '4'],
                    json_encode(['Value 1', 'Value 2', 'Value 3', 'Value 4']),
                    ['Value 1', 'Value 2', 'Value 3', 'Value 4']
                ]
            ],
            'Use string data type: success to use string data type (multiple; with unique; without required, formatting)' => [
                [
                    'type' => 'string',
                    'multiple_require' => true,
                    'multiple_unique_require' => true,
                    'format_set_string_value_require' => false,
                    'save_format_get_multiple_value_require' => false,
                    'save_format_set_multiple_value_require' => false
                ],
                false,
                [],
                ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                ['Value 1', 2, 'Value 3', 4],
                ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                json_encode(['Value 1', 'Value 2', 'Value 3', 'Value 4']),
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    'type_string',
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'group_sub_rule_or',
                                                    [
                                                        'rule_config' => [
                                                            'is-multiline' => [
                                                                [
                                                                    'string_contain',
                                                                    ['contain_value' => PHP_EOL]
                                                                ],
                                                                [
                                                                    'string_contain',
                                                                    ['contain_value' => "\n"]
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is multiline.'
                                        ]
                                    ],
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                'type_string',
                                                'is_empty'
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid strings.'
                            ]
                        ],
                        [
                            'callable',
                            [
                                'valid_callable' => function($strName, $value) {
                                    return (
                                        (!is_array($value)) ||
                                        (count($value) == count(array_unique($value)))
                                    );
                                },
                                'error_message_pattern' => '%1$s must contain unique values.'
                            ]
                        ]
                    ],
                    ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                    ['Value 1', 2, 'Value 3', 4],
                    ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
                    json_encode(['Value 1', 'Value 2', 'Value 3', 'Value 4'])
                ]
            ],
            'Use string data type: fail to get formatted values, from string data type (multiple; with formatting; without required, unique)' => [
                [
                    'type' => 'string',
                    'multiple_require' => true
                ],
                false,
                [],
                'test',
                'test',
                'test',
                'test',
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    'type_string',
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'group_sub_rule_or',
                                                    [
                                                        'rule_config' => [
                                                            'is-multiline' => [
                                                                [
                                                                    'string_contain',
                                                                    ['contain_value' => PHP_EOL]
                                                                ],
                                                                [
                                                                    'string_contain',
                                                                    ['contain_value' => "\n"]
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is multiline.'
                                        ]
                                    ],
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                'type_string',
                                                'is_empty'
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid strings.'
                            ]
                        ]
                    ],
                    'test',
                    'test',
                    'test',
                    'test'
                ]
            ]
        );
    }



}