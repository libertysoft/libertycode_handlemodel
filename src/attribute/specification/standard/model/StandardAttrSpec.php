<?php
/**
 * Description :
 * This class allows to define standard attribute specification class.
 * Standard attribute specification uses data type objects,
 * to manage entity attribute value.
 *
 * Standard attribute specification uses the following specified configuration:
 * [
 *     Default attribute specification configuration,
 *
 *     sort_data_type_asc_require(optional: got true, if not found):
 *         true / false,
 *         if true, sort data type ASC applied, else, sort data type DESC applied,
 *
 *     select_data_type_first_require(optional: got false, if not found):
 *         true / false,
 *         if true, first data type match taken, else, last data type match taken
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\standard\model;

use liberty_code\handle_model\attribute\specification\model\DefaultAttrSpec;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\handle_model\attribute\specification\type\library\ToolBoxDataType;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataTypeCollection;
use liberty_code\handle_model\attribute\specification\library\ConstAttrSpec;
use liberty_code\handle_model\attribute\specification\standard\library\ConstStandardAttrSpec;
use liberty_code\handle_model\attribute\specification\standard\exception\DataTypeCollectionInvalidFormatException;
use liberty_code\handle_model\attribute\specification\standard\exception\ConfigInvalidFormatException;



/**
 * @method DefaultDataTypeCollection getObjDataTypeCollection() Get data type collection object.
 * @method void setObjDataTypeCollection(DefaultDataTypeCollection $objDataTypeCollection) Set data type collection object.
 */
class StandardAttrSpec extends DefaultAttrSpec
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DefaultDataTypeCollection $objDataTypeCollection
     */
    public function __construct(
        DefaultDataTypeCollection $objDataTypeCollection,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $objCacheRepo
        );

        // Init data type collection
        $this->setObjDataTypeCollection($objDataTypeCollection);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstStandardAttrSpec::DATA_KEY_DEFAULT_DATA_TYPE_COLLECTION))
        {
            $this->__beanTabData[ConstStandardAttrSpec::DATA_KEY_DEFAULT_DATA_TYPE_COLLECTION] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstStandardAttrSpec::DATA_KEY_DEFAULT_DATA_TYPE_COLLECTION
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstStandardAttrSpec::DATA_KEY_DEFAULT_DATA_TYPE_COLLECTION:
                    DataTypeCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstAttrSpec::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check sort data type ASC required,
     * used to select data type.
     *
     * @return boolean
     */
    public function checkSortDataTypeAscRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstStandardAttrSpec::TAB_CONFIG_KEY_SORT_DATA_TYPE_ASC_REQUIRE])) ||
            (intval($tabConfig[ConstStandardAttrSpec::TAB_CONFIG_KEY_SORT_DATA_TYPE_ASC_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check selection data type first required,
     * used to select data type, on same order.
     *
     * @return boolean
     */
    public function checkSelectDataTypeFirstRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstStandardAttrSpec::TAB_CONFIG_KEY_SELECT_DATA_TYPE_FIRST_REQUIRE]) &&
            (intval($tabConfig[ConstStandardAttrSpec::TAB_CONFIG_KEY_SELECT_DATA_TYPE_FIRST_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabDataTypeEngine()
    {
        // Init var
        $objDataTypeCollection = $this->getObjDataTypeCollection();
        $boolSortAsc = $this->checkSortDataTypeAscRequired();
        $result = ToolBoxDataType::getTabType($objDataTypeCollection, $boolSortAsc);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabDataTypeRuleConfigEngine(
        $strDataType,
        $boolValueRequired = false,
        array $tabListValue = array()
    )
    {
        // Init var
        $objDataTypeCollection = $this->getObjDataTypeCollection();
        $boolSortAsc = $this->checkSortDataTypeAscRequired();
        $boolFirst = $this->checkSelectDataTypeFirstRequired();
        $objDataType = ToolBoxDataType::getObjDataTypeFromType(
            $objDataTypeCollection,
            $strDataType,
            $boolSortAsc,
            $boolFirst
        );
        $result = $objDataType->getTabRuleConfig($boolValueRequired, $tabListValue);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getDataTypeValueFormatGetEngine($strDataType, $value)
    {
        // Init var
        $objDataTypeCollection = $this->getObjDataTypeCollection();
        $boolSortAsc = $this->checkSortDataTypeAscRequired();
        $boolFirst = $this->checkSelectDataTypeFirstRequired();
        $objDataType = ToolBoxDataType::getObjDataTypeFromType(
            $objDataTypeCollection,
            $strDataType,
            $boolSortAsc,
            $boolFirst
        );
        $result = $objDataType->getValueFormatGet($value);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getDataTypeValueFormatSetEngine($strDataType, $value)
    {
        // Init var
        $objDataTypeCollection = $this->getObjDataTypeCollection();
        $boolSortAsc = $this->checkSortDataTypeAscRequired();
        $boolFirst = $this->checkSelectDataTypeFirstRequired();
        $objDataType = ToolBoxDataType::getObjDataTypeFromType(
            $objDataTypeCollection,
            $strDataType,
            $boolSortAsc,
            $boolFirst
        );
        $result = $objDataType->getValueFormatSet($value);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getDataTypeValueSaveFormatGetEngine($strDataType, $value)
    {
        // Init var
        $objDataTypeCollection = $this->getObjDataTypeCollection();
        $boolSortAsc = $this->checkSortDataTypeAscRequired();
        $boolFirst = $this->checkSelectDataTypeFirstRequired();
        $objDataType = ToolBoxDataType::getObjDataTypeFromType(
            $objDataTypeCollection,
            $strDataType,
            $boolSortAsc,
            $boolFirst
        );
        $result = $objDataType->getValueSaveFormatGet($value);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getDataTypeValueSaveFormatSetEngine($strDataType, $value)
    {
        // Init var
        $objDataTypeCollection = $this->getObjDataTypeCollection();
        $boolSortAsc = $this->checkSortDataTypeAscRequired();
        $boolFirst = $this->checkSelectDataTypeFirstRequired();
        $objDataType = ToolBoxDataType::getObjDataTypeFromType(
            $objDataTypeCollection,
            $strDataType,
            $boolSortAsc,
            $boolFirst
        );
        $result = $objDataType->getValueSaveFormatSet($value);

        // Return result
        return $result;
    }



}