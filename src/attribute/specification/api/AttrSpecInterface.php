<?php
/**
 * Description :
 * This class allows to describe behavior of attribute specification class.
 * Attribute specification allows to provide a scope of data types,
 * to manage entity attribute value (validation, formatting).
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\api;

use liberty_code\model\entity\model\ValidatorConfigEntity;



interface AttrSpecInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get index array of string data types.
     *
     * @return array
     */
    public function getTabDataType();



    /**
     * Get entity attribute rule configuration array,
     * from specified data type.
     *
     * List values array format:
     * Index array of mixed values.
     * Allows to include a set of potential values,
     * on rule configuration.
     * Empty array considered has no list value required.
     *
     * Return array format:
     * Attribute rule configuration array format,
     * from @see ValidatorConfigEntity::getTabRuleConfig() return array format.
     *
     * @param string $strDataType
     * @param boolean $boolValueRequired = false
     * @param array $tabListValue = array()
     * @return array
     */
    public function getTabDataTypeRuleConfig(
        $strDataType,
        $boolValueRequired = false,
        array $tabListValue = array()
    );



    /**
     * Get specified entity attribute formatted value,
     * when get action required,
     * from specified data type.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return mixed
     */
    public function getDataTypeValueFormatGet($strDataType, $value);



    /**
     * Get specified entity attribute formatted value,
     * when set action required,
     * from specified data type.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return mixed
     */
    public function getDataTypeValueFormatSet($strDataType, $value);



    /**
     * Get specified entity attribute formatted value,
     * when get action required, to be saved,
     * from specified data type.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return mixed
     */
    public function getDataTypeValueSaveFormatGet($strDataType, $value);



    /**
     * Get specified entity attribute formatted value,
     * when set action required, to be loaded,
     * from specified data type.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return mixed
     */
    public function getDataTypeValueSaveFormatSet($strDataType, $value);
}