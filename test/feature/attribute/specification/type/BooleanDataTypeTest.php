<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\test\feature\attribute\specification\type;

use liberty_code\handle_model\test\attribute\specification\type\model\StandardDataTypeTest;

use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_boolean\exception\ConfigInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\standard\type_boolean\model\BooleanDataType;



/**
 * @cover DataTypeInterface
 * @cover DefaultDataType
 * @cover BooleanDataType
 */
class BooleanDataTypeTest extends StandardDataTypeTest
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use boolean data type object.
     *
     * @param null|array $tabConfig
     * @param boolean $boolValueRequired
     * @param array $tabListValue
     * @param mixed $valueFormatGet
     * @param mixed $valueFormatSet
     * @param mixed $valueSaveFormatGet
     * @param mixed $valueSaveFormatSet
     * @param string|array $expectResult
     * @dataProvider providerUseBooleanDataType
     */
    public function testCanUseBooleanDataType(
        $tabConfig,
        $boolValueRequired,
        array $tabListValue,
        $valueFormatGet,
        $valueFormatSet,
        $valueSaveFormatGet,
        $valueSaveFormatSet,
        $expectResult
    )
    {
        // Init var
        $callableGetObjDataType = function() use ($tabConfig) {
            return new BooleanDataType($tabConfig);
        };

        // Test
        $this->testCanUseStandardDataType(
            $callableGetObjDataType,
            $boolValueRequired,
            $tabListValue,
            $valueFormatGet,
            $valueFormatSet,
            $valueSaveFormatGet,
            $valueSaveFormatSet,
            $expectResult
        );
    }



    /**
     * Data provider,
     * to test can use boolean data type object.
     *
     * @return array
     */
    public function providerUseBooleanDataType()
    {
        // Return result
        return array(
            'Use boolean data type: fail to create boolean data type (invalid format list value option format)' => [
                [
                    'type' => 'boolean',
                    'format_list_value_require' => []
                ],
                false,
                [],
                true,
                1,
                false,
                '0',
                ConfigInvalidFormatException::class
            ],
            'Use boolean data type: fail to create boolean data type (invalid format value option (for setting) format)' => [
                [
                    'type' => 'boolean',
                    'format_set_boolean_value_require' => 'test'
                ],
                false,
                [],
                true,
                1,
                false,
                '0',
                ConfigInvalidFormatException::class
            ],
            'Use boolean data type: fail to create boolean data type (invalid format value option (for loading) format)' => [
                [
                    'type' => 'boolean',
                    'save_format_set_boolean_value_require' => 'test'
                ],
                false,
                [],
                true,
                1,
                false,
                '0',
                ConfigInvalidFormatException::class
            ],
            'Use boolean data type: success to use boolean data type (simple; with required, list, formatting)' => [
                [
                    'type' => 'boolean'
                ],
                true,
                [true, 0],
                true,
                1,
                false,
                '0',
                [
                    [
                        [
                            'type_boolean',
                            [
                                'integer_enable_require' => false,
                                'string_enable_require' => false
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => null]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ],
                        [
                            'compare_in',
                            [
                                'compare_value' => [true, false]
                            ]
                        ]
                    ],
                    true,
                    true,
                    '0',
                    false
                ]
            ],
            'Use boolean data type: success to use boolean data type (simple; with custom empty value, formatting; without required, list)' => [
                [
                    'type' => 'boolean',
                    'empty_value' => 'null',
                    'format_get_empty_value' => null,
                    'format_set_empty_value' => null,
                    'save_format_get_empty_value' => '[null]',
                    'save_format_set_empty_value' => '[null]',
                ],
                false,
                [],
                'null',
                null,
                'null',
                '[null]',
                [
                    [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-empty' => [
                                        [
                                            'compare_equal',
                                            ['compare_value' => 'null']
                                        ]
                                    ],
                                    'is-valid-boolean' => [
                                        [
                                            'type_boolean',
                                            [
                                                'integer_enable_require' => false,
                                                'string_enable_require' => false
                                            ]
                                        ],
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => [
                                                    [
                                                        'compare_equal',
                                                        ['compare_value' => 'null']
                                                    ]
                                                ],
                                                'error_message_pattern' => '%1$s is empty.'
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be empty or a valid boolean.'
                            ]
                        ]
                    ],
                    null,
                    'null',
                    '[null]',
                    'null',
                ]
            ],
            'Use boolean data type: fail to get formatted values, from boolean data type (simple; with required list; without formatting)' => [
                [
                    'type' => 'boolean',
                    'format_list_value_require' => false,
                    'format_set_boolean_value_require' => false,
                    'save_format_get_boolean_value_require' => false,
                    'save_format_set_boolean_value_require' => false,
                ],
                true,
                [true, 0],
                'test',
                1,
                false,
                '0',
                [
                    [
                        [
                            'type_boolean',
                            [
                                'integer_enable_require' => false,
                                'string_enable_require' => false
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => null]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ],
                        [
                            'compare_in',
                            [
                                'compare_value' => [true, 0]
                            ]
                        ]
                    ],
                    'test',
                    1,
                    false,
                    '0'
                ]
            ],
            'Use boolean data type: fail to create boolean data type (invalid multiple option format)' => [
                [
                    'type' => 'boolean',
                    'multiple_require' => 'test'
                ],
                false,
                [],
                [true, false, true, false],
                ['1', 0, true, false],
                [true, false, true, false],
                json_encode(['1', '0', '1', '0']),
                ConfigInvalidFormatException::class
            ],
            'Use boolean data type: fail to create boolean data type (invalid multiple unique option format)' => [
                [
                    'type' => 'boolean',
                    'multiple_unique_require' => []
                ],
                false,
                [],
                [true, false, true, false],
                ['1', 0, true, false],
                [true, false, true, false],
                json_encode(['1', '0', '1', '0']),
                ConfigInvalidFormatException::class
            ],
            'Use boolean data type: fail to create boolean data type (invalid format multiple value option (for saving) format)' => [
                [
                    'type' => 'boolean',
                    'save_format_get_multiple_value_require' => 'test'
                ],
                false,
                [],
                [true, false, true, false],
                ['1', 0, true, false],
                [true, false, true, false],
                json_encode(['1', '0', '1', '0']),
                ConfigInvalidFormatException::class
            ],
            'Use boolean data type: success to use boolean data type (multiple; with required, formatting; without unique)' => [
                [
                    'type' => 'boolean',
                    'multiple_require' => true
                ],
                true,
                [],
                [true, false, true, false],
                ['1', 0, true, false],
                [true, false, true, false],
                json_encode(['1', '0', '1', '0']),
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    [
                                        'type_boolean',
                                        [
                                            'integer_enable_require' => false,
                                            'string_enable_require' => false
                                        ]
                                    ],
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'compare_equal',
                                                    ['compare_value' => null]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid booleans.'
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => ['is_empty'],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ]
                    ],
                    [true, false, true, false],
                    [true, false, true, false],
                    json_encode(['1', '0', '1', '0']),
                    [true, false, true, false]
                ]
            ],
            'Use boolean data type: success to use boolean data type (multiple; with unique, formatting; without required, multiple formatting)' => [
                [
                    'type' => 'boolean',
                    'multiple_require' => true,
                    'multiple_unique_require' => true,
                    'save_format_get_multiple_value_require' => false,
                    'save_format_set_multiple_value_require' => false
                ],
                false,
                [],
                [true, false, true, false],
                ['1', 0, true, false],
                [true, false, true, false],
                json_encode(['1', '0', '1', '0']),
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    [
                                        'type_boolean',
                                        [
                                            'integer_enable_require' => false,
                                            'string_enable_require' => false
                                        ]
                                    ],
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'compare_equal',
                                                    ['compare_value' => null]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid booleans.'
                            ]
                        ],
                        [
                            'callable',
                            [
                                'valid_callable' => function($strName, $value) {
                                    return (
                                        (!is_array($value)) ||
                                        (count($value) == count(array_unique($value)))
                                    );
                                },
                                'error_message_pattern' => '%1$s must contain unique values.'
                            ]
                        ]
                    ],
                    [true, false, true, false],
                    [true, false, true, false],
                    ['1', '0', '1', '0'],
                    json_encode(['1', '0', '1', '0'])
                ]
            ],
            'Use boolean data type: fail to get formatted values, from boolean data type (multiple; with multiple formatting; without required, unique, formatting)' => [
                [
                    'type' => 'boolean',
                    'multiple_require' => true,
                    'save_format_get_boolean_value_require' => false,
                    'save_format_set_boolean_value_require' => false
                ],
                false,
                [],
                'test',
                'test',
                [true, false, true, false],
                json_encode(['1', '0', '1', '0']),
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    [
                                        'type_boolean',
                                        [
                                            'integer_enable_require' => false,
                                            'string_enable_require' => false
                                        ]
                                    ],
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'compare_equal',
                                                    ['compare_value' => null]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid booleans.'
                            ]
                        ]
                    ],
                    'test',
                    'test',
                    json_encode([true, false, true, false]),
                    ['1', '0', '1', '0']
                ]
            ]
        );
    }



}