<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\test\feature;

use PHPUnit\Framework\TestCase;

use liberty_code\model\datetime\factory\model\DefaultDateTimeFactory;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_string\model\StringDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\model\NumericDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_boolean\model\BooleanDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\model\DateDataType;
use liberty_code\handle_model\attribute\specification\type\factory\exception\ConfigInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\factory\api\DataTypeFactoryInterface;
use liberty_code\handle_model\attribute\specification\type\factory\model\DefaultDataTypeFactory;
use liberty_code\handle_model\attribute\specification\type\factory\base\exception\ConfigInvalidFormatException as BaseConfigInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\factory\base\model\BaseDataTypeFactory;
use liberty_code\handle_model\attribute\specification\type\factory\standard\model\StandardDataTypeFactory;



/**
 * @cover DataTypeFactoryInterface
 * @cover DefaultDataTypeFactory
 * @cover BaseDataTypeFactory
 * @cover StandardDataTypeFactory
 */
class DataTypeFactoryTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can create data type object.
     *
     * @param null|string $strConfigKey
     * @param array $tabConfig
     * @param null|string $strInitialClassPath
     * @param null|array $tabInitialConfig
     * @param string|array $expectResult
     * @dataProvider providerCreateDataType
     */
    public function testCanCreateDataType(
        $strConfigKey,
        array $tabConfig,
        $strInitialClassPath,
        $tabInitialConfig,
        $expectResult
    )
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/../../../..';
        require($strRootAppPath . '/attribute/specification/type/factory/boot/DataTypeFactoryBootstrap.php');

        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get initial data type
        $objInitialDataType = null;
        if (!is_null($strInitialClassPath))
        {
            /** @var DefaultDateTimeFactory $objDateTimeFactory */
            /** @var DefaultDataType $objInitialDataType */
            $objInitialDataType = (
                ($strInitialClassPath == DateDataType::class) ?
                    new DateDataType($objDateTimeFactory, $tabInitialConfig) :
                    (
                        in_array(
                            $strInitialClassPath,
                            array(StringDataType::class, NumericDataType::class, BooleanDataType::class)
                    ) ?
                        new $strInitialClassPath($tabInitialConfig) :
                        null
                    )
            );
        }

        // Get data type
        /** @var StandardDataTypeFactory $objDataTypeFactory */
        /** @var DefaultDataType $objDataType */
        $objDataType = $objDataTypeFactory->getObjDataType($tabConfig, $strConfigKey, $objInitialDataType);
        $strClassPath = $objDataTypeFactory->getStrDataTypeClassPath($tabConfig, $strConfigKey);

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $strExpectClassPath = $expectResult[0];

            // Set assertions (check data type creation)
            if(is_null($objDataType))
            {
                $this->assertSame(null, $objDataType);
                $this->assertSame($strExpectClassPath, $strClassPath);
            }
            else
            {
                $this->assertInstanceOf($strClassPath, $objDataType);
                $this->assertSame($strExpectClassPath, get_class($objDataType));
            }

            if(!is_null($objDataType))
            {
                // Get info
                $tabExpectConfig = $expectResult[1];
                $strExpectKey = $expectResult[2];

                // Set assertions (check data type detail)
                $tabDataTypeConfig = $objDataType->getTabConfig();
                $strDataTypeKey = $objDataType->getStrKey();
                $this->assertEquals($tabExpectConfig, $tabDataTypeConfig);
                if(!is_null($strExpectKey))
                {
                    $this->assertSame($strExpectKey, $strDataTypeKey);
                }

                // Print
                /*
                echo('Get factory class path: '. PHP_EOL);var_dump($strClassPath);echo(PHP_EOL);
                echo('Get object class path: '. PHP_EOL);var_dump(get_class($objDataType));echo(PHP_EOL);
                echo('Get config: ' . PHP_EOL);var_dump($tabDataTypeConfig);echo(PHP_EOL);
                echo('Get key: ' . PHP_EOL);var_dump($strDataTypeKey);echo(PHP_EOL);
                //*/
            }
        }
    }



    /**
     * Data provider,
     * to test can create data type object.
     *
     * @return array
     */
    public function providerCreateDataType()
    {
        // Return result
        return array(
            'Create data type: fail to create string data type (invalid type format)' => [
                null,
                [
                    'type' => 7,
                    'config' => [
                        'key' => 'key_test',
                        'type' => 'str'
                    ]
                ],
                null,
                null,
                ConfigInvalidFormatException::class
            ],
            'Create data type: fail to create string data type (type not found)' => [
                null,
                [
                    'type' => 'test',
                    'config' => [
                        'key' => 'key_test',
                        'type' => 'str'
                    ]
                ],
                null,
                null,
                [
                    null
                ]
            ],
            'Create data type: fail to create string data type (invalid config format)' => [
                null,
                [
                    'type' => 'string',
                    'config' => 7
                ],
                null,
                null,
                BaseConfigInvalidFormatException::class
            ],
            'Create data type: success to create string data type (without type, config key, initial data type)' => [
                null,
                [
                    'config' => [
                        'key' => 'key_test',
                        'type' => 'str'
                    ]
                ],
                null,
                null,
                [
                    StringDataType::class,
                    [
                        'key' => 'key_test',
                        'type' => 'str'
                    ],
                    'key_test'
                ]
            ],
            'Create data type: success to create string data type (with type; without config key, initial data type)' => [
                null,
                [
                    'type' => 'string',
                    'config' => [
                        'key' => 'key_test',
                        'type' => 'str'
                    ]
                ],
                null,
                null,
                [
                    StringDataType::class,
                    [
                        'key' => 'key_test',
                        'type' => 'str'
                    ],
                    'key_test'
                ]
            ],
            'Create data type: fail to create numeric data type (invalid initial data type)' => [
                'key_test',
                [
                    'type' => 'numeric',
                    'config' => [
                        'type' => 'int'
                    ]
                ],
                StringDataType::class,
                [
                    'key' => 'init_key_test',
                    'type' => 'init_int'
                ],
                [
                    NumericDataType::class
                ]
            ],
            'Create data type: success to create numeric data type (with config key, initial data type)' => [
                'key_test',
                [
                    'type' => 'numeric',
                    'config' => [
                        'type' => 'int'
                    ]
                ],
                NumericDataType::class,
                [
                    'key' => 'init_key_test',
                    'type' => 'init_int'
                ],
                [
                    NumericDataType::class,
                    [
                        'key' => 'key_test',
                        'type' => 'int'
                    ],
                    'key_test'
                ]
            ],
            'Create data type: success to create boolean data type (without config key, initial data type)' => [
                'key_test_1',
                [
                    'type' => 'boolean',
                    'config' => [
                        'key' => 'key_test_2',
                        'type' => 'bool'
                    ]
                ],
                null,
                null,
                [
                    BooleanDataType::class,
                    [
                        'key' => 'key_test_2',
                        'type' => 'bool'
                    ],
                    'key_test_2'
                ]
            ],
            'Create data type: success to create date data type (without config key, initial data type)' => [
                null,
                [
                    'type' => 'date',
                    'config' => [
                        'type' => 'dt'
                    ]
                ],
                null,
                null,
                [
                    DateDataType::class,
                    [
                        'type' => 'dt'
                    ],
                    null
                ]
            ]
        );
    }



}