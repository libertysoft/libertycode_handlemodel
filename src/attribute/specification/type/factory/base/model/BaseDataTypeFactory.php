<?php
/**
 * Description :
 * This class allows to define base data type factory class.
 * Can be consider is base of all default data type factory type.
 *
 * Base data type factory uses the following specified configuration, to get and hydrate default data type:
 * [
 *     -> Configuration key(optional): "string data type key"
 *
 *     Default data type factory configuration,
 *
 *     config(optional): [
 *         ... specific @see DataTypeInterface configuration array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\factory\base\model;

use liberty_code\handle_model\attribute\specification\type\factory\model\DefaultDataTypeFactory;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;
use liberty_code\handle_model\attribute\specification\type\factory\base\library\ConstBaseDataTypeFactory;
use liberty_code\handle_model\attribute\specification\type\factory\base\exception\ConfigInvalidFormatException;



abstract class BaseDataTypeFactory extends DefaultDataTypeFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateDataType(DataTypeInterface $objDataType, array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $tabConfig = (
            array_key_exists(ConstBaseDataTypeFactory::TAB_CONFIG_KEY_CONFIG, $tabConfigFormat) ?
                $tabConfigFormat[ConstBaseDataTypeFactory::TAB_CONFIG_KEY_CONFIG] :
                null
        );

        // Hydrate data type
        if(!is_null($tabConfig) && ($objDataType instanceof DefaultDataType))
        {
            $objDataType->setTabConfig($tabConfig);
        }
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(
            (!is_null($strConfigKey)) &&
            (
                (!array_key_exists(ConstBaseDataTypeFactory::TAB_CONFIG_KEY_CONFIG, $result)) ||
                (
                    is_array($result[ConstBaseDataTypeFactory::TAB_CONFIG_KEY_CONFIG]) &&
                    (!array_key_exists(ConstDataType::TAB_CONFIG_KEY_KEY, $result[ConstBaseDataTypeFactory::TAB_CONFIG_KEY_CONFIG]))
                )
            )
        )
        {
            if(!array_key_exists(ConstBaseDataTypeFactory::TAB_CONFIG_KEY_CONFIG, $result))
            {
                $result[ConstBaseDataTypeFactory::TAB_CONFIG_KEY_CONFIG] = array();
            }

            // Add configured key as data type key
            $result[ConstBaseDataTypeFactory::TAB_CONFIG_KEY_CONFIG][ConstDataType::TAB_CONFIG_KEY_KEY] = $strConfigKey;
        }

        // Return result
        return $result;
    }



}