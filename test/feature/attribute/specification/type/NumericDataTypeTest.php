<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\test\feature\attribute\specification\type;

use liberty_code\handle_model\test\attribute\specification\type\model\StandardDataTypeTest;

use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\exception\ConfigInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\model\NumericDataType;



/**
 * @cover DataTypeInterface
 * @cover DefaultDataType
 * @cover NumericDataType
 */
class NumericDataTypeTest extends StandardDataTypeTest
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use numeric data type object.
     *
     * @param null|array $tabConfig
     * @param boolean $boolValueRequired
     * @param array $tabListValue
     * @param mixed $valueFormatGet
     * @param mixed $valueFormatSet
     * @param mixed $valueSaveFormatGet
     * @param mixed $valueSaveFormatSet
     * @param string|array $expectResult
     * @dataProvider providerUseNumericDataType
     */
    public function testCanUseNumericDataType(
        $tabConfig,
        $boolValueRequired,
        array $tabListValue,
        $valueFormatGet,
        $valueFormatSet,
        $valueSaveFormatGet,
        $valueSaveFormatSet,
        $expectResult
    )
    {
        // Init var
        $callableGetObjDataType = function() use ($tabConfig) {
            return new NumericDataType($tabConfig);
        };

        // Test
        $this->testCanUseStandardDataType(
            $callableGetObjDataType,
            $boolValueRequired,
            $tabListValue,
            $valueFormatGet,
            $valueFormatSet,
            $valueSaveFormatGet,
            $valueSaveFormatSet,
            $expectResult
        );
    }



    /**
     * Data provider,
     * to test can use numeric data type object.
     *
     * @return array
     */
    public function providerUseNumericDataType()
    {
        // Return result
        return array(
            'Use numeric data type: fail to create numeric data type (invalid integer option format)' => [
                [
                    'type' => 'numeric',
                    'integer_require' => 'test'
                ],
                false,
                [],
                1,
                '1',
                1,
                '1',
                ConfigInvalidFormatException::class
            ],
            'Use numeric data type: fail to create numeric data type (invalid greater value format)' => [
                [
                    'type' => 'numeric',
                    'greater_compare_value' => 'test'
                ],
                false,
                [],
                1,
                '1',
                1,
                '1',
                ConfigInvalidFormatException::class
            ],
            'Use numeric data type: fail to create numeric data type (invalid greater equal option format)' => [
                [
                    'type' => 'numeric',
                    'greater_equal_enable_require' => []
                ],
                false,
                [],
                1,
                '1',
                1,
                '1',
                ConfigInvalidFormatException::class
            ],
            'Use numeric data type: fail to create numeric data type (invalid format list value option format)' => [
                [
                    'type' => 'integer',
                    'format_list_value_require' => []
                ],
                false,
                [],
                1,
                '1',
                1,
                '1',
                ConfigInvalidFormatException::class
            ],
            'Use numeric data type: success to use numeric data type (simple; with rule options, required, list, formatting)' => [
                [
                    'type' => 'integer',
                    'integer_require' => true,
                    'greater_compare_value' => 0,
                    'less_compare_value' => 100
                ],
                true,
                ['1', 2, '3', 4],
                1,
                '1',
                1,
                '1',
                [
                    [
                        [
                            'type_numeric',
                            [
                                'string_enable_require' => false,
                                'integer_only_require' => true
                            ]
                        ],
                        [
                            'compare_greater',
                            [
                                'compare_value' => 0,
                                'equal_enable_require' => true
                            ]
                        ],
                        [
                            'compare_less',
                            [
                                'compare_value' => 100,
                                'equal_enable_require' => true
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => null]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ],
                        [
                            'compare_in',
                            [
                                'compare_value' => [1, 2, 3, 4]
                            ]
                        ]
                    ],
                    1,
                    1,
                    '1',
                    1,
                ]
            ],
            'Use numeric data type: success to use numeric data type (simple; with rule options, custom empty value, list, formatting; without required)' => [
                [
                    'type' => 'numeric',
                    'greater_compare_value' => 0,
                    'greater_equal_enable_require' => false,
                    'less_compare_value' => 100,
                    'less_equal_enable_require' => false,
                    'empty_value' => 7,
                    'format_list_value_require' => false,
                    'format_get_empty_value' => null,
                    'format_set_empty_value' => null,
                    'save_format_get_empty_value' => 'Value 7',
                    'save_format_set_empty_value' => 'Value 7',
                ],
                false,
                ['1', 2, '3', 4],
                7,
                null,
                7,
                'Value 7',
                [
                    [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-empty' => [
                                        [
                                            'compare_equal',
                                            ['compare_value' => 7]
                                        ]
                                    ],
                                    'is-valid-numeric' => [
                                        [
                                            'type_numeric',
                                            [
                                                'string_enable_require' => false,
                                                'integer_only_require' => false
                                            ]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ],
                                        [
                                            'compare_less',
                                            [
                                                'compare_value' => 100,
                                                'equal_enable_require' => false
                                            ]
                                        ],
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => [
                                                    [
                                                        'compare_equal',
                                                        ['compare_value' => 7]
                                                    ]
                                                ],
                                                'error_message_pattern' => '%1$s is empty.'
                                            ]
                                        ],
                                        [
                                            'compare_in',
                                            [
                                                'compare_value' => ['1', 2, '3', 4]
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be empty or a valid numeric.'
                            ]
                        ]
                    ],
                    null,
                    7,
                    'Value 7',
                    7,
                ]
            ],
            'Use numeric data type: success to use numeric data type (simple; with required, empty value formatting only; without list)' => [
                [
                    'type' => 'numeric',
                    'format_set_numeric_value_require' => false,
                    'save_format_get_numeric_value_require' => false,
                    'save_format_set_numeric_value_require' => false
                ],
                true,
                [],
                null,
                '',
                null,
                '',
                [
                    [
                        [
                            'type_numeric',
                            [
                                'string_enable_require' => false,
                                'integer_only_require' => false
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => null]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ]
                    ],
                    null,
                    null,
                    '',
                    null,
                ]
            ],
            'Use numeric data type: fail to get formatted values, from numeric data type (simple; with required; without list, formatting)' => [
                [
                    'type' => 'numeric',
                    'format_set_numeric_value_require' => false,
                    'save_format_get_numeric_value_require' => false,
                    'save_format_set_numeric_value_require' => false
                ],
                true,
                [],
                'test',
                '1',
                1,
                '1',
                [
                    [
                        [
                            'type_numeric',
                            [
                                'string_enable_require' => false,
                                'integer_only_require' => false
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => null]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ]
                    ],
                    'test',
                    '1',
                    1,
                    '1'
                ]
            ],
            'Use numeric data type: fail to create numeric data type (invalid multiple option format)' => [
                [
                    'type' => 'numeric',
                    'multiple_require' => 'test'
                ],
                false,
                [],
                [1, 2, 3, 4],
                ['1', 2, '3', 4],
                [1, 2, 3, 4],
                json_encode(['1', '2', '3', '4']),
                ConfigInvalidFormatException::class
            ],
            'Use numeric data type: fail to create numeric data type (invalid multiple unique option format)' => [
                [
                    'type' => 'numeric',
                    'multiple_unique_require' => []
                ],
                false,
                [],
                [1, 2, 3, 4],
                ['1', 2, '3', 4],
                [1, 2, 3, 4],
                json_encode(['1', '2', '3', '4']),
                ConfigInvalidFormatException::class
            ],
            'Use numeric data type: fail to create numeric data type (invalid format value option (for saving) format)' => [
                [
                    'type' => 'numeric',
                    'save_format_get_numeric_value_require' => 'test'
                ],
                false,
                [],
                [1, 2, 3, 4],
                ['1', 2, '3', 4],
                [1, 2, 3, 4],
                json_encode(['1', '2', '3', '4']),
                ConfigInvalidFormatException::class
            ],
            'Use numeric data type: success to use numeric data type (multiple; with required, formatting; without unique)' => [
                [
                    'type' => 'numeric',
                    'multiple_require' => true
                ],
                true,
                [],
                [1, 2, 3, 4],
                ['1', 2, '3', 4],
                [1, 2, 3, 4],
                json_encode(['1', '2', '3', '4']),
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    [
                                        'type_numeric',
                                        [
                                            'string_enable_require' => false,
                                            'integer_only_require' => false
                                        ]
                                    ],
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'compare_equal',
                                                    ['compare_value' => null]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid numerics.'
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => ['is_empty'],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ]
                    ],
                    [1, 2, 3, 4],
                    [1.0, 2.0, 3.0, 4.0],
                    json_encode(['1', '2', '3', '4']),
                    [1.0, 2.0, 3.0, 4.0]
                ]
            ],
            'Use numeric data type: success to use numeric data type (multiple; with unique, formatting; without required, multiple formatting)' => [
                [
                    'type' => 'numeric',
                    'multiple_require' => true,
                    'multiple_unique_require' => true,
                    'save_format_get_multiple_value_require' => false,
                    'save_format_set_multiple_value_require' => false
                ],
                false,
                [],
                [1, 2, 3, 4],
                ['1', 2, '3', 4],
                [1, 2, 3, 4],
                json_encode(['1', '2', '3', '4']),
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    [
                                        'type_numeric',
                                        [
                                            'string_enable_require' => false,
                                            'integer_only_require' => false
                                        ]
                                    ],
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'compare_equal',
                                                    ['compare_value' => null]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid numerics.'
                            ]
                        ],
                        [
                            'callable',
                            [
                                'valid_callable' => function($strName, $value) {
                                    return (
                                        (!is_array($value)) ||
                                        (count($value) == count(array_unique($value)))
                                    );
                                },
                                'error_message_pattern' => '%1$s must contain unique values.'
                            ]
                        ]
                    ],
                    [1, 2, 3, 4],
                    [1.0, 2.0, 3.0, 4.0],
                    ['1', '2', '3', '4'],
                    json_encode(['1', '2', '3', '4'])
                ]
            ],
            'Use numeric data type: fail to get formatted values, from numeric data type (multiple; with multiple formatting; without required, unique, formatting)' => [
                [
                    'type' => 'numeric',
                    'multiple_require' => true,
                    'save_format_get_numeric_value_require' => false,
                    'save_format_set_numeric_value_require' => false
                ],
                false,
                [],
                'test',
                'test',
                [1, 2, 3, 4],
                json_encode(['1', '2', '3', '4']),
                [
                    [
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => [
                                    [
                                        'type_numeric',
                                        [
                                            'string_enable_require' => false,
                                            'integer_only_require' => false
                                        ]
                                    ],
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => [
                                                [
                                                    'compare_equal',
                                                    ['compare_value' => null]
                                                ]
                                            ],
                                            'error_message_pattern' => '%1$s is empty.'
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be an array of valid numerics.'
                            ]
                        ]
                    ],
                    'test',
                    'test',
                    json_encode([1, 2, 3, 4]),
                    ['1', '2', '3', '4']
                ]
            ]
        );
    }



}