<?php
/**
 * Description :
 * This class allows to define default attribute factory class.
 * Can be consider is base of all attribute factory types.
 *
 * Default attribute factory uses the following specified configuration, to get and hydrate attribute:
 * [
 *     type(optional): "string constant to determine attribute type",
 *
 *     ... specific attribute configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\handle_model\attribute\factory\api\AttributeFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\handle_model\attribute\api\AttributeInterface;
use liberty_code\handle_model\attribute\factory\library\ConstAttributeFactory;
use liberty_code\handle_model\attribute\factory\exception\FactoryInvalidFormatException;
use liberty_code\handle_model\attribute\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|AttributeFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|AttributeFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultAttributeFactory extends DefaultFactory implements AttributeFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttributeFactoryInterface $objFactory = null
     */
    public function __construct(
        AttributeFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init attribute factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAttributeFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstAttributeFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified attribute.
     * Overwrite it to set specific call hydration.
     *
     * @param AttributeInterface $objAttribute
     * @param array $tabConfigFormat
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateAttribute(AttributeInterface $objAttribute, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstAttributeFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstAttributeFactory::TAB_CONFIG_KEY_TYPE]);
        }

        // Hydrate attribute
        $objAttribute->setAttributeConfig($tabConfigFormat);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAttributeFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAttributeFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified attribute object.
     *
     * @param AttributeInterface $objAttribute
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(AttributeInterface $objAttribute, array $tabConfigFormat)
    {
        // Init var
        $strAttributeClassPath = $this->getStrAttributeClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strAttributeClassPath)) &&
            ($strAttributeClassPath == get_class($objAttribute))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstAttributeFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstAttributeFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of attribute,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrAttributeClassPathFromType($strConfigType);



    /**
     * Get string class path of attribute engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrAttributeClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrAttributeClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrAttributeClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrAttributeClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrAttributeClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance attribute,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|AttributeInterface
     */
    protected function getObjAttributeNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrAttributeClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance attribute engine.
     *
     * @param array $tabConfigFormat
     * @param AttributeInterface $objAttribute = null
     * @return null|AttributeInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjAttributeEngine(
        array $tabConfigFormat,
        AttributeInterface $objAttribute = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objAttribute = (
            is_null($objAttribute) ?
                $this->getObjAttributeNew($strConfigType) :
                $objAttribute
        );

        // Get and hydrate attribute, if required
        if(
            (!is_null($objAttribute)) &&
            $this->checkConfigIsValid($objAttribute, $tabConfigFormat)
        )
        {
            $this->hydrateAttribute($objAttribute, $tabConfigFormat);
            $result = $objAttribute;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjAttribute(
        array $tabConfig,
        $strConfigKey = null,
        AttributeInterface $objAttribute = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjAttributeEngine($tabConfigFormat, $objAttribute);

        // Get attribute from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjAttribute($tabConfig, $strConfigKey, $objAttribute);
        }

        // Return result
        return $result;
    }



}