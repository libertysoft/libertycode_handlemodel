<?php
/**
 * This class allows to define string data type class.
 * String data type is data type,
 * which allows to manage entity attribute string value.
 * Can be consider is base of all string data type types.
 *
 * String data type uses the following specified configuration:
 * [
 *     Default data type configuration,
 *
 *     multiline_require(optional: got false, if not found): true / false,
 *
 *     regexp(optional): "string REGEXP pattern",
 *
 *     size_min(optional): integer size,
 *
 *     size_max(optional): integer size,
 *
 *     multiple_require(optional: got false, if not found):
 *         true / false
 *         If true, consider entity attribute value as index array of string values,
 *
 *     multiple_unique_require(optional: got false, if not found):
 *         true / false
 *         In case multiple required, if true, each value must be unique,
 *
 *     format_list_value_require(optional: got true, if not found): true / false,
 *
 *     format_set_string_value_require(optional: got true, if not found): true / false,
 *
 *     save_format_get_multiple_value_require(optional: got true, if not found): true / false,
 *
 *     save_format_set_multiple_value_require(optional: got true, if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\standard\type_string\model;

use liberty_code\handle_model\attribute\specification\type\model\DefaultDataType;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_string\library\ConstStringDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_string\exception\ConfigInvalidFormatException;



class StringDataType extends DefaultDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        //$result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDataType::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check multiline required.
     *
     * @return boolean
     */
    public function checkMultilineRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_MULTILINE_REQUIRE]) &&
            (intval($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_MULTILINE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check multiple required.
     * If true, entity attribute value is considered as index array of string values.
     *
     * @return boolean
     */
    public function checkMultipleRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) &&
            (intval($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check multiple unique required.
     * In case multiple required, if true, each value must be unique.
     *
     * @return boolean
     */
    public function checkMultipleUniqueRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) &&
            (intval($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting list values required.
     *
     * @return boolean
     */
    public function checkListValueFormatRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute string value required,
     * when set action required.
     *
     * @return boolean
     */
    public function checkStrValueFormatSetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_SET_STRING_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_SET_STRING_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute multiple value required,
     * when get action required, to be saved.
     *
     * @return boolean
     */
    public function checkMultiValueSaveFormatGetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting entity attribute multiple value required,
     * when set action required, to be loaded.
     *
     * @return boolean
     */
    public function checkMultiValueSaveFormatSetRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE])) ||
            (intval($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get entity attribute rule configuration array,
     * for string value.
     * Overwrite it to set specific validation.
     *
     * Return format:
     * @see getTabRuleConfig() return array format.
     *
     * @return array
     */
    protected function getTabValidStringRuleConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strRegexpPattern = (
            isset($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_REGEXP]) ?
                $tabConfig[ConstStringDataType::TAB_CONFIG_KEY_REGEXP] :
                null
        );
        $intSizeMin = (
            isset($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MIN]) ?
                $tabConfig[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MIN] :
                null
        );
        $intSizeMax = (
            isset($tabConfig[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MAX]) ?
                $tabConfig[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MAX] :
                null
        );

        // Build rule configuration
        $result = array(
            'type_string'
        );

        if(!$this->checkMultilineRequired())
        {
            $result[] = array(
                'sub_rule_not',
                [
                    'rule_config' => [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-multiline' => [
                                        [
                                            'string_contain',
                                            ['contain_value' => PHP_EOL]
                                        ],
                                        [
                                            'string_contain',
                                            ['contain_value' => "\n"]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s is multiline.'
                ]
            );
        }

        if(!is_null($strRegexpPattern))
        {
            $result[] = array(
                'string_regexp',
                ['regexp' => $strRegexpPattern]
            );
        }

        if(!is_null($intSizeMin))
        {
            $result[] = array(
                'sub_rule_size',
                [
                    'rule_config' => [
                        [
                            'compare_greater',
                            [
                                'compare_value' => $intSizeMin
                            ]
                        ]
                    ],
                    'error_message_pattern' => sprintf(
                        '%1$s did not reach minimum size (%2$d)',
                        '%1$s',
                        $intSizeMin
                    )
                ]
            );
        }

        if(!is_null($intSizeMax))
        {
            $result[] = array(
                'sub_rule_size',
                [
                    'rule_config' => [
                        [
                            'compare_less',
                            [
                                'compare_value' => $intSizeMax
                            ]
                        ]
                    ],
                    'error_message_pattern' => sprintf(
                        '%1$s has exceeded maximum size (%2$d)',
                        '%1$s',
                        $intSizeMax
                    )
                ]
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get entity attribute rule configuration array,
     * for empty value.
     * Overwrite it to set specific validation.
     *
     * Return format:
     * @see getTabRuleConfig() return array format.
     *
     * @return array
     */
    protected function getTabEmptyRuleConfig()
    {
        // Return result
        return array(
            'type_string',
            'is_empty'
        );
    }



    /**
     * Get specified index array of formatted list values.
     * Overwrite it to set specific formatting.
     *
     * @param array $tabListValue
     * @return array
     */
    protected function getTabListValueFormat(array $tabListValue)
    {
        // Return result
        return array_map(
            function($value) {
                return $this->getStrValueFormatSet($value);
            },
            array_values($tabListValue)
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabRuleConfig(
        $boolValueRequired = false,
        array $tabListValue = array()
    )
    {
        // Init var
        $boolValueRequired = (is_bool($boolValueRequired) ? $boolValueRequired : false);
        $tabListValue = (
            $this->checkListValueFormatRequired() ?
                $this->getTabListValueFormat($tabListValue) :
                array_values($tabListValue)
        );
        $tabEmptyRuleConfig = $this->getTabEmptyRuleConfig();
        $tabValidStringRuleConfig = $this->getTabValidStringRuleConfig();
        $tabValidStringRuleConfig[] = array(
            'sub_rule_not',
            [
                'rule_config' => $tabEmptyRuleConfig,
                'error_message_pattern' => '%1$s is empty.'
            ]
        );
        if(count($tabListValue) > 0) {
            $tabValidStringRuleConfig[] = array(
                'compare_in',
                [
                    'compare_value' => $tabListValue
                ]
            );
        }

        // Build rule configuration
        $result = (
            $this->checkMultipleRequired() ?
                // Check multiple value
                array_merge(
                    array(
                        [
                            'type_array',
                            [
                                'index_only_require' => true
                            ]
                        ],
                        [
                            'sub_rule_iterate',
                            [
                                'rule_config' => $tabValidStringRuleConfig,
                                'error_message_pattern' => '%1$s must be an array of valid strings.'
                            ]
                        ]
                    ),
                    (
                        $boolValueRequired ?
                            array(
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty'],
                                        'error_message_pattern' => '%1$s is empty.'
                                    ]
                                ]
                            ) :
                            array()
                    ),
                    (
                        $this->checkMultipleUniqueRequired() ?
                            array(
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function($strName, $value) {
                                            return (
                                                (!is_array($value)) ||
                                                (count($value) == count(array_unique($value)))
                                            );
                                        },
                                        'error_message_pattern' => '%1$s must contain unique values.'
                                    ]
                                ]
                            ) :
                            array()
                    )
                ) :
                // Check simple value
                (
                    $boolValueRequired ?
                        $tabValidStringRuleConfig :
                        array(
                            [
                                'group_sub_rule_or',
                                [
                                    'rule_config' => [
                                        'is-empty' => $tabEmptyRuleConfig,
                                        'is-valid-string' => $tabValidStringRuleConfig
                                    ],
                                    'error_message_pattern' => '%1$s must be empty or a valid string.'
                                ]
                            ]
                        )
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted string value,
     * when set action required.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getStrValueFormatSet($value)
    {
        // Return result
        return (
            ToolBoxString::checkConvertString($value) ?
                strval($value) :
                parent::getValueFormatSet($value)
        );
    }



    /**
     * @inheritdoc
     */
    public function getValueFormatSet($value)
    {
        // Init var
        $boolStrValueFormatSetRequired = $this->checkStrValueFormatSetRequired();
        $getValueFormatSet = function($value) use ($boolStrValueFormatSetRequired) {
            return (
                $boolStrValueFormatSetRequired ?
                    $this->getStrValueFormatSet($value) :
                    $value
            );
        };
        $result = (
            $this->checkMultipleRequired() ?
                // Format multiple value
                (
                    is_array($value) ?
                        array_map(
                            function($subValue) use ($getValueFormatSet) {
                                return $getValueFormatSet($subValue);
                            },
                            array_values($value)
                        ) :
                        parent::getValueFormatSet($value)
                ) :
                // Format simple value
                $getValueFormatSet($value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted multiple value,
     * when get action required, to be saved.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getMultiValueSaveFormatGet($value)
    {
        // Return result
        return (
            is_array($value) ?
                json_encode($value) :
                parent::getValueSaveFormatGet($value)
        );
    }



    /**
     * @inheritdoc
     */
    public function getValueSaveFormatGet($value)
    {
        // Return result
        return (
            (
                $this->checkMultipleRequired() &&
                $this->checkMultiValueSaveFormatGetRequired()
            ) ?
                $this->getMultiValueSaveFormatGet($value) :
                parent::getValueSaveFormatGet($value)
        );
    }



    /**
     * Get specified entity attribute formatted multiple value,
     * when set action required, to be loaded.
     * Overwrite it to set specific formatting.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function getMultiValueSaveFormatSet($value)
    {
        // Return result
        return (
            is_array($resultValue = json_decode($value, true)) ?
                $resultValue :
                parent::getValueSaveFormatSet($value)
        );
    }



    /**
     * @inheritdoc
     */
    public function getValueSaveFormatSet($value)
    {
        // Return result
        return (
            (
                $this->checkMultipleRequired() &&
                $this->checkMultiValueSaveFormatSetRequired()
            ) ?
                $this->getMultiValueSaveFormatSet($value) :
                parent::getValueSaveFormatSet($value)

        );
    }



}