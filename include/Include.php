<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/attribute/specification/type/library/ConstDataType.php');
include($strRootPath . '/src/attribute/specification/type/library/ToolBoxDataType.php');
include($strRootPath . '/src/attribute/specification/type/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/api/DataTypeInterface.php');
include($strRootPath . '/src/attribute/specification/type/api/DataTypeCollectionInterface.php');
include($strRootPath . '/src/attribute/specification/type/model/DefaultDataType.php');
include($strRootPath . '/src/attribute/specification/type/model/DefaultDataTypeCollection.php');

include($strRootPath . '/src/attribute/specification/type/standard/type_string/library/ConstStringDataType.php');
include($strRootPath . '/src/attribute/specification/type/standard/type_string/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/standard/type_string/model/StringDataType.php');

include($strRootPath . '/src/attribute/specification/type/standard/type_numeric/library/ConstNumericDataType.php');
include($strRootPath . '/src/attribute/specification/type/standard/type_numeric/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/standard/type_numeric/model/NumericDataType.php');

include($strRootPath . '/src/attribute/specification/type/standard/type_boolean/library/ConstBooleanDataType.php');
include($strRootPath . '/src/attribute/specification/type/standard/type_boolean/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/standard/type_boolean/model/BooleanDataType.php');

include($strRootPath . '/src/attribute/specification/type/standard/type_date/library/ConstDateDataType.php');
include($strRootPath . '/src/attribute/specification/type/standard/type_date/exception/DateTimeFactoryInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/standard/type_date/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/standard/type_date/model/DateDataType.php');

include($strRootPath . '/src/attribute/specification/type/factory/library/ConstDataTypeFactory.php');
include($strRootPath . '/src/attribute/specification/type/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/factory/api/DataTypeFactoryInterface.php');
include($strRootPath . '/src/attribute/specification/type/factory/model/DefaultDataTypeFactory.php');

include($strRootPath . '/src/attribute/specification/type/factory/base/library/ConstBaseDataTypeFactory.php');
include($strRootPath . '/src/attribute/specification/type/factory/base/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/factory/base/model/BaseDataTypeFactory.php');

include($strRootPath . '/src/attribute/specification/type/factory/standard/library/ConstStandardDataTypeFactory.php');
include($strRootPath . '/src/attribute/specification/type/factory/standard/model/StandardDataTypeFactory.php');

include($strRootPath . '/src/attribute/specification/type/build/library/ConstDataTypeBuilder.php');
include($strRootPath . '/src/attribute/specification/type/build/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/type/build/api/DataTypeBuilderInterface.php');
include($strRootPath . '/src/attribute/specification/type/build/model/DefaultDataTypeBuilder.php');

include($strRootPath . '/src/attribute/specification/library/ConstAttrSpec.php');
include($strRootPath . '/src/attribute/specification/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/exception/CacheRepoInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/exception/DataTypeInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/api/AttrSpecInterface.php');
include($strRootPath . '/src/attribute/specification/model/DefaultAttrSpec.php');

include($strRootPath . '/src/attribute/specification/standard/library/ConstStandardAttrSpec.php');
include($strRootPath . '/src/attribute/specification/standard/exception/DataTypeCollectionInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/standard/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/specification/standard/model/StandardAttrSpec.php');

include($strRootPath . '/src/attribute/library/ConstAttribute.php');
include($strRootPath . '/src/attribute/library/ToolBoxAttribute.php');
include($strRootPath . '/src/attribute/library/ToolBoxEntity.php');
include($strRootPath . '/src/attribute/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/attribute/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/attribute/api/AttributeInterface.php');
include($strRootPath . '/src/attribute/api/AttributeCollectionInterface.php');
include($strRootPath . '/src/attribute/model/DefaultAttribute.php');
include($strRootPath . '/src/attribute/model/DefaultAttributeCollection.php');

include($strRootPath . '/src/attribute/repository/library/ToolBoxEntity.php');
include($strRootPath . '/src/attribute/repository/api/SaveAttributeInterface.php');
include($strRootPath . '/src/attribute/repository/api/SaveAttributeCollectionInterface.php');
include($strRootPath . '/src/attribute/repository/model/DefaultSaveAttribute.php');
include($strRootPath . '/src/attribute/repository/model/DefaultSaveAttributeCollection.php');

include($strRootPath . '/src/attribute/factory/library/ConstAttributeFactory.php');
include($strRootPath . '/src/attribute/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/attribute/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/factory/api/AttributeFactoryInterface.php');
include($strRootPath . '/src/attribute/factory/model/DefaultAttributeFactory.php');

include($strRootPath . '/src/attribute/factory/standard/library/ConstStandardAttributeFactory.php');
include($strRootPath . '/src/attribute/factory/standard/model/StandardAttributeFactory.php');

include($strRootPath . '/src/attribute/build/library/ConstBuilder.php');
include($strRootPath . '/src/attribute/build/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/attribute/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/attribute/build/api/BuilderInterface.php');
include($strRootPath . '/src/attribute/build/model/DefaultBuilder.php');

include($strRootPath . '/src/attribute/provider/library/ConstAttrProvider.php');
include($strRootPath . '/src/attribute/provider/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/attribute/provider/exception/CacheRepoInvalidFormatException.php');
include($strRootPath . '/src/attribute/provider/exception/AttrKeyInvalidFormatException.php');
include($strRootPath . '/src/attribute/provider/api/AttrProviderInterface.php');
include($strRootPath . '/src/attribute/provider/model/DefaultAttrProvider.php');

include($strRootPath . '/src/attribute/provider/standard/library/ConstStandardAttrProvider.php');
include($strRootPath . '/src/attribute/provider/standard/exception/AttributeCollectionInvalidFormatException.php');
include($strRootPath . '/src/attribute/provider/standard/model/StandardAttrProvider.php');

include($strRootPath . '/src/entity/model/HandleValidatorConfigEntity.php');

include($strRootPath . '/src/entity/repository/model/HandleSaveConfigEntity.php');