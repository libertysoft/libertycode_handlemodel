<?php
/**
 * Description :
 * This class allows to define standard data type factory class.
 * Standard data type factory allows to provide and hydrate data type instance.
 *
 * Standard data type factory uses the following specified configuration, to get and hydrate data type:
 * [
 *     Base data type factory configuration,
 *
 *     type(optional): "string",
 *
 *     config(optional): [
 *         ... specific @see StringDataType configuration array format
 *     ]
 *
 *     OR
 *
 *     Base data type factory configuration,
 *
 *     type(optional): "numeric",
 *
 *     config(optional): [
 *         ... specific @see NumericDataType configuration array format
 *     ]
 *
 *     OR
 *
 *     Base data type factory configuration,
 *
 *     type(optional): "boolean",
 *
 *     config(optional): [
 *         ... specific @see BooleanDataType configuration array format
 *     ]
 *
 *     OR
 *
 *     Base data type factory configuration,
 *
 *     type(optional): "date",
 *
 *     config(optional): [
 *         ... specific @see DateDataType configuration array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\factory\standard\model;

use liberty_code\handle_model\attribute\specification\type\factory\base\model\BaseDataTypeFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\specification\type\standard\type_string\model\StringDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\model\NumericDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_boolean\model\BooleanDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\exception\DateTimeFactoryInvalidFormatException;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\model\DateDataType;
use liberty_code\handle_model\attribute\specification\type\factory\api\DataTypeFactoryInterface;
use liberty_code\handle_model\attribute\specification\type\factory\standard\library\ConstStandardDataTypeFactory;



/**
 * @method DateTimeFactoryInterface getObjDateTimeFactory() Get datetime factory object.
 * @method void setObjDateTimeFactory(DateTimeFactoryInterface $objDateTimeFactory) Set datetime factory object.
 */
class StandardDataTypeFactory extends BaseDataTypeFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        DataTypeFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objFactory,
            $objProvider
        );

        // Init datetime factory
        $this->setObjDateTimeFactory($objDateTimeFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstStandardDataTypeFactory::DATA_KEY_DEFAULT_DATETIME_FACTORY))
        {
            $this->__beanTabData[ConstStandardDataTypeFactory::DATA_KEY_DEFAULT_DATETIME_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstStandardDataTypeFactory::DATA_KEY_DEFAULT_DATETIME_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstStandardDataTypeFactory::DATA_KEY_DEFAULT_DATETIME_FACTORY:
                    DateTimeFactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrDataTypeClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of data type, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardDataTypeFactory::CONFIG_TYPE_STRING:
                $result = StringDataType::class;
                break;

            case ConstStandardDataTypeFactory::CONFIG_TYPE_NUMERIC:
                $result = NumericDataType::class;
                break;

            case ConstStandardDataTypeFactory::CONFIG_TYPE_BOOLEAN:
                $result = BooleanDataType::class;
                break;

            case ConstStandardDataTypeFactory::CONFIG_TYPE_DATE:
                $result = DateDataType::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjDataTypeNew($strConfigType)
    {
        // Init var
        $result = null;
        $objDateTimeFactory = $this->getObjDateTimeFactory();

        // Get data type, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardDataTypeFactory::CONFIG_TYPE_STRING:
                $result = new StringDataType();
                break;

            case ConstStandardDataTypeFactory::CONFIG_TYPE_NUMERIC:
                $result = new NumericDataType();
                break;

            case ConstStandardDataTypeFactory::CONFIG_TYPE_BOOLEAN:
                $result = new BooleanDataType();
                break;

            case ConstStandardDataTypeFactory::CONFIG_TYPE_DATE:
                $result = new DateDataType($objDateTimeFactory);
                break;
        }

        // Return result
        return $result;
    }



}