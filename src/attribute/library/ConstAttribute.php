<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\library;



class ConstAttribute
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_KEY = 'key';
    const TAB_CONFIG_KEY_ATTRIBUTE_CONFIG = 'attribute_config';
    const TAB_CONFIG_KEY_DATA_TYPE = 'data_type';
    const TAB_CONFIG_KEY_VALUE_REQUIRED = 'value_required';
    const TAB_CONFIG_KEY_LIST_VALUE = 'list_value';
    const TAB_CONFIG_KEY_RULE_CONFIG = 'rule_config';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default attribute configuration standard.';
    const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Key invalid! The key "%1$s" must be a valid string in collection.';
    const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Value invalid! The value "%1$s" must be an attribute object in collection.';
}