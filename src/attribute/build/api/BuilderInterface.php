<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified attribute collection instance, with attributes.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\build\api;

use liberty_code\handle_model\attribute\api\AttributeCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************


    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified attribute collection.
     *
     * @param AttributeCollectionInterface $objAttributeCollection
     * @param boolean $boolClear = true
     */
    public function hydrateAttributeCollection(AttributeCollectionInterface $objAttributeCollection, $boolClear = true);
}