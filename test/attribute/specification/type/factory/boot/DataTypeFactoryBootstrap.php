<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load
require($strRootAppPath . '/attribute/specification/type/boot/DataTypeBootstrap.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\handle_model\attribute\specification\type\factory\standard\model\StandardDataTypeFactory;



// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objProvider = new DefaultProvider($objDepCollection);

// Init var
$objDataTypeFactory = new StandardDataTypeFactory($objDateTimeFactory, null, $objProvider);


