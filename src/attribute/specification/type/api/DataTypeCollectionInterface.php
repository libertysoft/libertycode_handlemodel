<?php
/**
 * Description :
 * This class allows to describe behavior of data type collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\api;

use liberty_code\handle_model\attribute\specification\type\api\DataTypeInterface;



interface DataTypeCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods value
	// ******************************************************************************

	/**
     * Check if specified data type exists.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get index array of keys.
     *
     * Sort option can be provided:
     * - True: Asc order required.
     * - False: Desc order required.
     * - Null: no specific order required.
     *
     * @param null|boolean $sortAscRequired = null
	 * @return array
	 */
	public function getTabKey($sortAscRequired = null);



	/**
	 * Get data type,
     * from specified key.
	 * 
	 * @param string $strKey
	 * @return null|DataTypeInterface
	 */
	public function getObjDataType($strKey);





	// Methods setters
	// ******************************************************************************

	/**
	 * Set specified data type,
     * and return its key.
	 * 
	 * @param DataTypeInterface $objDataType
	 * @return string
     */
	public function setDataType(DataTypeInterface $objDataType);



    /**
     * Set specified data types (index array or collection),
     * and return its index array of keys.
     *
     * @param array|static $tabDataType
     * @return array
     */
    public function setTabDataType($tabDataType);



    /**
     * Remove specified data type,
     * and return its instance.
     *
     * @param string $strKey
     * @return DataTypeInterface
     */
    public function removeDataType($strKey);



    /**
     * Remove all data types.
     */
    public function removeDataTypeAll();
}