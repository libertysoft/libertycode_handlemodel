<?php
/**
 * Description :
 * This class allows to describe behavior of attribute collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\api;

use liberty_code\model\entity\model\ValidatorConfigEntity;
use liberty_code\handle_model\attribute\api\AttributeInterface;



interface AttributeCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods check
	// ******************************************************************************

	/**
	 * Check if attribute exists,
     * from specified attribute key.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkAttributeExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get index array of attribute keys.
	 *
	 * @return array
	 */
	public function getTabAttributeKey();



	/**
	 * Get attribute,
     * from specified attribute key.
	 * 
	 * @param string $strKey
	 * @return null|AttributeInterface
	 */
	public function getObjAttribute($strKey);
	




	// Methods setters
	// ******************************************************************************

	/**
	 * Set attribute and return its key.
	 * 
	 * @param AttributeInterface $objAttribute
	 * @return string
     */
	public function setAttribute(AttributeInterface $objAttribute);



    /**
     * Set list of attributes (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|static $tabAttribute
     * @return array
     */
    public function setTabAttribute($tabAttribute);



    /**
     * Remove attribute and return its instance.
     *
     * @param string $strKey
     * @return AttributeInterface
     */
    public function removeAttribute($strKey);



    /**
     * Remove all attributes.
     */
    public function removeAttributeAll();





    // Methods entity attribute
    // ******************************************************************************

    /**
     * Get index array of entity attribute configuration,
     * from all attributes.
     *
     * Return array format:
     * @see ValidatorConfigEntity::getTabConfig() return array format.
     *
     * @return array
     */
    public function getTabEntityAttrConfig();



    /**
     * Get entity attribute rule configurations array,
     * from all attributes.
     *
     * Return array format:
     * @see ValidatorConfigEntity::getTabRuleConfig() return array format.
     *
     * @return array
     */
    public function getTabEntityAttrRuleConfig();



    /**
     * Get specified entity attribute formatted value when get action required,
     * from specified attribute key.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueFormatGet($strKey, $value);



    /**
     * Get specified entity attribute formatted value when set action required,
     * from specified attribute key.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getEntityAttrValueFormatSet($strKey, $value);
}