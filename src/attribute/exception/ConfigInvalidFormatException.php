<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\exception;

use liberty_code\validation\validator\exception\RuleConfigInvalidFormatException;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\exception\ConfigInvalidFormatException as EntityConfigInvalidFormatException;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\handle_model\attribute\library\ConstAttribute;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstAttribute::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @param AttrSpecInterface $objAttrSpec = null
     * @return boolean
     */
    protected static function checkConfigIsValid($config, AttrSpecInterface $objAttrSpec = null)
    {
        // Init list values index array check function
        $checkTabListValueIsValid = function($tabValue)
        {
            $result = (
                is_array($tabValue) &&
                (count($tabValue) > 0)
            );

            // Check each value is valid, if required
            if($result)
            {
                $tabValue = array_values($tabValue);
                for($intCpt = 0; ($intCpt < count($tabValue)) && $result; $intCpt++)
                {
                    $value = $tabValue[$intCpt];
                    $result = (
                        // Check valid value
                        (
                            is_string($value) ||
                            is_numeric($value) ||
                            is_bool($value) ||
                            is_null($value)
                        )
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid key
            isset($config[ConstAttribute::TAB_CONFIG_KEY_KEY]) &&
            is_string($config[ConstAttribute::TAB_CONFIG_KEY_KEY]) &&
            (trim($config[ConstAttribute::TAB_CONFIG_KEY_KEY]) != '') &&

            // Check valid attribute configuration
            (
                (!isset($config[ConstAttribute::TAB_CONFIG_KEY_ATTRIBUTE_CONFIG])) ||
                (
                    is_array($config[ConstAttribute::TAB_CONFIG_KEY_ATTRIBUTE_CONFIG]) &&
                    EntityConfigInvalidFormatException::checkAttributeConfigIsValid(
                        array_merge(
                            $config[ConstAttribute::TAB_CONFIG_KEY_ATTRIBUTE_CONFIG],
                            array(ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => $config[ConstAttribute::TAB_CONFIG_KEY_KEY])
                        )
                    )
                )
            ) &&

            // Check valid data type
            isset($config[ConstAttribute::TAB_CONFIG_KEY_DATA_TYPE]) &&
            is_string($config[ConstAttribute::TAB_CONFIG_KEY_DATA_TYPE]) &&
            (trim($config[ConstAttribute::TAB_CONFIG_KEY_DATA_TYPE]) != '') &&
            (
                is_null($objAttrSpec) ||
                in_array(
                    $config[ConstAttribute::TAB_CONFIG_KEY_DATA_TYPE],
                    $objAttrSpec->getTabDataType()
                )
            ) &&

            // Check valid value required option
            (
                (!array_key_exists(ConstAttribute::TAB_CONFIG_KEY_VALUE_REQUIRED, $config)) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstAttribute::TAB_CONFIG_KEY_VALUE_REQUIRED]) ||
                    is_int($config[ConstAttribute::TAB_CONFIG_KEY_VALUE_REQUIRED]) ||
                    (
                        is_string($config[ConstAttribute::TAB_CONFIG_KEY_VALUE_REQUIRED]) &&
                        ctype_digit($config[ConstAttribute::TAB_CONFIG_KEY_VALUE_REQUIRED])
                    )
                )
            ) &&

            // Check valid list value
            (
                (!isset($config[ConstAttribute::TAB_CONFIG_KEY_LIST_VALUE])) ||
                $checkTabListValueIsValid($config[ConstAttribute::TAB_CONFIG_KEY_LIST_VALUE])
            ) &&

            // Check valid rule configuration
            (
                (!isset($config[ConstAttribute::TAB_CONFIG_KEY_RULE_CONFIG])) ||
                is_array($config[ConstAttribute::TAB_CONFIG_KEY_RULE_CONFIG]) &&
                RuleConfigInvalidFormatException::checkConfigIsValid(
                    $config[ConstAttribute::TAB_CONFIG_KEY_RULE_CONFIG]
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
     * @param AttrSpecInterface $objAttrSpec = null
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config, AttrSpecInterface $objAttrSpec = null)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config, $objAttrSpec);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}