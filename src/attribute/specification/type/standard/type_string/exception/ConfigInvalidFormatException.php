<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\standard\type_string\exception;

use Exception;

use liberty_code\handle_model\attribute\specification\type\standard\type_string\library\ConstStringDataType;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStringDataType::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid multiline required option
            (
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_MULTILINE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstStringDataType::TAB_CONFIG_KEY_MULTILINE_REQUIRE]) ||
                    is_int($config[ConstStringDataType::TAB_CONFIG_KEY_MULTILINE_REQUIRE]) ||
                    (
                        is_string($config[ConstStringDataType::TAB_CONFIG_KEY_MULTILINE_REQUIRE]) &&
                        ctype_digit($config[ConstStringDataType::TAB_CONFIG_KEY_MULTILINE_REQUIRE])
                    )
                )
            ) &&

            // Check valid REGEXP pattern
            (
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_REGEXP])) ||
                (
                    is_string($config[ConstStringDataType::TAB_CONFIG_KEY_REGEXP]) &&
                    (trim($config[ConstStringDataType::TAB_CONFIG_KEY_REGEXP]) != '')
                )
            ) &&

            // Check valid minimum size
            (
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MIN])) ||
                (
                    is_int($config[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MIN]) &&
                    ($config[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MIN] > 0)
                )
            ) &&

            // Check valid maximum size
            (
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MAX])) ||
                (
                    is_int($config[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MAX]) &&
                    ($config[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MAX] > 0)
                )
            ) &&

            // Check valid size option
            (
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MIN])) ||
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MAX])) ||
                (
                    $config[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MAX] >=
                    $config[ConstStringDataType::TAB_CONFIG_KEY_SIZE_MIN]
                )
            ) &&

            // Check valid multiple required option
            (
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) ||
                    is_int($config[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) ||
                    (
                        is_string($config[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE]) &&
                        ctype_digit($config[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE])
                    )
                )
            ) &&

            // Check valid multiple unique required option
            (
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) ||
                    is_int($config[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) ||
                    (
                        is_string($config[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE]) &&
                        ctype_digit($config[ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE])
                    )
                )
            ) &&

            // Check valid formatting list value required option
            (
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE]) ||
                    is_int($config[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE]) ||
                    (
                        is_string($config[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE]) &&
                        ctype_digit($config[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_LIST_VALUE_REQUIRE])
                    )
                )
            ) &&

            // Check valid formatting entity attribute string value required option, when set action required
            (
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_SET_STRING_VALUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_SET_STRING_VALUE_REQUIRE]) ||
                    is_int($config[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_SET_STRING_VALUE_REQUIRE]) ||
                    (
                        is_string($config[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_SET_STRING_VALUE_REQUIRE]) &&
                        ctype_digit($config[ConstStringDataType::TAB_CONFIG_KEY_FORMAT_SET_STRING_VALUE_REQUIRE])
                    )
                )
            ) &&

            // Check valid formatting entity attribute multiple value required option, when get action required, to be saved
            (
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE]) ||
                    is_int($config[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE]) ||
                    (
                        is_string($config[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE]) &&
                        ctype_digit($config[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE])
                    )
                )
            ) &&

            // Check valid formatting entity attribute multiple value required option, when set action required, to be loaded
            (
                (!isset($config[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE]) ||
                    is_int($config[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE]) ||
                    (
                        is_string($config[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE]) &&
                        ctype_digit($config[ConstStringDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}