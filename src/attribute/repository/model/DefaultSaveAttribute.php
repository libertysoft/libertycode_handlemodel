<?php
/**
 * This class allows to define default save attribute class.
 * Can be consider is base of all save attribute types.
 *
 * Default save attribute uses the following specified configuration:
 * [
 *     Default attribute configuration,
 *
 *     attribute_config(optional: got [], if not found): [
 *         Entity attribute configuration array format: @see SaveConfigEntity for one attribute
 *         (attribute key not required)
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\repository\model;

use liberty_code\handle_model\attribute\model\DefaultAttribute;
use liberty_code\handle_model\attribute\repository\api\SaveAttributeInterface;



class DefaultSaveAttribute extends DefaultAttribute implements SaveAttributeInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatGet($value)
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $strDataType = $this->getStrAttributeDataType();
        $result = (
            (!is_null($objAttrSpec)) ?
                $objAttrSpec->getDataTypeValueSaveFormatGet($strDataType, $value) :
                $value
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatSet($value)
    {
        // Init var
        $objAttrSpec = $this->getObjAttrSpec();
        $strDataType = $this->getStrAttributeDataType();
        $result = (
            (!is_null($objAttrSpec)) ?
                $objAttrSpec->getDataTypeValueSaveFormatSet($strDataType, $value) :
                $value
        );

        // Return result
        return $result;
    }



}