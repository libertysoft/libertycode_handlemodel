<?php
/**
 * Description :
 * This class allows to describe behavior of attribute factory class.
 * Attribute factory allows to provide new or specified attribute instances,
 * hydrated with a specified configuration,
 * from a set of potential predefined attribute types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\factory\api;

use liberty_code\handle_model\attribute\api\AttributeInterface;



interface AttributeFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of attribute,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrAttributeClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified object instance attribute,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param AttributeInterface $objAttribute = null
     * @return null|AttributeInterface
     */
    public function getObjAttribute(
        array $tabConfig,
        $strConfigKey = null,
        AttributeInterface $objAttribute = null
    );
}