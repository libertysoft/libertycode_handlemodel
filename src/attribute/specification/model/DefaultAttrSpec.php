<?php
/**
 * Description :
 * This class allows to define default attribute specification class.
 * Can be consider is base of all attribute specification types.
 *
 * Default attribute specification uses the following specified configuration:
 * [
 *     cache_require(optional: got true, if not found): true / false,
 *
 *     cache_list_data_type_key(optional: got @see ConstValidator::CACHE_DEFAULT_LIST_DATA_TYPE_KEY, if not found):
 *         "string key,
 *         used on cache repository,
 *         to store array of data types",
 *
 *     cache_rule_config_key_pattern(optional: got @see ConstValidator::CACHE_DEFAULT_RULE_CONFIG_KEY_PATTERN, if not found):
 *         "string sprintf pattern,
 *         to build key used on cache repository,
 *         to store entity attribute rule configuration,
 *         where '%1$s', '%2$s' and '%3$s',
 *         replaced by specified data type, specified value required option hash and specified list values hash",
 *
 *     cache_value_format_get_key_pattern(optional: got @see ConstValidator::CACHE_DEFAULT_VALUE_FORMAT_GET_KEY_PATTERN, if not found):
 *         "string sprintf pattern,
 *         to build key used on cache repository,
 *         to store entity attribute formatted value, when get action required,
 *         where '%1$s' and '%2$s',
 *         replaced by specified data type, and specified value hash",
 *
 *     cache_value_format_set_key_pattern(optional: got @see ConstValidator::CACHE_DEFAULT_VALUE_FORMAT_SET_KEY_PATTERN, if not found):
 *         "string sprintf pattern,
 *         to build key used on cache repository,
 *         to store entity attribute formatted value, when set action required,
 *         where '%1$s' and '%2$s',
 *         replaced by specified data type, and specified value hash",
 *
 *     cache_value_save_format_get_key_pattern(optional: got @see ConstValidator::CACHE_DEFAULT_VALUE_SAVE_FORMAT_GET_KEY_PATTERN, if not found):
 *         "string sprintf pattern,
 *         to build key used on cache repository,
 *         to store entity attribute formatted value, when get action required, to be saved,
 *         where '%1$s' and '%2$s',
 *         replaced by specified data type, and specified value hash",
 *
 *     cache_value_save_format_set_key_pattern(optional: got @see ConstValidator::CACHE_DEFAULT_VALUE_SAVE_FORMAT_SET_KEY_PATTERN, if not found):
 *         "string sprintf pattern,
 *         to build key used on cache repository,
 *         to store entity attribute formatted value, when set action required, to be loaded,
 *         where '%1$s' and '%2$s',
 *         replaced by specified data type, and specified value hash",
 *
 *     cache_set_config(optional): [
 *         @see RepositoryInterface::setTabItem() configuration array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;

use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\cache\repository\library\ToolBoxRepository;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\handle_model\attribute\specification\library\ConstAttrSpec;
use liberty_code\handle_model\attribute\specification\exception\ConfigInvalidFormatException;
use liberty_code\handle_model\attribute\specification\exception\CacheRepoInvalidFormatException;
use liberty_code\handle_model\attribute\specification\exception\DataTypeInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method null|RepositoryInterface getObjCacheRepo() Get cache repository object.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 * @method void setObjCacheRepo(null|RepositoryInterface $objCacheRepo) Set cache repository object.
 */
abstract class DefaultAttrSpec extends FixBean implements AttrSpecInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param RepositoryInterface $objCacheRepo = null
     */
    public function __construct(
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init cache repository, if required
        if(!is_null($objCacheRepo))
        {
            $this->setObjCacheRepo($objCacheRepo);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAttrSpec::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstAttrSpec::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        if(!$this->beanExists(ConstAttrSpec::DATA_KEY_DEFAULT_CACHE_REPO))
        {
            $this->__beanTabData[ConstAttrSpec::DATA_KEY_DEFAULT_CACHE_REPO] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAttrSpec::DATA_KEY_DEFAULT_CONFIG,
            ConstAttrSpec::DATA_KEY_DEFAULT_CACHE_REPO
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAttrSpec::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstAttrSpec::DATA_KEY_DEFAULT_CACHE_REPO:
                    CacheRepoInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check cache required.
     *
     * @return boolean
     */
    public function checkCacheRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjCacheRepo())) &&
            (
                (!isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (intval($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }



    /**
     * Set check specified data type is valid.
     *
     * @param string $strDataType
     * @throws DataTypeInvalidFormatException
     */
    protected function setCheckDataTypeIsValid($strDataType)
    {
        // Check data type format
        DataTypeInvalidFormatException::setCheck($strDataType);

        // Check data type found
        $tabDataType = $this->getTabDataType();
        if(!in_array($strDataType, $tabDataType))
        {
            throw new DataTypeInvalidFormatException($strDataType);
        }
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified cache key,
     * to store data types.
     *
     * @return string
     */
    protected function getStrCacheDataTypeKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_DATA_TYPE_KEY]) ?
                $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_DATA_TYPE_KEY] :
                ConstAttrSpec::CACHE_DEFAULT_DATA_TYPE_KEY
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of string data types engine.
     *
     * @return array
     */
    abstract protected function getTabDataTypeEngine();



    /**
     * @inheritdoc
     */
    public function getTabDataType()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            $this->checkCacheRequired() ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $this->getStrCacheDataTypeKey(),
                    function() {return $this->getTabDataTypeEngine();},
                    (
                        isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getTabDataTypeEngine()
        );

        // Return result
        return $result;
    }



    /**
     * Get specified cache key,
     * to store entity attribute rule configuration,
     * from specified data type.
     *
     * List values array format:
     * @see getTabDataTypeRuleConfig() list values array format.
     *
     * @param string $strDataType
     * @param boolean $boolValueRequired
     * @param array $tabListValue
     * @return null|string
     */
    protected function getStrCacheRuleConfigKey(
        $strDataType,
        $boolValueRequired,
        array $tabListValue
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_RULE_CONFIG_KEY_PATTERN]) ?
                $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_RULE_CONFIG_KEY_PATTERN] :
                ConstAttrSpec::CACHE_DEFAULT_RULE_CONFIG_KEY_PATTERN
        );
        $result = (
            (
                is_string($strDataType) &&
                is_string($strValueRequiredHash = ToolBoxHash::getStrHash($boolValueRequired)) &&
                is_string($strListValueHash =  ToolBoxHash::getStrHash($tabListValue))
            ) ?
                sprintf(
                    $strPattern,
                    $strDataType,
                    $strValueRequiredHash,
                    $strListValueHash
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get entity attribute rule configuration array engine,
     * from specified validated data type.
     *
     * List values array format:
     * @see getTabDataTypeRuleConfig() List values array format.
     *
     * Return array format:
     * @see getTabDataTypeRuleConfig() return array format.
     *
     * @param string $strDataType
     * @param boolean $boolValueRequired = false
     * @param array $tabListValue = array()
     * @return array
     */
    abstract protected function getTabDataTypeRuleConfigEngine(
        $strDataType,
        $boolValueRequired = false,
        array $tabListValue = array()
    );



    /**
     * @inheritdoc
     * @throws DataTypeInvalidFormatException
     */
    public function getTabDataTypeRuleConfig(
        $strDataType,
        $boolValueRequired = false,
        array $tabListValue = array()
    )
    {
        // Check arguments
        $this->setCheckDataTypeIsValid($strDataType);

        // Init var
        $tabConfig = $this->getTabConfig();
        $strCacheKey = $this->getStrCacheRuleConfigKey($strDataType, $boolValueRequired, $tabListValue);
        $result = (
            ((!is_null($strCacheKey)) && $this->checkCacheRequired()) ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $strCacheKey,
                    function() use ($strDataType, $boolValueRequired, $tabListValue)
                    {
                        return $this->getTabDataTypeRuleConfigEngine(
                            $strDataType,
                            $boolValueRequired,
                            $tabListValue
                        );
                    },
                    (
                        isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getTabDataTypeRuleConfigEngine(
                    $strDataType,
                    $boolValueRequired,
                    $tabListValue
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get specified cache key,
     * to store entity attribute formatted value, when get action required,
     * from specified data type,
     * and specified value.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return null|string
     */
    protected function getStrCacheValueFormatGetKey($strDataType, $value)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_GET_KEY_PATTERN]) ?
                $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_GET_KEY_PATTERN] :
                ConstAttrSpec::CACHE_DEFAULT_VALUE_FORMAT_GET_KEY_PATTERN
        );
        $result = (
            (
                is_string($strDataType) &&
                is_string($strValueHash = ToolBoxHash::getStrHash($value))
            ) ?
                sprintf(
                    $strPattern,
                    $strDataType,
                    $strValueHash
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted value engine,
     * when get action required,
     * from specified validated data type.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return mixed
     */
    abstract protected function getDataTypeValueFormatGetEngine($strDataType, $value);



    /**
     * @inheritdoc
     * @throws DataTypeInvalidFormatException
     */
    public function getDataTypeValueFormatGet($strDataType, $value)
    {
        // Check arguments
        $this->setCheckDataTypeIsValid($strDataType);

        // Init var
        $tabConfig = $this->getTabConfig();
        $strCacheKey = $this->getStrCacheValueFormatGetKey($strDataType, $value);
        $result = (
            ((!is_null($strCacheKey)) && $this->checkCacheRequired())  ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $strCacheKey,
                    function() use ($strDataType, $value)
                    {
                        return $this->getDataTypeValueFormatGetEngine($strDataType, $value);
                    },
                    (
                        isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getDataTypeValueFormatGetEngine($strDataType, $value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified cache key,
     * to store entity attribute formatted value, when set action required,
     * from specified data type,
     * and specified value.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return null|string
     */
    protected function getStrCacheValueFormatSetKey($strDataType, $value)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_SET_KEY_PATTERN]) ?
                $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_FORMAT_SET_KEY_PATTERN] :
                ConstAttrSpec::CACHE_DEFAULT_VALUE_FORMAT_SET_KEY_PATTERN
        );
        $result = (
            (
                is_string($strDataType) &&
                is_string($strValueHash = ToolBoxHash::getStrHash($value))
            ) ?
                sprintf(
                    $strPattern,
                    $strDataType,
                    $strValueHash
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted value engine,
     * when set action required,
     * from specified validated data type.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return mixed
     */
    abstract protected function getDataTypeValueFormatSetEngine($strDataType, $value);



    /**
     * @inheritdoc
     * @throws DataTypeInvalidFormatException
     */
    public function getDataTypeValueFormatSet($strDataType, $value)
    {
        // Check arguments
        $this->setCheckDataTypeIsValid($strDataType);

        // Init var
        $tabConfig = $this->getTabConfig();
        $strCacheKey = $this->getStrCacheValueFormatSetKey($strDataType, $value);
        $result = (
            ((!is_null($strCacheKey)) && $this->checkCacheRequired())  ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $strCacheKey,
                    function() use ($strDataType, $value)
                    {
                        return $this->getDataTypeValueFormatSetEngine($strDataType, $value);
                    },
                    (
                        isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getDataTypeValueFormatSetEngine($strDataType, $value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified cache key,
     * to store entity attribute formatted value, when get action required, to be saved,
     * from specified data type,
     * and specified value.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return null|string
     */
    protected function getStrCacheValueSaveFormatGetKey($strDataType, $value)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_GET_KEY_PATTERN]) ?
                $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_GET_KEY_PATTERN] :
                ConstAttrSpec::CACHE_DEFAULT_VALUE_SAVE_FORMAT_GET_KEY_PATTERN
        );
        $result = (
            (
                is_string($strDataType) &&
                is_string($strValueHash = ToolBoxHash::getStrHash($value))
            ) ?
                sprintf(
                    $strPattern,
                    $strDataType,
                    $strValueHash
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted value engine,
     * when get action required, to be saved,
     * from specified validated data type.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return mixed
     */
    abstract protected function getDataTypeValueSaveFormatGetEngine($strDataType, $value);



    /**
     * @inheritdoc
     * @throws DataTypeInvalidFormatException
     */
    public function getDataTypeValueSaveFormatGet($strDataType, $value)
    {
        // Check arguments
        $this->setCheckDataTypeIsValid($strDataType);

        // Init var
        $tabConfig = $this->getTabConfig();
        $strCacheKey = $this->getStrCacheValueSaveFormatGetKey($strDataType, $value);
        $result = (
            ((!is_null($strCacheKey)) && $this->checkCacheRequired())  ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $strCacheKey,
                    function() use ($strDataType, $value)
                    {
                        return $this->getDataTypeValueSaveFormatGetEngine($strDataType, $value);
                    },
                    (
                        isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getDataTypeValueSaveFormatGetEngine($strDataType, $value)
        );

        // Return result
        return $result;
    }



    /**
     * Get specified cache key,
     * to store entity attribute formatted value, when set action required, to be loaded,
     * from specified data type,
     * and specified value.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return null|string
     */
    protected function getStrCacheValueSaveFormatSetKey($strDataType, $value)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_SET_KEY_PATTERN]) ?
                $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_VALUE_SAVE_FORMAT_SET_KEY_PATTERN] :
                ConstAttrSpec::CACHE_DEFAULT_VALUE_SAVE_FORMAT_SET_KEY_PATTERN
        );
        $result = (
            (
                is_string($strDataType) &&
                is_string($strValueHash = ToolBoxHash::getStrHash($value))
            ) ?
                sprintf(
                    $strPattern,
                    $strDataType,
                    $strValueHash
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get specified entity attribute formatted value engine,
     * when set action required, to be loaded,
     * from specified validated data type.
     *
     * @param string $strDataType
     * @param mixed $value
     * @return mixed
     */
    abstract protected function getDataTypeValueSaveFormatSetEngine($strDataType, $value);



    /**
     * @inheritdoc
     * @throws DataTypeInvalidFormatException
     */
    public function getDataTypeValueSaveFormatSet($strDataType, $value)
    {
        // Check arguments
        $this->setCheckDataTypeIsValid($strDataType);

        // Init var
        $tabConfig = $this->getTabConfig();
        $strCacheKey = $this->getStrCacheValueSaveFormatSetKey($strDataType, $value);
        $result = (
            ((!is_null($strCacheKey)) && $this->checkCacheRequired())  ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $strCacheKey,
                    function() use ($strDataType, $value)
                    {
                        return $this->getDataTypeValueSaveFormatSetEngine($strDataType, $value);
                    },
                    (
                        isset($tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstAttrSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
            ) :
            $this->getDataTypeValueSaveFormatSetEngine($strDataType, $value)
        );

        // Return result
        return $result;
    }



}