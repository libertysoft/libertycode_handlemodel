<?php
/**
 * Description :
 * This class allows to define handle validator configured entity class.
 * Handle validator configured entity is validator configured entity, using attribute provider,
 * to build and manage its attributes.
 *
 * Handle validator configured entity allows to configure attributes from following configuration (feature 'getTabConfig'):
 * [
 *     Validator configured entity configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\entity\model;

use liberty_code\model\entity\model\ValidatorConfigEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\handle_model\attribute\provider\api\AttrProviderInterface;



class HandleValidatorConfigEntity extends ValidatorConfigEntity
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Attribute provider object
     * @var AttrProviderInterface
     */
    protected $objAttrProvider;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttrProviderInterface $objAttrProvider
     */
    public function __construct(
        AttrProviderInterface $objAttrProvider,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Set attribute provider
        $this->setAttrProvider($objAttrProvider);

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get attribute provider object.
     *
     * @return AttrProviderInterface
     */
    public function getObjAttrProvider()
    {
        // Return result
        return $this->objAttrProvider;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return $this->getObjAttrProvider()->getTabEntityAttrConfig();
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return $this->getObjAttrProvider()->getTabEntityAttrRuleConfig();
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Return result
        return $this->getObjAttrProvider()->getEntityAttrValueFormatGet($strKey, $value);
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Return result
        return $this->getObjAttrProvider()->getEntityAttrValueFormatSet($strKey, $value);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set attribute provider object.
     *
     * @param AttrProviderInterface $objAttrProvider
     */
    public function setAttrProvider(AttrProviderInterface $objAttrProvider)
    {
        // Set data
        $this->objAttrProvider = $objAttrProvider;
    }



}