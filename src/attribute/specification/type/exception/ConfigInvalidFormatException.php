<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\handle_model\attribute\specification\type\exception;

use Exception;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDataType::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function(array $tabStr)
        {
            $result = true;
            $tabStr = array_values($tabStr);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid key
            (
                (!isset($config[ConstDataType::TAB_CONFIG_KEY_KEY])) ||
                (
                    is_string($config[ConstDataType::TAB_CONFIG_KEY_KEY]) &&
                    (trim($config[ConstDataType::TAB_CONFIG_KEY_KEY]) != '')
                )
            ) &&

            // Check valid type
            isset($config[ConstDataType::TAB_CONFIG_KEY_TYPE]) &&
            (
                (
                    is_string($config[ConstDataType::TAB_CONFIG_KEY_TYPE]) &&
                    (trim($config[ConstDataType::TAB_CONFIG_KEY_TYPE]) != '')
                ) ||
                (
                    is_array($config[ConstDataType::TAB_CONFIG_KEY_TYPE]) &&
                    $checkTabStrIsValid($config[ConstDataType::TAB_CONFIG_KEY_TYPE])
                )
            ) &&

            // Check valid order
            (
                (!isset($config[ConstDataType::TAB_CONFIG_KEY_ORDER])) ||
                is_int($config[ConstDataType::TAB_CONFIG_KEY_ORDER])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
     * @param array $tabFixConfig
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($config, array $tabFixConfig)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&
            ($tabFixConfig === array_intersect_key($config, $tabFixConfig)) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}