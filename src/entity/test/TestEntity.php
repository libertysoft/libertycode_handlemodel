<?php

namespace liberty_code\handle_model\entity\test;

use liberty_code\handle_model\entity\repository\model\HandleSaveConfigEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\handle_model\attribute\provider\api\AttrProviderInterface;
use liberty_code\handle_model\entity\test\TestAttrProvider;



/**
 * @method TestAttrProvider getObjAttrProvider() @inheritdoc
 */
class TestEntity extends HandleSaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param TestAttrProvider $objAttrProvider
     */
    public function __construct(
        TestAttrProvider $objAttrProvider,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Call parent constructor
        parent::__construct($objAttrProvider, $tabValue, $objValidator);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setAttrProvider(AttrProviderInterface $objAttrProvider)
    {
        // Set data, if required
        if($objAttrProvider instanceof TestAttrProvider)
        {
            // Call parent method
            parent::setAttrProvider($objAttrProvider);
        }
    }



}